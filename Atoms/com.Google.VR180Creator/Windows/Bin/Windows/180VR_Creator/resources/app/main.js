const electron = require('electron');
// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;
const ipc = electron.ipcMain;
const path = require('path');
const url = require('url');
const vr = require('vr180_editor');

// Keep a global reference of the window object. Otherwise the window may get
// garbage collected and it closes.
let mainWindow = null;

/**
 * Opens URLs in an external browser window and cancels event processing.
 * If the URL is a local file, ignore the request.
 * @param {!Event} event The JS event that triggered the action.
 * @param {string} url The URL to open.
 */
function openExternalUrl(event, url) {
  if (url.startsWith('file://')) return;
  event.preventDefault();
  electron.shell.openExternal(url);
}

/**
 * Registers a protocol handler for "font://" ressources.
 */
function registerFontProtocol() {
  const protocol = electron.protocol;
  protocol.registerFileProtocol('font', (request, callback) => {
    const url = request.url.substr('font://'.length);
    callback({path: path.join(__dirname, 'fonts', url)});
  });
}

/**
 * Create the main browser window.
 */
function createWindow() {
  // Disable all session permissions
  electron.session.defaultSession.setPermissionRequestHandler(
      (webContents, permission, callback) => {
        return callback(false);
      });

  mainWindow = new BrowserWindow({
    width: 1200,
    height: 750,
    minWidth: 1200,
    minHeight: 750,
    icon: path.join(__dirname, '../../images/vr180.png')
  });

  // Load the index.html of the app.
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true,
  }));

  // Handle all navigation requests with the external browser.
  mainWindow.webContents.on('will-navigate', openExternalUrl);
  mainWindow.webContents.on('new-window', openExternalUrl);

  // Emitted when the window is closed.
  mainWindow.on('closed', function() {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', function() {
  registerFontProtocol();
  createWindow();
});

// Quit when all windows are closed.
app.on('window-all-closed', function() {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', function() {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});
