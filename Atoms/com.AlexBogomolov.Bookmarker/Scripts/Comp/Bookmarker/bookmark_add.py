"""
Remember flow position script
Inspired by original tool by Michael Vorberg
Python implementation with QT UI
This script stores tool's name and bookmark in current comp metadata.

KEY FEATURES:
* Submit bookmark with ENTER button
* ESC to close the UI
* Rename current tool bookmark with reassigning different bookmark name to the selected tool
* Add Fusion 17 Flow bookmarks when adding a Bookmarker data.

Alexey Bogomolov mail@abogomolov.com
Requests and issues: https://github.com/movalex/fusion_scripts/issues
Donations are highly appreciated: https://paypal.me/aabogomolov

MIT License: https://mit-license.org/
"""
# legacy python reporting compatibility
from __future__ import print_function

comp = fu.GetCurrentComp()
ui = fu.UIManager
flow = comp.CurrentFrame.FlowView

# close UI on ESC button
comp.Execute(
    """app:AddConfig("AddBookmark",
{
    Target  {ID = "AddBookmark"},
    Hotkeys {Target = "AddBookmark",
             Defaults = true,
             ESCAPE = "Execute{cmd = [[app.UIManager:QueueEvent(obj, 'Close', {})]]}" }})
"""
)


def get_bm_name(tool):
    tool_ID_key = int(tool.GetAttrs("TOOLI_ID"))
    bm_data = comp.GetData("BM.tool_ID{}".format(tool_ID_key))
    if bm_data:
        return bm_data.get(1)


def set_bookmark(tool=None):
    """grab current checkbox index and save bookmark data"""
    bm_text = itm["BookmarkLine"].GetText()

    current_scale = flow.GetScale()

    if tool:
        tool_name = tool.Name
        toolID = tool.ID
        tool_reg_id = tool.GetAttrs("TOOLI_ID")
        tool_ID_key = "tool_ID{}".format(int(tool_reg_id))
        print("bookmarked {} as {}".format(tool.ID, bm_text))
        comp.SetData(
                "BM.{}".format(tool_ID_key),
                [bm_text, tool_name, current_scale, tool_ID_key, toolID],
                )
    else:
        toolID = "Location"
        print("bookmarked current flow location")
        comp.SetData(
                "BM.{}".format(bm_text),
                [bm_text, toolID],
                )

    if fu.Version >= 17:
        bookmark_name = "[BM] : {}".format(bm_text)
        flow.InsertBookmark(bookmark_name)


def get_tool():
    comp = fu.GetCurrentComp()
    """get either active or selected tool
    from multiple selected tools use the first one"""
    active = comp.ActiveTool
    if active:
        return active
    selected_nodes = list(comp.GetToolList(True).values())
    if len(selected_nodes) > 0:
        return selected_nodes[0]
    print("No tools selected. You can save current flow location")
    return False


def close_UI(ev):
    disp.ExitLoop()


def enter_bookmark(ev):
    if itm["BookmarkLine"].Text == "":
        print("enter bookmark name!")
        return
    set_bookmark(tool)
    disp.ExitLoop()


def launch_ui(tool=None):
    if not tool:
        line_text = ""
    else:
        bookmark_name = get_bm_name(tool)
        if bookmark_name:
            # tool is already bookmarked
            bookmark_data = "[BM] : {}".format(bookmark_name)
            # print("deleting {}".format(bookmark_data))
            comp = fu.GetCurrentComp()
            flow = comp.CurrentFrame.FlowView
            if fu.Version >= 17:
                flow.DeleteBookmark(bookmark_data)
            line_text = bookmark_name
        else:
            line_text = tool.Name
    disp = bmd.UIDispatcher(ui)

    # Main Window
    win = disp.AddWindow(
        {
            "ID": "AddBookmark",
            "TargetID": "AddBookmark",
            "WindowTitle": "Add Bookmark",
            "Geometry": [800, 600, 300, 75],
        },
        [
            ui.VGroup(
                [
                    ui.LineEdit(
                        {
                            "ID": "BookmarkLine",
                            "Text": line_text,
                            "Weight": 0.5,
                            "Events": {"ReturnPressed": True},
                            "Alignment": {"AlignHCenter": True, "AlignVCenter": True},
                        }
                    ),
                    ui.HGroup(
                        {"Alignment": {"AlignHRight": True}, "Weight": 0},
                        [
                            ui.Button({"ID": "OkButton", "Text": "Ok", }),
                            ui.Button({"ID": "CancelButton", "Text": "Cancel", }),
                        ]
                    ),
                ]
            ),
        ],
    )

    itm = win.GetItems()

    if tool:
        itm["BookmarkLine"].SelectAll()
    itm["BookmarkLine"].SetClearButtonEnabled(True)
    itm["BookmarkLine"].SetPlaceholderText("Type bookmark name and hit Enter")
    return win, itm, disp


if __name__ == "__main__":
    tool = get_tool()
    add_win_exists = ui.FindWindow("AddBookmark")

    if add_win_exists:
        add_win_exists.Raise()
        add_win_exists.ActivateWindow()
        if tool:
            win_itm = add_win_exists.GetItems()
            win_itm["BookmarkLine"].Text = tool.Name
            win_itm["BookmarkLine"].SelectAll()
    else:
        win, itm, disp = launch_ui(tool)
        win.On.OkButton.Clicked = enter_bookmark
        win.On.CancelButton.Clicked = close_UI
        win.On.BookmarkLine.ReturnPressed = enter_bookmark
        win.On.AddBookmark.Close = close_UI
        win.Show()
        disp.RunLoop()
        win.Hide()
