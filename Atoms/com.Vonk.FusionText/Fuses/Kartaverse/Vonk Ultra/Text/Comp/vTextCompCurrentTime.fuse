-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextCompCurrentTime"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = "Number",
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Comp",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Returns the comp's Current Time.",
    REGS_OpIconString = FUSE_NAME,
    REG_TimeVariant = true, -- required to disable caching of the current time parameter
    REGB_Temporal = true, -- ensures reliability in Resolve 15
})

function Create()
    -- [[ Creates the user interface. ]]

    -- Should the image sequence frame numbering be shifted (forwards/backwards) by the sequence offset value?
    InSequenceStartFrame = self:AddInput('Sequence Offset', 'SequenceStartFrame', {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        -- INPID_InputControl = "SliderControl",
        -- TEC_Lines = 1,
        INP_Integer = true,
        INP_MinScale = -100,
        INP_MaxScale = 100,
        INP_Default = 0,
        IC_Steps = 201,
        INP_MinAllowed = -1e+38,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = -1000000,
        -- INP_MaxAllowed = 1000000,
        LINK_Main = 1,
    })

    InPadding = self:AddInput("Padding", "Padding", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 8,
        INP_Default = 1,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 38,
        INP_Integer = true,
        LINK_Main = 2,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    Output = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InPadding:SetAttrs({LINK_Visible = visible})
        InSequenceStartFrame:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    -- Should the image sequence frame numbering be shifted (forwards/backwards) by the sequence offset value?
    local sequenceStartFrame = tonumber(InSequenceStartFrame:GetValue(req).Value)

    -- Note: Comp.CurrentTime will still report the current timeline frame when a time remapping effect is done. If you need to iterate across a range of frames for a temporal effect use Req.Time instead.
    local num = tonumber(self.Comp.CurrentTime) + sequenceStartFrame

    local padding = InPadding:GetValue(req).Value
    local result = tostring(string.format('%0' .. padding .. 'd', tonumber(num)))
    local out = Text(result)

    Output:Set(req, out)
end
