-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextFromFile"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\IO",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Reads a Text string from a file.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InFile = self:AddInput("Input" , "Input" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "FileControl",
        FC_IsSaver = false,
        FC_ClipBrowse = false,
        FCS_FilterString =  "TXT (*.txt)|*.txt|CSV Files (*.csv)|*.csv|JSON Files (*.json)|*.json|YAML Files (*.yml)|*.yml|XML Files (*.xml)|*.xml",
        LINK_Main = 1
    })

--    InViewFile = self:AddInput('View File', 'View File', {
--        LINKID_DataType = 'Number',
--        INPID_InputControl = 'ButtonControl',
--        INP_DoNotifyChanged = true,
--        INP_External = false,
--        ICD_Width = 1,
--        INP_Passive = true,
--        IC_Visible = true,
--        BTNCS_Execute = [[
---- check if a tool is selected
--local selectedNode = tool or comp.ActiveTool
--if selectedNode then
--    local filename = comp:MapPath(selectedNode:GetInput('Input'))
--    if filename then
--        if bmd.fileexists(filename) then
--            bmd.openfileexternal('Open', filename)
--        else
--            print('[View File] File does not exist yet:', filename)
--        end
--    else
--        print('[View File] Filename is nil. Possibly the text is sourced from an external text input connection.')
--    end
--else
--    print('[View File] Please select the node.')
--end
--        ]],
--    })

    InRemoveNonPrintableCharacters = self:AddInput("Remove Non-Printable Characters", "RemoveNonPrintableCharacters", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function read_file( filename )
    local f = assert( io.open( filename , "rb" ) )
    local content = f:read( "*all" )

    f:close()

    return content
end


function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InFile:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local rel_path = InFile:GetValue(req).Value
    local abs_path = self.Comp:MapPath(rel_path)

    local txt_str = read_file(abs_path)

    -- Zap the gremlins found in invisible non-ASCII printable characters
    local remove = InRemoveNonPrintableCharacters:GetValue(req).Value
    if remove == 1 then
        -- txt_str = string.gsub(txt_str, "[%z\1-\31]", "")
        -- txt_str = string.gsub(txt_str, "[%W]", "")
        txt_str = string.gsub(txt_str, "[%z\1-\8\11-\12\14-\31]", "")
        txt_str = string.gsub(txt_str, "[\127-\255]", "")
    end

    -- dump(txt_str)
    local out = Text(txt_str)

    OutText:Set(req, out)
end
