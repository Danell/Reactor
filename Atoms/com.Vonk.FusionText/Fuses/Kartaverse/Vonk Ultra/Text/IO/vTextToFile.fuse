-- ============================================================================
-- modules
-- ============================================================================
local textutils = self and require("vtextutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextToFile"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_SinkTool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\IO",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Writes a Text string to a file.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InText = self:AddInput("Input" , "Input" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 10,
        LINK_Main = 1
    })

    InFile = self:AddInput("File" , "File" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "FileControl",
        FC_IsSaver = true,
        FC_ClipBrowse = false,
        LINK_Visible = false,
        FCS_FilterString =  "TXT (*.txt)|*.txt",
        LINK_Main = 2
    })

--    InViewFile = self:AddInput('View File', 'View File', {
--        LINKID_DataType = 'Number',
--        INPID_InputControl = 'ButtonControl',
--        INP_DoNotifyChanged = true,
--        INP_External = false,
--        ICD_Width = 1,
--        INP_Passive = true,
--        IC_Visible = true,
--        BTNCS_Execute = [[
---- check if a tool is selected
--local selectedNode = tool or comp.ActiveTool
--if selectedNode then
--    local filename = comp:MapPath(selectedNode:GetInput('File'))
--    if filename then
--        if bmd.fileexists(filename) then
--            bmd.openfileexternal('Open', filename)
--        else
--            print('[View File] File does not exist yet:', filename)
--        end
--    else
--        print('[View File] Filename is nil.')
--    end
--else
--    print('[View File] Please select the node.')
--end
--        ]],
--    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InFile:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local txt_str = InText:GetValue(req).Value

    local rel_path = InFile:GetValue(req).Value
    local abs_path = self.Comp:MapPath(rel_path)

    textutils.write_file(abs_path, txt_str)
    local out = Text(txt_str)

    OutText:Set(req, out)
end
