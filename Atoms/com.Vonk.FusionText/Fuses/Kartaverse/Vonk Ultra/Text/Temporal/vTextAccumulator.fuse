-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextAccumulator"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_SinkTool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Temporal",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Temporally concatenates a text string over a frame range.",
    REGS_OpIconString = FUSE_NAME
})

function Create()
    -- [[ Creates the user interface. ]]
    InText = self:AddInput("Text", "Text", {
        LINKID_DataType = "Text",
        LINK_Main = 1,
        INP_Required = false
    })

    InStartFrame = self:AddInput("Start Frame", "StartFrame", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinAllowed = 0,
        INP_MaxAllowed = 1e+38,
        INP_MaxScale = 2000,
        INP_Integer = true,
        INP_Default = 0,
        IC_Steps = 1.0,
        IC_Visible = true,
        LINK_Main = 3,
    })

    InEndFrame = self:AddInput("End Frame", "EndFrame", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinAllowed = 0,
        INP_MaxAllowed = 1e+38,
        INP_MaxScale = 2000,
        INP_Integer = true,
        INP_Default = 0,
        IC_Steps = 1.0,
        IC_Visible = true,
        LINK_Main = 4,
    })

    InStep = self:AddInput("Step", "Step", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        TEC_Lines = 1,
        INP_MinScale = -120,
        INP_MaxScale = 120,
        INP_Default = 1,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 1e+38,
        LINK_Main = 5,
    })

    InSeparator = self:AddInput("Separator", "Separator", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        INPS_DefaultText = [[\n]],
        TEC_Lines = 1,
        LINK_Main = 2,
        INP_Required = false
    })

    InSort = self:AddInput("Sort List", "Sort", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InRemoveDuplicates = self:AddInput("Remove Duplicates", "RemoveDuplicates", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end


function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InStartFrame:SetAttrs({LINK_Visible = visible})
        InEndFrame:SetAttrs({LINK_Visible = visible})
        InStep:SetAttrs({LINK_Visible = visible})
        InSeparator:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local start_frame = tonumber(InStartFrame:GetValue(req).Value)
    local end_frame = tonumber(InEndFrame:GetValue(req).Value)
    local step = tonumber(InStep:GetValue(req).Value)
    local sort = InSort:GetValue(req).Value
    local remove_dup = InRemoveDuplicates:GetValue(req).Value

    -- Replace newlines and tabs: https://stackoverflow.com/questions/57099292/replace-n-string-with-real-n-in-lua
    local seperator = InSeparator:GetValue(req).Value:gsub("\\([nt])", {n = "\n", t = "\t", r = "\r"})

    local lines = {}

    for i = start_frame, end_frame, step do
        if InText:GetSource(i, req:GetFlags()) and InText:GetSource(i, req:GetFlags()).Value then
            local sample = InText:GetSource(i, req:GetFlags()).Value
            if sample ~= nil and sample ~= "" then
                if sort == 1.0 then
                    -- Help sort the sub-lists alphabetically by breaking them apart per-line
                    local currentLine = 0
                    for row in string.gmatch(sample, "[^\r\n]+") do
                        table.insert(lines, row)
                    end
                else
                    table.insert(lines, sample)
                end
            end
        end

        -- Update the rendering progress bar on the node
        local progress = i/end_frame
        self:SetProgress(progress)
    end

    -- De-duplicate list items
    local result = {}
    if remove_dup == 1.0 then
        -- https://stackoverflow.com/questions/20066835/lua-remove-duplicate-elements/20067270#20067270
        local hash = {}
        for _,v in ipairs(lines) do
           if not hash[v] then
               result[#result + 1] = v
               hash[v] = true
           end
        end
    else
        result = lines
    end

--   for _,v in ipairs(result) do
--        -- Read a time sample
--       local sample = v
--       if i == 1 then
--           accumulate_str = tostring(sample)
--       else
--           accumulate_str = accumulate_str .. seperator .. tostring(sample)
--       end
--   end

    -- Sort the list alphabetically
    if sort == 1.0 then
        table.sort(result)
    end
    
    local accumulate_str = ""
    accumulate_str = table.concat(result, seperator)

    -- Remove the first newline
    -- accumulate_str = accumulate_str:sub(2)

    local out = Text(accumulate_str)
    OutText:Set(req, out)
end
