-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextFromNumberPadded"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Converts a number to a leading zero padded text",
    REGS_OpIconString = FUSE_NAME
})

function Create()
    -- [[ Creates the user interface. ]]
    InNumber = self:AddInput("Number" , "Number" , {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        LINK_Main = 1,
        INP_MinScale = -100,
        INP_MaxScale = 100,
        INP_MinAllowed = -1e+38,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = -1000000,
        -- INP_MaxAllowed = 1000000,
        INP_Default = 0
    })

    InPadding = self:AddInput("Padding", "Padding", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 8,
        INP_Default = 1,
        INP_MinAllowed = 1,
        INP_Integer = true,
        LINK_Main = 2
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InNumber:SetAttrs({LINK_Visible = visible})
        InPadding:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local num = InNumber:GetValue(req).Value
    local padding = InPadding:GetValue(req).Value

    local result = tostring(string.format('%0' .. padding .. 'd', tonumber(num)))
    local out = Text(result)

    OutText:Set(req, out)
end
