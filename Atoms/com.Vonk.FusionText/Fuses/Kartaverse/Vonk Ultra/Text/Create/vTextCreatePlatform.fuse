-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextCreatePlatform"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Create",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Create a unique Fusion Text object per OS platform.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InMac = self:AddInput("macOS" , "macOS" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1
    })

    InWindows = self:AddInput("Windows" , "Windows" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1
    })

    InLinux = self:AddInput("Linux" , "Linux" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1
    })

    InExpandPathMaps = self:AddInput("Expand PathMaps", "ExpandPathMaps", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end


function GetPlatform()
    -- Find out the current Fusion host platform (Windows/Mac/Linux)
    local platform = ""

--    if string.find(self.Comp:MapPath("Fusion:/"), "Program Files", 1) then
--        platform = "Windows"
--    elseif string.find(self.Comp:MapPath("Fusion:/"), "PROGRA~1", 1) then
--        platform = "Windows"
--    elseif string.find(self.Comp:MapPath("Fusion:/"), "Applications", 1) then
--        platform = "Mac"
--    else
--        platform = "Linux"
--    end

    if jit.os == "Windows" then
        platform = "Windows"
    elseif jit.os == "Linux" then
        platform = "Linux"
    elseif jit.os == "OSX" then
        platform = "Mac"
    else
        platform = "Linux"
    end

    return platform
end

function Process(req)
    -- [[ Creates the output. ]]
    local mac = InMac:GetValue(req).Value
    local win = InWindows:GetValue(req).Value
    local linux = InLinux:GetValue(req).Value

    local expandPathMaps = InExpandPathMaps:GetValue(req).Value

    local platform = GetPlatform()
    local result = ""
    if platform == "Windows" then
        -- Running on Windows
        result = win
    elseif platform == 'Mac' then
        -- Running on Mac
        result = mac
    elseif platform == "Linux" then
        -- Running on Linux
        result = linux
    else
        error("[TextCreatePlatform] There is an invalid Fusion platform detected")
    end

    -- Expand PatMaps to absolute filepaths
    local out = ""
    if expandPathMaps == 1 then
        out = Text(self.Comp:MapPath(result))
    else
        out = Text(result)
    end

    OutText:Set(req, out)
end
