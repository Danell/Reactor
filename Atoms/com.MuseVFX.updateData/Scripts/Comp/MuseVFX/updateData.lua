DEBUG = false
print("Update Data v1.0")
--[[--

Fusion 9 Update Comp Data
	v1.0 - 2018-07-06
	by Bryan Ray for Muse VFX

	=====Overview======

	Updates CustomData for the comp in case it doesn't exist or The Grid was incorrect at the time of comp creation.
	

	=====License======

	This script is released to the public domain, and no guarantee is made as to its suitability for any particular purpose. No warranty
	is provided, and no burden of support is assumed by any of its creators or distributors. 

	Change log:
		v1.0: Initial release


-- To Do: 	
		

--]]--



-- ===========================================================================
-- constants
-- ===========================================================================

-- PIPELINE CONFIG
VERSION_FORMAT = "[Vv]%d+"	-- Matches v## format. This will match element_v03 or v03_element, as well. For v_## use "[Vv]_%d+". 
							-- And beware of projects or elements that end with a v. Dragunov1, for instance, will give you a false match.

RESOLUTION_FORMAT = "%d+[Xx]%d+"	-- Matches ####x#### format for resolution folder. Beware of projects or shots that match this pattern.
									-- 2x4 will give a false match.

COMP_FILE_DEPTH = 3			-- Determines how deep in the shot directory structure the comp file lives. This is used to identify assets that
							-- are external to the shot folder.

SEPARATOR = package.config:sub(1,1)	-- Folder separator used by the Operating System.

SHOT_FOLDER =  "comp:.." .. SEPARATOR .. ".." .. SEPARATOR 	-- Path relative to the .comp file.
															-- comp:..\..\


-- ===========================================================================
-- globals
-- ===========================================================================
_fusion = nil
_comp = nil
ui = nil
disp = nil



--==============================================================
-- Main()
--
-- Typical set-up for a script developed in an IDE. Sets globals
-- and connects to the active comp. Initializes UI Manger.
--==============================================================

function main()
	-- get fusion instance
	_fusion = getFusion()

	-- ensure a fusion instance was retrieved
	if not _fusion then
		error("Please open the Fusion GUI before running this tool.")
	end

	-- Set up aliases to the UI Manager framework
	ui = _fusion.UIManager
	disp = bmd.UIDispatcher(ui)
	
	-- get composition
	_comp = _fusion.CurrentComp
	SetActiveComp(_comp)

	-- ensure a composition is active
	if not _comp then
		error("Please open a composition before running this tool.")
	end

	dprint("\nActive comp is ".._comp:GetAttrs().COMPS_Name)

	-- Get current CustomData
	data = {}
	local maxwidth = 0
	--if #data > 0 then
		for key, value in ipairs(_comp:GetData()) do 
			data[value] = _comp:GetData(value)

			if type(data[value]) == 'number' then
				data[value] = tostring(data[value])..'_@num'
			end
			if type(data[value]) == 'boolean' then
				data[value] = tostring(data[value])
			end
			--dprint(data[value]..": "..type(data[value]))
			if type(data[value]) == 'table' then
				data[value] = '[table]'
			end
			maxwidth = math.max(maxwidth, (string.len(data[value]) * 7 + string.len(key) * 7) + 48)
		end
	--end

	dprint('maxwidth = '..maxwidth)

	-- UI Manager
	local x = 960
	local y = 540
	local width = math.max(maxwidth, 400)
	local height = 120 + 40 * table.getn(data)
	window = createMainWindow(x, y, width, height, 'Update Comp Data' )
	local content = window:GetItems().root:GetItems().content

	content:AddChild(ui:HGroup{ 
		Weight = 0, 
		ui:Label{ ID = 'keylabel', Text = 'Key', Weight = 0.2}, 
		ui:LineEdit{ ID = 'key' }, 
		ui:HGap(50), 
		ui:Label{ ID = 'valueLabel', Text = 'Value', Weight = 0.2},
		ui:LineEdit{ ID = 'value' }, 
		ui:HGap(10), 
		ui:Button{ ID = 'set', Text = 'Set Value', Weight = 0.2},
		})

	content:AddChild(ui:HGroup{ui:Tree{ ID = 'tree', SortingEnabled = true, Events = { ItemChanged = true, }, }, })

	window:Resize({ width, 400 })
	window:RecalcLayout()

	local windowItems = window:GetItems()
	local columns = {'key', 'value', }
	local widths = { 120, 180, }

	configureTree(windowItems.tree, columns, widths, false)
	populateTree(data, windowItems.tree, false)

	ddump(windowItems)

	function window.On.set.Clicked(ev)
		dprint('Setting '..windowItems.key.Text..' to '..windowItems.value.Text)
		_comp:SetData(windowItems.key.Text, windowItems.value.Text)

		for key2, value2 in ipairs(_comp:GetData()) do 
			data[value2] = _comp:GetData(value2)

			if type(data[value2]) == 'number' then
				data[value2] = tostring(data[value2])..'_@num'
			end
			if type(data[value2]) == 'boolean' then
				data[value2] = tostring(data[value2])
			end
			if type(data[value2]) == 'table' then
				data[value2] = '[table]'
			end
			dprint(data[value2]..": "..type(data[value2]))
		end

		window:GetItems().root:GetItems().content:RemoveChild(tree)
		content:AddChild(ui:HGroup{ui:Tree{ ID = 'tree', SortingEnabled = true, }, })
		windowItems = window:GetItems()
		configureTree(windowItems.tree, columns, widths, false)
		populateTree(data, windowItems.tree, false)
		window:RecalcLayout()
	end

	function window.On.tree.ItemClicked(ev)
		windowItems.key.Text = ev.item.Text[0]
		windowItems.value.Text = ev.item.Text[1]
	end

	function window.On.tree.ItemChanged(ev)
		_comp:SetData(ev.item.Text[0], ev.item.Text[1])
		if ev.item.Text[1] == 'nil' then
			_comp:SetData(ev.item.Text[0], nil)
		end
	end

	function window.On.main.Close(ev) 
		disp:ExitLoop() 
	end

	window:Show()
	disp:RunLoop()
	window:Hide()


end



--==================================================================================================================
-- UI MANAGER
--==================================================================================================================

--------------- Tree Creation functions ----------------------
function configureTree(tree, columns, widths, headerHidden)
	-- configures given tree with given data
	tree.ColumnCount = table.getn(columns)
	tree.HeaderHidden = headerHidden

	-- create column headers
	local header = tree:NewItem()
	for i, column in ipairs(columns) do
		header.Text[i-1] = column
		tree.ColumnWidth[i-1] = widths[i]
		dprint(column.." column width: "..widths[i].." px.")
	end
	tree:SetHeaderItem(header)

end

function setTreeItemValue(item, index, value, allowCheckbox)
	-- set the value of a tree item
	if type(value) == "boolean" and allowCheckbox then
		item.Text[index-1] = ''
		if value then
			item.CheckState[index-1] = "Checked"
		else
			item.CheckState[index-1] = "Unchecked"
		end
	elseif type(value) == 'table' then
		item.TextColor[0] = value
	else
		value = string.gsub(value, '_@num', '')
		item.Text[index-1] = value
	end
end

function createTreeItem(tree, data, allowCheckbox)
	-- creates a tree item
	local item = tree:NewItem()
	local i = 0
	for i, value in ipairs(data) do
		dprint("setting "..value..", "..i)
		setTreeItemValue(item, i, value, allowCheckbox)
	end
	return item
end

function addTreeTopLevelItem(tree, data, allowCheckbox, expanded)
	-- creates a tree item parented to the tree
	local item = createTreeItem(tree, data, allowCheckbox)
	tree:AddTopLevelItem(item)
	item.Expanded = expanded
	return item
end

function addTreeLevelItem(tree, data, parent)
	-- creates a tree item parented to the given parent item
	local item = createTreeItem(tree, data, true)
	parent:AddChild(item)
	return item
end



function populateTree(data, tree, allowSelectAll)

	if allowSelectAll then
		local parent = addTreeTopLevelItem(tree, {'Select all', true, {R = 0.5, G = 0.5, B = 0.5, A = 1}}, true)
	end


	for key, value in pairs(data) do
		local parent = addTreeTopLevelItem(tree, { key, value, }, false, false)
		parent:SetFlags( { 
				ItemIsSelectable = true,
				ItemIsDropEnabled = true,
				ItemIsTristate = false,
				ItemNeverHasChildren = false,
				ItemIsDragEnabled = true,
				ItemIsEditable = true,
				ItemIsUserCheckable = true,
				ItemIsEnabled = true, 
			} )
	end

end


--=================================================================
-- createMainWindow(x, y, width, height, title)
--
-- Creates a generic UI Manager window featuring the Muse VFX
-- Logo header. There is an empty content container into 
-- which other functions can load information.
--
-- Arguments:
--		x, y: Integer. Screen coordinates where window will appear
--		width, height: Integer. Window size in pixels
--		title: String. Window title.
-- Returns:
--		window: UIWindow object
--=================================================================
function createMainWindow(x, y, width, height, title)
	dprint('createMainWindow')

	-- Generate HTML for logo header
	html = "<html>\n"
	html = html.."\t<head>\n"
	html = html.."\t\t<style>\n"
	html = html.."\t\t</style>\n"
	html = html .."\t</head>\n"
	html = html .."\t<body>\n"
	html = html .."\t<div>\n"
	html = html .."\t<div style=\"float:right;\">\n"
	--html = html .. museLogo()
	html = html .. "\t</div>\n"
	html = html .. "\t</body>\n"
	html = html .. "</html>"

	-- Create the window
	window = disp:AddWindow({

		-- Window properties
		ID = 'main',
		WindowTitle = title,
		--Geometry = {x, y, width, height},
	
		-- Main window container  
		ui:VGroup{
	    	ID = 'root',
	    	
	    	-- The logo header.
	    	-- ui:VGroup{
		    -- 	ID = 'header',
		    -- 	Weight = 0,
		    -- 	ui:TextEdit{ ID = 'museLogo', ReadOnly = true, Alignment = { AlignHCenter = true, AlignTop = true, }, MinimumSize = {650, 130}, MaximumSize = {width * 2, 130}, HTML = html, },
		    -- },
	    	
	    	-- This holds the dynamic content
	    	ui:VGroup{
	    		ID = 'content',
	    		Weight = 2.0,
	    	},

	    	-- Footer contains the window control buttons
	   --  	ui:VGroup{
	   --  		ID = 'footer',
	   --  		Weight = 0.0,
				-- ui:HGroup{
	   --  			ID = 'control',
	   --  			Weight = 0.0,

	   --  			ui:HGap(width - 300),

	   --  			ui:Button{
	   --  				ID = 'cancel',
	   --  				Text = 'Cancel',
	   --  			},

		  --   		ui:Button{
		  --   			ID = 'next',
		  --   			Text = 'Next',
		  --   		},
	   --  		},
	   --  	},
	   	},
	})

	return window
end


--======================== ENVIRONMENT SETUP ============================--

------------------------------------------------------------------------
-- getFusion()
--
-- check if global fusion is set, meaning this script is being
-- executed from within fusion
--
-- Arguments: None
-- Returns: handle to the Fusion instance
------------------------------------------------------------------------
function getFusion()
	if fusion == nil then 
		-- remotely get the fusion ui instance
		fusion = bmd.scriptapp("Fusion", "localhost")
	end
	return fusion
end -- end of getFusion()


--========================== DEBUGGING ============================--


---------------------------------------------------------------------
-- dprint(string, suppressNewline)
--
-- Prints debugging information to the console when DEBUG flag is set
--
-- Arguments:
--		string, string, a message for the console
--		suppressNewline, boolean, do not start a new line
---------------------------------------------------------------------
function dprint(string, suppressNewline)
	local newline = "\n"
	if suppressNewline then newline = '' end
	if DEBUG then _comp:Print(string..newline) end
end -- dprint()

---------------------------------------------------------------------
-- ddump(object)
--
-- Performs a dump() if the DEBUG flag is set
--
-- Arguments
--		object, object, an object to be dumped
---------------------------------------------------------------------
function ddump(object)
	if DEBUG then dump(object) end
end -- end ddump()

main()