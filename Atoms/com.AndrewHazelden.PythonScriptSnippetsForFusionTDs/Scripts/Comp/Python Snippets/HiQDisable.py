# You can disable the High Quality (HiQ) rendering mode using:

comp = fu.CurrentComp
comp.SetAttrs({'COMPB_HiQ' : False})
