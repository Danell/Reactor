-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vPointToText"
DATATYPE = "Point"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = "Text",
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Point\\Text",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Return a pair of Text objects from a Fusion Point object.",
    REGS_OpIconString = FUSE_NAME,
})

function Create()
    -- [[ Creates the user interface. ]]
    InPoint = self:AddInput("Point", "Point", {
        LINKID_DataType = "Point",
        LINK_Main = 1,
        INPID_InputControl = "OffsetControl",
        INPID_PreviewControl = 'CrosshairControl',
        INP_DoNotifyChanged = true,
        INP_DefaultX = 0.5,
        INP_DefaultY = 0.5,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutTextX = self:AddOutput("OutputX", "OutputX", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })

    OutTextY = self:AddOutput("OutputY", "OutputY", {
        LINKID_DataType = "Text",
        LINK_Main = 2
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end
        InPoint:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local point = InPoint:GetValue(req)

    local outX = Text(tostring((point.X)))
    local outY = Text(tostring((point.Y)))

    OutTextX:Set(req, outX)
    OutTextY:Set(req, outY)
end
