-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil
local arrayutils = self and require("varrayutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vMetadataFromMediaIn"
DATATYPE = "Image"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Meta\\Resolve",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Creates a Fusion image with MediaIn MediaProps metadata.",
    REGS_OpIconString = FUSE_NAME
})

function Create()
    InImage = self:AddInput("MediaIn Input", "Input",{
        LINKID_DataType = "Image",
        INPID_InputControl = "ImageControl",
        LINK_Visible = true,
        LINK_Main = 1,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutImage = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Image",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InImage:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local img = InImage:GetValue(req)

    local result = Image({IMG_Like = img, IMG_NoData = req:IsPreCalc()})
    -- Crop (with no offset, ie. Copy) handles images having no data, so we don't need to put this within if/then/end
    img:Crop(result, {})

    -- Based upon Cryptomatte for Fusion 8 upstream connection scanning code
    local fusion = Fusion("localhost", 0, bmd.getappuuid())
    local comp = fusion.CurrentComp
    local tool = comp:FindTool(self.Name)
    local input = tool:FindMainInput(1)
    if input ~= nil then
        local input_tool = input:GetConnectedOutput():GetTool()
        if input_tool ~= nil then
            local tbl = input_tool:GetData("MediaProps")
            if tbl ~= nil and #tbl then
                -- Grab the Metadata Lua table from the image input
                local meta = result.Metadata or {}
                
                -- Merge the metadata
                if tbl then
                    for name, val in pairs(tbl) do
                        meta[name] = val
                    end
                end

                result.Metadata = meta
            end
        end
    end

    OutImage:Set(req, result)
end
