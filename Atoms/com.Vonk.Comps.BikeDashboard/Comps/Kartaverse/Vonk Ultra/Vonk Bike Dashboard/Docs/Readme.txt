Bike Dashboard 2023-12-19
By Andrew Hazelden <andrew@andrewhazelden.com>

Overview:
This example was prepared for WSL forum user "Misocko".

The example shows how a CSV file can be used with the Vonk data nodes in Fusion. A Fusion Macro/Effects Template is used to render HUD overlay graphics for a bike ride.

The example macro was tested with 1920x1080p resolution footage. 

If your footage is a different aspect ratio, or is a vertical format video you may have to update the Merge node transform parameters to line the dashboard visuals up to your footage resolution.

Vonk Ultra v1.5 is required to use this example macro. Vonk is available from the Steakunderwater Fusion community forum's Reactor Package Manager toolset.


Effects Template:
The provided "Bike Dashboard HD.setting" effects template macro is able to be applied to footage in the Resolve Edit page.

Install the Effects Template file to the PathMap folder of:
Reactor:/Templates/Edit/Effects/Vonk Ultra/Bike Dashboard HD.setting

The Effects template is used on the Edit page by opening the Effects library tab at the top left of the Resolve UI. Expand the "Toolbox > Effects" section and look for the "Vonk Ultra > Bike Dashboard HD" item.

Drag the effects template onto the footage in the timeline.

Then in the Inspector window switch to the Effects tab. Next to the "Bike Dashboard HD" item's "CSV File" heading is a "Browse" button. The Browse button can be used to select your CSV file.

The BMP and speed in meters per second gauges should be visible ontop of your footage. The gauges update at a rate of one CSV sample per second.

Vonk Node Graph Details:

Reading the Time Value:
The "vNumberCompReqTime1" node reads the current time value. This number is supposed to refresh the CSV at a rate of 1 sample per second. This means we need to divide the time value by the composite's frame rate which is provided by the "vNumberCompFPS1" node. 

The floating point number is rounded downwards to the nearest whole number using the vNumberFloor node.


CSV Processing:
The CSV data is imported using a "vTextFromFile" node. This creates a text based output. Since the CSV data is semicolon separated, not comma separated a vTextDoString node is used to reformat the text by replacing ";" characters with "," characters. 

Then vNumberFromCSV nodes are used to read the individual records from the CSV file. Each heading item is accessed using the "Column" value on the vNumberFromCSV nodes to select the heading item. The vNumberfromCSV node uses the "Row" input to define the CSV line number to read.

The CSV records were then linked into Text+ nodes using expressions:
:return csv_Heartratebpm:GetValue("Output", time) .. " BPM"

:return csv_Speedms:GetValue("Output", time) .. " m/s"


Icons:
The ForkAwesome font was used to generate the two icons. The icon font based graphics were converted into outlines using Affinity Designer and exported to SVG.

Sample Photo:
The photo used in the example comp comes from Pixbay (https://pixabay.com/photos/nature-cyclists-bike-trail-sport-1792422/). It was photographed by "FabricioMacedoPhotos". The photo's original filename was "pixabay-nature-cyclists-bike-trail-sport-1792422.jpg"

This image is used under the Pixbay content license terms (https://pixabay.com/service/license-summary/)