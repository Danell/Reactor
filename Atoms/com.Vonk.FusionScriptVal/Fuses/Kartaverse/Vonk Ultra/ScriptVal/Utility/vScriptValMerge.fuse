-- ============================================================================
-- modules
-- ============================================================================
-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValMerge"
DATATYPE = "ScriptVal"
MAX_INPUTS = 64

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\ScriptVal\\Utility",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Dynamically join ScriptVal elements into one table.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InWhich = self:AddInput("Which", "Which", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        INP_MinAllowed = 1,
        INP_MaxAllowed = MAX_INPUTS,
        INP_MaxScale = 1,
        INP_Integer = true,
        IC_Steps = 1.0,
        IC_Visible = false
    })

    InScriptVal1 = self:AddInput("ScriptVal1", "ScriptVal1", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1,
        INP_Required = false
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutScriptVal = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1
    })
end



function OnAddToFlow()
    --[[ Callback triggered when adding the Fuse to the flow view. ]]
    -- find highest existing input
    local highest_input = 0

    for i = 1, MAX_INPUTS do
        local input = self:FindInput("ScriptVal" .. tostring(i))

        if input == nil then
            break
        end

        highest_input = i
    end

    -- add inputs
    -- NOTE: start at 2, inputs 1 always exists
    for i = 2, highest_input do
        self:AddInput("ScriptVal" .. i, "ScriptVal" .. i, {
            LINKID_DataType = "ScriptVal",
            LINK_Main = i,
            INP_Required = false,
            INP_DoNotifyChanged = true
        })
    end

    -- set slider maximum
    InWhich:SetAttrs({INP_MaxScale = highest_input, INP_MaxAllowed = highest_input})
end

function OnConnected(inp, old, new)
    --[[ Callback triggered when connecting a Fuse to the input of this Fuse. ]]
    local inp_nr = tonumber(string.match(inp:GetAttr("LINKS_Name"), "ScriptVal(%d+)"))
    local max_nr = tonumber(InWhich:GetAttr("INP_MaxAllowed"))

    if inp_nr then
        -- add input if maximum inputs is not exceeded and connection is not empty
        if inp_nr >= max_nr and max_nr < MAX_INPUTS and new ~= nil then
            -- set slider maximum
            InWhich:SetAttrs({INP_MaxScale = inp_nr, INP_MaxAllowed = inp_nr})

            -- add extra input
            self:AddInput("ScriptVal" .. (inp_nr + 1), "ScriptVal" .. (inp_nr + 1), {
                LINKID_DataType = "ScriptVal",
                LINK_Main = (inp_nr + 1),
                INP_Required = false,
                INP_DoNotifyChanged = true
            })
        end
    end
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
    end
end


function Process(req)
    -- [[ Creates the output. ]]
    local input_count = tonumber(InWhich:GetAttr("INP_MaxAllowed"))

    local values_tbl = {}

    for i = 1, input_count do
        -- get input from index
        local input = self:FindInput("ScriptVal" .. tostring(i))

        -- get Lua table from input
        if input ~= nil and input:GetSource(req.Time, req:GetFlags()) then
            -- local inp_tbl = input:GetSource(req.Time, req:GetFlags())
            local inp_tbl = input:GetSource(req.Time, req:GetFlags()):GetValue()
            -- local inp_tbl = input:GetSource(req.Time, req:GetFlags()).Value

            if inp_tbl ~= nil then
                table.insert(values_tbl, inp_tbl)
            end
        end
    end

--    print("[ScriptVal Lua Table]")
--    dump(tbl)

    OutScriptVal:Set(req, ScriptValParam(values_tbl))
end
