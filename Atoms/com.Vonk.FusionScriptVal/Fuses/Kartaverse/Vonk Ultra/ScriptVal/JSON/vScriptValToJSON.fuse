-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil
local arrayutils = self and require("varrayutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValToJSON"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = "ScriptVal",
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\ScriptVal\\JSON",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Casts a ScriptVal object into JSON text.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InScriptVal = self:AddInput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        LINK_Main = 1
    })

    InSort = self:AddInput("Sort List", "Sort", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutJSON = self:AddOutput("Output", "Output", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
end

function Process(req)
    -- [[ Creates the output. ]]
    local tbl = InScriptVal:GetValue(req):GetValue()
    local sort = InSort:GetValue(req).Value

    -- Sort the array alphabetically
    if sort == 1.0 then
        table.sort(tbl)
    end

--    print("[ScriptVal Lua Table]")
--    dump(tbl)

    -- local json_str = jsonutils.encode(tbl)
    local json_str = jsonutils.encode_indent(tbl, true)
    local out = Text(json_str)

    OutJSON:Set(req, out)
end
