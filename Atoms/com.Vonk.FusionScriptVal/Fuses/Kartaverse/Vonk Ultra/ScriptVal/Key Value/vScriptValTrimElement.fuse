-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValTrimElement"
DATATYPE = "ScriptVal"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    -- REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\ScriptVal\\Key Value",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Extract a range of elements from a ScriptVal array as a table.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InScriptVal = self:AddInput("ScriptVal", "ScriptVal", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1
    })

    InFrom = self:AddInput("From", "From" , {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        TEC_Lines = 1,
        INP_MinScale = 1,
        INP_MaxScale = 100,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = -1000000,
        -- INP_MaxAllowed = 1000000,
        INP_Default = 1,
        INP_Integer = true,
        LINK_Main = 2,
    })

    InTo = self:AddInput("To", "To", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        TEC_Lines = 1,
        INP_MinScale = 1,
        INP_MaxScale = 100,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = -1000000,
        -- INP_MaxAllowed = 1000000,
        INP_Default = 1,
        INP_Integer = true,
        LINK_Main = 3,
    })

    InStep = self:AddInput("Step", "Step", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        TEC_Lines = 1,
        INP_MinScale = 1,
        INP_MaxScale = 100,
        INP_MinAllowed = 1,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = 1,
        -- INP_MaxAllowed = 1000000,
        INP_Default = 1,
        INP_Integer = true,
        LINK_Main = 4,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutScriptVal = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        if param.Value == 1.0 then visible = true else visible = false end

        InFrom:SetAttrs({LINK_Visible = visible})
        InTo:SetAttrs({LINK_Visible = visible})
        InStep:SetAttrs({LINK_Visible = visible})
    end
end

function get(t, key)
    --[[
        Returns the value of a key in a table.

        :param t: Table to get key value for.
        :type t: table

        :param key: Key to get value of.
        :type key: string

        :rtype: ?
    ]]
    local value = nil
    local found = false

    for k, v in pairs(t) do
        if k == key then
            value = v
            found = true
            break
        end
    end

    if not found then
        -- error(string.format("no key '%s' found in ScriptVal table", key))
        print(string.format("no key '%s' found in ScriptVal table", key))
    end

    return value
end

function Process(req)
    -- [[ Creates the output. ]]
    local tbl = InScriptVal:GetValue(req):GetValue()

    local from = InFrom:GetValue(req).Value
    local to = InTo:GetValue(req).Value
    local step = InStep:GetValue(req).Value

    local tbl_out = {}
    local counter = 1

    for key = from, to, step do
        if key ~= nil then
            tbl_out[counter] = get(tbl or {}, key)
            counter = counter + 1
        end
    end

    -- print("[ScriptVal Lua Table]")
    -- dump(tbl_out)

    OutScriptVal:Set(req, ScriptValParam(tbl_out))
end
