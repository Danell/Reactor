-- ============================================================================
-- modules
-- ============================================================================
local base64utils = self and require("vbase64utils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValToBinaryFile"
DATATYPE = "ScriptVal"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_SinkTool, {
    REGID_DataType = DATATYPE,
    -- REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\ScriptVal\\IO",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Writes a Fusion ScriptVal blob encoded object to a binary file.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InScriptVal = self:AddInput("ScriptVal", "ScriptVal", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1
    })

    InFile = self:AddInput("File" , "File" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "FileControl",
        FC_IsSaver = true,
        FC_ClipBrowse = false,
        LINK_Visible = false,
        FCS_FilterString =  "TXT (*.txt)|*.txt|Any Filetype (*.*)|*.*|",
        LINK_Main = 2
    })

--    InViewFile = self:AddInput('View File', 'View File', {
--        LINKID_DataType = 'Number',
--        INPID_InputControl = 'ButtonControl',
--        INP_DoNotifyChanged = true,
--        INP_External = false,
--        ICD_Width = 1,
--        INP_Passive = true,
--        IC_Visible = true,
--        BTNCS_Execute = [[
---- check if a tool is selected
--local selectedNode = tool or comp.ActiveTool
--if selectedNode then
--    local filename = comp:MapPath(selectedNode:GetInput('File'))
--    if filename then
--        if bmd.fileexists(filename) then
--            bmd.openfileexternal('Open', filename)
--        else
--            print('[View File] File does not exist yet:', filename)
--        end
--    else
--        print('[View File] Filename is nil.')
--    end
--else
--    print('[View File] Please select the node.')
--end
--        ]],
--    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutScriptVal= self:AddOutput("Output" , "Output" , {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InFile:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local rel_path = InFile:GetValue(req).Value
    local abs_path = self.Comp:MapPath(rel_path)
    local tbl = InScriptVal:GetValue(req):GetValue()
    
    local data = ""
    if tbl and tbl.Type == "File" and tbl.Base64 then
        data = base64utils.base64decode(tbl.Base64)
        base64utils.write_file(abs_path, data)
--        print("[ScriptVal] [Extracting Base64 Encoded File] \"" .. tostring(tbl.Filename) .. "\"")
--        print("[ScriptVal] [Saving File] \"" .. tostring(abs_path) .. "\"")
    else
        error("[ScriptVal] [Error] No File Attachment Present!")
    end

--    print("[ScriptVal Lua Table]")
--    dump(tbl)

    OutScriptVal:Set(req, ScriptValParam(tbl))
end
