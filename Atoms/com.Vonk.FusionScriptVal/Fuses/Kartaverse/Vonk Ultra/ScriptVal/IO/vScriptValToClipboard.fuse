-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValToClipboard"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = "ScriptVal",
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\ScriptVal\\IO",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Send a ScriptVal Lua table to the clipboard.",
    REGS_OpIconString = FUSE_NAME,
    REG_TimeVariant = true, -- required to disable caching of the current time parameter
    REGB_Temporal = true, -- ensures reliability in Resolve 15
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InScriptVal = self:AddInput("ScriptVal", "ScriptVal", {
        LINKID_DataType = "ScriptVal",
        INPID_InputControl = "ImageControl",
        LINK_Main = 1
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output", "Output", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end
        InScriptVal:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local tbl = InScriptVal:GetValue(req):GetValue()
    local txt_str = bmd.writestring(tbl)

--    print("[ScriptVal Lua Table]")
--    dump(tbl)

    -- Paste the text into the clipboard buffer
    bmd.setclipboard(txt_str)

    OutText:Set(req, Text(txt_str))
end
