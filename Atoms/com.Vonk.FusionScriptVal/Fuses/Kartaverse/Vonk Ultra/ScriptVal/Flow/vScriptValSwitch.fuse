-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vScriptValSwitch"
DATATYPE = "ScriptVal"


-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    -- REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REG_NoPreCalcProcess = true,  -- call Process for precalc requests (instead of PreCalcProcess)
    REGS_Category = "Kartaverse\\Vonk Ultra\\ScriptVal\\Flow",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Switch between Fusion ScriptVal objects.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    InWhich = self:AddInput("Which", "Which", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "SliderControl",
        INP_MinAllowed     = 1,
        INP_MaxAllowed     = 64,
        INP_MaxScale       = 1,
        INP_Integer        = true,
        IC_Steps           = 1.0,
        INP_DoNotifyChanged = true,
        LINK_Main = 1,
    })

    InInput1 = self:AddInput("Input1", "Input1", {
        LINKID_DataType = DATATYPE,
        LINK_Main       = 2,
        INP_Required    = false,
        INP_SendRequest = false, -- don't send a request for this branch before we actually need it.
    })

    InShowInput = self:AddInput("Show Which Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InShowActive = self:AddInput("Show Active Input", "ShowActive", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutScriptVal = self:AddOutput("Output", "Output", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1,
    })
end


function OnAddToFlow()
    -- print("OnAddToFlow:")
    -- print("   INP_MaxAllowed = ", InWhich:GetAttr("INP_MaxAllowed"))
    -- If a comp is reopened, we need to recreate all inputs that might have
    -- been saved. The weird thing is, that FindInput tells us that an input
    -- exists while it's not visible in the GUI. So we just call AddInput
    -- again, which will make the triangle show up in the GUI.
    -- A problem arises if, for example, inputs 1 and 3 are connected, but 2
    -- isn't. Since Input2 won't be saved in the comp we first need to look
    -- for the highest input we need. Afterwards, OnConnected() will be called
    -- for each of the saved inputs. The additional input needed to connect
    -- further images will be created there.
    local highestInput = 2
    for i = 2, 64 do
        if self:FindInput("Input" .. tostring(i)) ~= nil then
            highestInput = i
        end
    end

    -- NOTE: Start at 2, input 1 always exists
    for i = 2, highestInput do
        self:AddInput("Input" .. i, "Input" .. i, {
            LINKID_DataType = DATATYPE,
            LINK_Main = i + 1,
            INP_Required = false,
            INP_SendRequest = false,
            LINK_Visible = true,
        })
        -- print("[Input " .. tostring(i) .. "] recreated")
    end

    InWhich:SetAttrs({
        INP_MaxScale = highestInput,
        INP_MaxAllowed = highestInput
    })
end


-- OnConnected gets called whenever a connection is made to the inputs. A new
-- input is created if something has been connected to the highest input.
function OnConnected(inp, old, new)
    local inpNr = tonumber(string.match(inp:GetAttr("LINKS_Name"), "Input(%d+)"))
    local maxNr = tonumber(InWhich:GetAttr("INP_MaxAllowed"))

    -- print("OnConnected:\n   inpNr = ",inpNr)
    -- print("   INP_MaxAllowed = ",maxNr)
    if inpNr then
        if inpNr >= maxNr and maxNr < 64 and new ~= nil then
            -- print("   create new input!")
            InWhich:SetAttrs({
                INP_MaxScale = inpNr,
                INP_MaxAllowed = inpNr
            })
            self:AddInput("Input" .. (inpNr + 1), "Input" .. (inpNr + 1), {
                LINKID_DataType = DATATYPE,
                LINK_Main = (inpNr + 2),
                INP_Required = false,
                INP_SendRequest = false,
                LINK_Visible = true,
            })
        end
    end
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InWhich:SetAttrs({LINK_Visible = visible})
    elseif inp == InWhich or inp == InShowActive then
        local maxNr = tonumber(InWhich:GetAttr("INP_MaxAllowed"))
        local which
        local show      
        
        if InWhich:GetSource(time, REQF_SecondaryTime) ~= nil then
          which = tonumber(InWhich:GetSource(time, REQF_SecondaryTime).Value)
        else
          -- Fallback to showing all inputs
          show = 0
        end

        if InShowActive:GetSource(time, REQF_SecondaryTime) ~= nil then
          show = tonumber(InShowActive:GetSource(time, REQF_SecondaryTime).Value)
        else
          -- Fallback to showing all inputs
          show = 0
        end

--      print("[Which] ", which, "[Max Connections] ", maxNr)
--      print("[Show Active Wire Only] ", show)

        if show == 1.0 then
            -- Show only the active connection wire on the switch inputs
            for i = 1, maxNr do
                local inpConnectionName = self:FindInput("Input" .. tostring(i))
                if inpConnectionName ~= nil then
                    if i == (which) then
                        inpConnectionName:SetAttrs({LINK_Visible = true})
                    else
                        inpConnectionName:SetAttrs({LINK_Visible = false})
                    end

--                  print("[Input " .. tostring(i) .. "] ", inpConnectionName:GetAttr("LINK_Visible"))
                end
            end
        else
            for i = 1, maxNr do
                local inpConnectionName = self:FindInput("Input" .. tostring(i))
                if inpConnectionName ~= nil then
                    inpConnectionName:SetAttrs({LINK_Visible = true})

--                  print("[Input " .. tostring(i) .. "] ", inpConnectionName:GetAttr("LINK_Visible"))
                end
            end
        end
    end
end

function Process(req)
    local which = InWhich:GetValue(req).Value

    inp = self:FindInput("Input" .. tonumber(which))
    if inp ~= nil then
        local sv = inp:GetSource(req.Time, req:GetFlags())
        if sv ~= nil then
            OutScriptVal:Set(req, sv)
        end
    end
end

