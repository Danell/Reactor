_author = "Alexey Bogomolov <mail@abogomolov.com>"
_date = "2020-01-29"
_VERSION = "0.1"


if fusion == nil then
	print("[Fusion] Error: Please open up the Fusion GUI before running this tool.\n")
else
	ui = app.UIManager
	disp = bmd.UIDispatcher(ui)
end

if app:GetVersion().App == 'Fusion' then
    minWidth, minHeight = 270, 110
    originX, originY = 600,500
else
    print('this app will work in Fusion Standalone only')
    disp.ExitLoop()
end

print(string.format("[Layouter] - v%s %s ", _VERSION, _date))
print(string.format("[Created By] %s\n\n", _author))

-- The app:AddConfig() command will capture the "Escape" hotkey to close the window.
app:AddConfig('ToolbarWin', {
    Target {
        ID = 'ToolbarWin',
    },
    Hotkeys {
        Target = 'ToolbarWin',
        Defaults = true,
        ESCAPE = 'Execute{cmd = [[app.UIManager:QueueEvent(obj, "Close", {})]]}',
    },
})
function get_cf()
	comp = fu:GetCurrentComp()
	local cf = comp.CurrentFrame
    if cf:GetID() == "ChildFrame" then
        return cf
    else
        print("Floating frame is currently active. Click on Nodes and try again")
        return false
    end
end

function AboutWindow()
	local URL = 'https://abogomolov.com'

	local width, height = 520, 250
	aboutWin = disp:AddWindow({
		ID = "AboutWin",
		WindowTitle = 'About Dialog',
		WindowFlags = {Window = true, WindowStaysOnTopHint = true,},
		Geometry = {200, 200, width, height},

		ui:VGroup{
			ID = 'root',

			-- Add your GUI elements here:
			ui:TextEdit{ID = 'AboutText', ReadOnly = true, Alignment = {AlignHCenter = true, AlignTop = true}, HTML = '<h1>Layouter</h1>\n<p>Version 0.1 - January 31, 2020</p>\n<p>Use this script to quickly switch between layouts in Fusion 16. This is undocumented feature. Layouts marked as Beta work reliably only on MacOS. Layouts presets are just for demonstration purpose. If you need your own layout, I\'ll do my best to create it</p> \n<p>Copyright &copy; 2020 Alexey Bogomolov (MIT License)</p>',},

			ui:VGroup{
				Weight = 0,

                ui:Label{
					ID = "EMAIL",
					Text = 'Email: <a href="' .. 'mailto:mail@abogomolov.com' .. '">' .. 'mail@abogomolov.com' .. '</a>',
					Alignment = {
						AlignHCenter = true,
						AlignTop = true,
					},
					WordWrap = true,
					OpenExternalLinks = true,
				},
                ui:Label{
					ID = "Donate",
					Text = 'Donate: <a href="' .. 'https://paypal.me/aabogomolov/10usd' .. '">' .. 'https://paypal.me/aabogomolov/' .. '</a>',
					Alignment = {
						AlignHCenter = true,
						AlignTop = true,
					},
					WordWrap = true,
					OpenExternalLinks = true,
				},

			},
		},
	})
	itm = aboutWin:GetItems()

	-- The window was closed
	function aboutWin.On.AboutWin.Close(ev)
		disp:ExitLoop()
        win:Show()
	end
	aboutWin:Show()
	disp:RunLoop()
	aboutWin:Hide()

	return aboutWin,aboutWin:GetItems()
end

layouterWin = ui:FindWindow("LT")
if layouterWin then
    layouterWin:Raise()
    layouterWin:ActivateWindow()
    return
end
iconsMedium = {16,26}
iconsMediumLong = {50,26}

win = disp:AddWindow({
    ID = 'LT',
    TargetID = 'Layouter',
    Geometry = {originX, originY, minWidth, minHeight},
    WindowTitle = 'Layouter',
    Events = {Close = true, KeyPress = true, KeyRelease = true, },
    ui:VGroup{
        ui:HGroup{ 
            ui:Button{ID = "LL", Text = "Load",MinimumSize = iconsMedium},
            ui:Button{ID = "SL", Text = "Save", MinimumSize = iconsMedium},
            ui:Button{ID = "ConsoleToggle", Text = "Console", MinimumSize = iconsMedium},
            ui:Button{ID = "reset", Text = "Reset", Flat = false, MinimumSize = iconsMedium},
        },
        ui:HGroup{
            ui:Label{Text = "Presets:",
        			Alignment = {
                        AlignHCenter = true,
                        AlignCenter = true,
				},
            },
            ui:Button{ID = "LT1", Text = "ND",MinimumSize = iconsMedium},
            ui:Button{ID = "SplinesToggle", Text = "SPL",MinimumSize = iconsMedium },
            ui:Button{ID = "TimelineToggle", Text = "TL",MinimumSize = iconsMedium },
            ui:Button{ID = "HideInspector", Text = "Tools",MinimumSize = iconsMediumLong, Weight = 1 },
            ui:Button{ID = "LT2", Text = "2Mon",MinimumSize = iconsMediumLong},
        },
        ui:HGroup{
           ui:Button{ID = "LClose", Text = "Close", Flat = false},
           ui:Button{ID = "Info", Text = "info",Weight = .3,  MinimumSize={7,20},}
           }
    },
})
itm = win:GetItems()


function win.On.Info.Clicked(ev)
    win:Hide()
    AboutWindow()
end

function win.On.LT.Close(ev)
    disp:ExitLoop()
end


function win.On.LClose.Clicked(ev)
    disp:ExitLoop()
end

function win.On.SL.Clicked(ev)
	print("Save current layout")
    local cf = get_cf()
    if cf then
        cf:SaveLayout()
    end
end

-- Now we can use our flag to differentiate button presses
function win.On.LL.Clicked(ev)
    print("Loading layout")
    fu:SetData("Layouter.Set", true)
    local cf = get_cf()
    if cf then
        cf:LoadLayout()
    end
end

function win.On.ConsoleToggle.Clicked(ev)
    comp:DoAction("Console_Show", {})
end

TimelineOnly = false
SplinesOnly = false


function win.On.SplinesToggle.Clicked(ev)
    if not comp:IsViewShowing('LayoutStrip') then
       local cf = get_cf()
        if cf then
            cf:LoadLayout(comp:MapPath("Reactor:Deploy/Layouts/FU16_1mon_bottom-timeview_Splines-Only.layout"))
            SplinesOnly = true
            TimelineOnly = false
        end 
    end
end

function win.On.TimelineToggle.Clicked(ev)
    if not comp:IsViewShowing('LayoutStrip') then
       local cf = get_cf()
        if cf then
            cf:LoadLayout(comp:MapPath("Reactor:Deploy/Layouts/FU16_1mon_bottom-timeview_Timeline-Only.layout"))
            SplinesOnly = false
            TimelineOnly = true 
        end 
    end
end

function win.On.HideInspector.Clicked(ev)
    if comp:IsViewShowing('LayoutStrip') then
       return
    end
    local cf = get_cf()
    if cf then
        if TimelineOnly then
            cf:LoadLayout(comp:MapPath("Reactor:Deploy/Layouts/FU16_1mon_bottom-timeview_Hide-Inspector_Timeline.layout"))
        elseif SplinesOnly then
            cf:LoadLayout(comp:MapPath("Reactor:Deploy/Layouts/FU16_1mon_bottom-timeview_Hide-Inspector_Splines.layout"))
        else
            cf:LoadLayout(comp:MapPath("Reactor:Deploy/Layouts/FU16_1mon_bottom-timeview_Hide-Inspector.layout"))
        end
    end
end


function win.On.LT1.Clicked(ev)
    print("loading layout 1")
    TimelineOnly = false
    SplinesOnly = false
    local cf = get_cf()
    if cf then
        cf:LoadLayout(comp:MapPath("Reactor:Deploy/Layouts/FU16_1mon_bottom-timeview.layout"))
    end
end
function win.On.LT2.Clicked(ev)
    print("loading layout 2")
    local cf = get_cf()
    if cf then
        cf:LoadLayout(comp:MapPath("Reactor:Deploy/Layouts/FU16_2mon_tl_spline.layout"))
    end
end


function win.On.reset.Clicked(ev)
    print("resetting to default layout")
    local cf = get_cf()
    if cf then
        cf:LoadLayout(comp:MapPath("Reactor:Deploy/Layouts/FU16_1mon_default.layout"))
    end
end

win:Show()
disp:RunLoop()
win:Hide()
collectgarbage()
