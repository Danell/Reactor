MUSEVFX = true
DEBUG = false

print("It's Tool Time v1.0")

--[[--
It's Tool Time v1.0 by Bryan Ray

This script gets a list of all tools in the comp and how long their most recent renders took and displays that information in a GUI. 

Change Log:
	v1.0 - Initial Release
--]]--

-- ===========================================================================
-- globals
-- ===========================================================================
_fusion = nil
_comp = nil
ui = nil
disp = nil

-- ---------------------------------
-- MAIN CODE
-- ---------------------------------
function main()
	-- get fusion instance
	_fusion = getFusion()

	-- ensure a fusion instance was retrieved
	if not _fusion then
		error("Please open the Fusion GUI before running this tool.")
	end

	-- Set up aliases to the UI Manager framework
	ui = _fusion.UIManager
	disp = bmd.UIDispatcher(ui)
	
	-- get composition
	_comp = _fusion.CurrentComp
	SetActiveComp(_comp)

	-- ensure a composition is active
	if not _comp then
		error("Please open a composition before running this tool.")
	end

	dprint("\nActive comp is ".._comp:GetAttrs().COMPS_Name)


	-- Get a list of all tools, IDs and most recent render time
	local alltools = {}
	for i, tool in ipairs(comp:GetToolList()) do 
		alltools[i] = {}
		alltools[i].name = tool.Name
		alltools[i].id = tool.ID
		alltools[i].time = tool:GetAttrs().TOOLN_LastFrameTime
		local outputs = tool:GetOutputList()
		
		-- Get the number of outgoing connections for the tool
		local connections = 0
		for j, out in ipairs(outputs) do
			local inputs = out:GetConnectedInputs()
			connections = connections + table.getn(inputs)
		end
		alltools[i].connections = connections
	end
	

	-- Create the UI Manager window
	win = createMainWindow(500, 70, "It's Tool Time", "main")	
	
	-- Add elements to UI
	win:GetItems().content:AddChild(ui:VGroup { ID = 'treeGroup', Weight = 1,
				ui:Tree{
					ID = 'tree',
					SortingEnabled = true,
					Events = {
						ItemClicked = true,
					},
					Weight = 1,
				},
			})
		
	itm = win:GetItems()
	header = itm.tree:NewItem()
	header.Text[0] = 'Tool Name'
	header.Text[1] = 'ID'
	header.Text[2] = 'Last Frame Time'
	header.Text[3] = 'Output Connections'
	itm.tree:SetHeaderItem(header)
	
	itm.tree.ColumnCount = 4
	itm.tree.ColumnWidth[0] = 150
	itm.tree.ColumnWidth[1] = 150
	itm.tree.ColumnWidth[2] = 80
	itm.tree.ColumnWidth[3] = 80
	
	
	for i, tool in ipairs(alltools) do 
		row = itm.tree:NewItem()
		row.Text[0] = tool.name
		row.Text[1] = tool.id
		row.Text[2] = tostring(tool.time)
		row.Text[3] = tostring(tool.connections)
		itm.tree:AddTopLevelItem(row)
	end
	
	itm.tree:SortByColumn(2, descending)
	
	-- signals
	function win.On.tree.ItemClicked(ev)
		local toolname = ev.item.Text[0]
		local this = _comp:FindTool(toolname)
		_comp:SetActiveTool(this)
	end
	
	-- close events
	function window.On.main.Close(ev) 
		disp:ExitLoop() 
		status = "Window closed by user."
	end

	function window.On.cancel.Clicked(ev)
		disp:ExitLoop()
		status = "Cancel Button clicked."
	end	
	
	
	win:Resize({400, 800 })
	win:RecalcLayout()

	win:Show()
	disp:RunLoop()
	win:Hide()

	return 1
end


--==================================================================================================================
-- UI MANAGER
--==================================================================================================================

function removeUIContent(window)

	if window:GetItems().flexContainer then 
		dprint("Removing widgets from UI: ", true)
		for i, item in pairs(window:GetItems().flexContainer:GetItems()) do
			window:GetItems().flexContainer:RemoveChild(item.ID)
			dprint(item.ID..", ", true)
		end
	end
	dprint('')
end

--=================================================================
-- createMainWindow(width, height, title, id, [link, x, y])
--
-- Creates a generic UI Manager window featuring the Muse VFX
-- Logo header. There is an empty content container into 
-- which other functions can load information.
--
-- Arguments:
--		x, y: Integers. Cartesian screen coordinates (not generally used)
--		width, height: Integers. Size of the window in pixels.
--		title: String. Window title.
--		id: String. Window's scripting ID.
--		link: A hypertext link that will appear at the bottom of the window, usually for documentation.
-- Returns:
--		window: UIWindow object
--=================================================================
function createMainWindow(width, height, title, id, link, x, y)
	dprint('createMainWindow')

	local headerHeight = 0
	local headerWidth = 0
	local html = ''

	if MUSEVFX then
		-- Generate HTML for logo header
		html = "<html>\n"
		html = html.."\t<head>\n"
		html = html.."\t\t<style>\n"
		html = html.."\t\t</style>\n"
		html = html .."\t</head>\n"
		html = html .."\t<body>\n"
		html = html .."\t<div>\n"
		html = html .."\t<div style=\"float:right;\">\n"
		html = html .. museLogo()
		html = html .. "\t</div>\n"
		html = html .. "\t</body>\n"
		html = html .. "</html>"
		headerHeight = 80
		headerWidth = width
	end

	-- Create the window
	window = disp:AddWindow({

		-- Window properties
		ID = id,
		TargetID = id,
		WindowTitle = title,
	
		-- Main window container  
		ui:VGroup{
	    	ID = 'root',
	    	
	    	-- The logo header and optional progress bar.
	    	ui:VGroup{
		    	ID = 'header',
		    	Weight = 0,
		    	--ui:TextEdit{ ID = 'ProgressHTML', ReadOnly = true, MinimumSize = {650, 32}, MaximumSize = {width * 2, 32}, Weight = 1, FontPointSize = 1, },
		    },
	    	
	    	-- This holds the dynamic content
	    	ui:VGroup{
	    		ID = 'content',
	    		Weight = 2.0,
	    	},


	   	},
	})
   	if MUSEVFX then
   		window:GetItems().header:AddChild(ui:TextEdit{ ID = 'museLogo', Weight = 0, ReadOnly = true, Alignment = { AlignHCenter = true, AlignTop = true, }, MinimumSize = {headerWidth, headerHeight}, MaximumSize = {width * 2, headerHeight}, HTML = html, })
   	end

	return window
end

--================================================
-- Muse VFX Logo encoded in Base 64 for UI windows
--================================================
function museLogo()
	return [[<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARAAAABCEAYAAADPq1UeAAAgAElEQVR4nOyde3QU9d3/X7u5LSGQDddVxExAZZEiCSqmrTUDVQlWyaKnNZzWw8ReSD2tLNqerra/MvZUjU8vCfV5anzakuGxtrHtKYlQG6ya4dFKatUEihIvNRMrsFyzQZJsLrvz+6Pz3dlnw5JwVWDf/8yZ3ZnvzHxnvt/v5/r+OOaE54TnhE2TFFJIIYUUUkghhTME50d9AymkkEIKKaSQwvmHlACSQgoppJBCCimccaQEkBRSSCGFFFJI4YwjJYCkkEIKKaSQQgpnHGdcAHG4HC6H60xf9aODeN6Py3OPdD+J/39c7vtsQ7L+S9a/Z7qfz9f3e74972gx2n4Z7Xcz0vd/vNdN4dyE43RnwcQ+MBcuXIAHDx4gRIgQUEghheBUnIpTgUw9U8/UwWW4DJcBmf5Mf6YfIp6IJ+KBqBE1ogbQQgstce0ECRIcvjUN0zANwMDAiDte7IcJE47binbF/wnnxdpLbMeNGzekB9ID6QFwNjmbnE0QDoaD4SD0t/W39bfBoGvQNegCs9wsN8vtfoq1e5rhkBySQ7L307xp3jQvZPuyfdk+iOpRPapDr6/X1+sDM2AGzAD2+7Oe1wybYTN8+u/3bIXD4/A4PMT6TYyDMaVjSseUQnpTelN6E/S09bT1tEGkOFIcKcb+DsX3GzJDZugU3pfb4Xa47fuKbcX/ikNxKJBRm1GbUQtZSpaSpYDL7XK73EAbbbTBUO1Q7VAtUEsttdjjQIy/JOPSbDfbzXaGjx+xL9BOO+329WLtJBuP4n+r/5wtzhZnC6R70j3pHhgMDgYHg9Af6A/0B6A/3B/uD0NUiSpRBdDR0ePu8zz7vh1eh9fhxX4PYisjI4OjxlHjqLHni8zSzNLMUsgIZ4QzwpBRnFGcUQyR2khtpBaGSodKh0phUB/UB3UY9A56B70Q0SJaRANTMiVTihsnCTCDZtAMnsEOSOEjQfppv4KY4Lx48UJaYVphWiFMUaeoU1SY457jnuOGublzc+fmQoFZYBaYkFeSV5JXAuM2jNswbgMMSAPSgARDxpAxZICpmZqpMVxQEBOJtTWzzCwzC9jDHvYc5TgBsbCqpmqqcf+bmJj21sw1c81coIsuuoAOOuiwm8m6Pev2rNshfWf6zvSd0P1Y92Pdj8Eh5ZBySIF35Hfkd2TYXrq9dHsptPvafe0+CAVCgVAAolXRqmjVaXkT/4b1PnJrcmtya+ALZV8o+0IZFCvFSrEC/av6V/Wvgj8X/LngzwWwuXZz7eZa6C/tL+0vPY33dY4iXU1X01WQm+QmuQnKppZNLZsKY5vHNo9thtdzX899PRee8j7lfcoL+/X9+n79NAgeQhGwxqEj7Ag7wjDeP94/3g+XSZdJl0lwxaorVl2xCmatmrVq1iqYqExUJiqQp+QpeQqYsimbMvT39ff19wF11FEHpm7qps7w8aihoWGPnw6zw4wbL7TSSmvc8RZi41ucL8ajNe7MLrPL7Io7r48++uzj0rV0LV0Dl+ySXTL0qD1qjwqh74S+E/oOvK++r76vwg7PDs8OD/yj/R/t/2iHoCfoCXpgyD3kHnKf+vfwcUPMUqE5NIcGYxvGNoxtgAvUC9QLVJipzlRnqiBVS9VSNeSTTz7gNt2m24TsxdmLsxdDtpqtZqvQX91f3V8N/Uq/0q9AT1lPWU8Z7NH2aHs06FA71A4V3q14t+LdCuis6azprIEutUvtUmFIGpKGpI+qN1I40zhlFpCYZi0kZ2uhc3qdXqcX8qvyq/Kr4HPNn2v+XDMskhZJiySYznSmA1lSlpQlxbUjISHFXUBMNIm/f9yRIOhE5agclaFX6pV6JXhbfVt9W4U/q39W/6zCc9ue2/bcNjjQdKDpQBNQSSWV2JqxhROdGMXE/LU9X9vztT3wFeMrxlcMyNQytUyNWD+HjJARMmDNsjXL1iyDF5QXlBcUMKvMKvN0CkjnCByljlJHKVyjX6Nfo8ODdQ/WPVgHU6Wp0lSJmGY5pA6pQyrUy/VyvQzVq6tXV6+G/pb+lv6WE7iusHBYFrnYeAwQIAB5VXlVeVUge2SP7IGbS24uubkEvIbX8BqQo+aoOSo4cf7bP2vdZwxn+zi07lvoFUKx2a3sVnYroMu6rMuwqXtT96Zu+Gf9P+v/WQ+R9kh7pB17HFrz28dFUx9maRbvP9HCW0wxxbZFa4Zvhm+GDz5V96m6T9XBAn2BvkCHmdJMaaYEbtktu2XIkDKkDAkcskN2yNjvP9k8Lbbi+7GOi+gRPaJDj9Qj9Uiw29ht7DagTWvT2jR4ce2La19cC9vbt7dvb4duqVvqlmyLSaJl7XyzVJ1rOHUCSKGj0FFI7MMf2zK2ZWwLLKlbUrekDr6kfEn5kgIFcoFcIINTd+pO/WSvehYjYaCKiXCbtE3aJsGvtv1q26+2wd/Cfwv/LQxDviHfkI+THnhiAfqvuv+q+686uEK/Qr9CZ/hEYmmef9T+qP1Rgx8s+8GyHyyDocqhyqHKk3v08wFp/jR/mh++0feNvm/0wVekr0hfkcChOlSHOvx4YUhb6VjpWOmA3eHd4d0n8H5jJm3rfaZVplWmVUJRVVFVURV8Of/L+V/Oh6uNq42rDcjSs/QsnbNPoDjVsASUqBSVohJ8oH2gfaDBU/pT+lM6NIxpGNMwBg6HD4cPx7lqPy4WkmECiNhaAkA22WQDhVqhVqjB0rKlZUvLYIG0QFogwUQmMhFwqk7VqXLmvgcxDyooKNBDDz3ATmmntFOCP3X+qfNPnbZgeFA/qB/UwWwxW8yPUf+ncGI4YReM+OBjC6ElYU9sn9g+sR2+2vzV5q82g0/yST4JxhpjjbEGw10f5ysSNMlMMskErtau1q7WbFPnL+Vfyr+UYUPthtoNtRD2hX1h31H6f5TIkDPkDBnGNI9pHtNM8onGmrjGymPlsTI4O52dzs6TeuLzCsKSl3N7zu05t9uxFcng0lyaS4NMNVPNVI/jOonfgaUhZrZntme2w03Bm4I3BaGyubK5shmmydPkaTLDXB7nLRL6QVh+LpYvli+W4ZvSN6VvSnCJdIl0iQQ/r/h5xc8rIFgYLAwW2ued6Hg8UQy7XkKMXXooPZQegsuNy43LDSjfUL6hfAN8JvczuZ/JtQ0kqKiocQ2faUFUXM9aF8Q6cRVXcRUw15hrzDXgZu1m7WYN6tvq2+rb4MXwi+EXw7YlJRYDZCFlGTk7cNwCSMzEK3zJlsaVV59Xn1cPdz9898N3Pww3yzfLN8uQaWQamQb2h5ZooktmyjtbkShgjWTCTuwXSxOYYkwxphjwTfWb6jdVcOku3aXDb/gNvwH6Xf2uftcJTHzieon3lew4gW666R5F+yn8GyL4dATBIwbR3w4cOI7jOmIcWuNSuNiWqkvVpSp8c8U3V3xzBUzQJ+gTdIYLHslM5mc7EmO9rHGVFInj0dqKYPil8lJ5qWwL5I888MgDjzwA+6R90j6JuBX9DCHxelYwf66aq+aqcOvKW1feuhJub7299fZWuFC9UL1QjbPAjVYRTHBdjYiR5vXE9hL/T9gXrvkrjSuNKw24TL5MvkyGv0h/kf4igebQHJoDjFqj1qgFs8lsMptGcZ8pfCxw3AKIMHkJDS/LyDKyDKgoqyirKIOb1ZvVm9WjCB4CiR+eJYH3a/1avwb79H36Ph32KnuVvQoMqAPqgAoRNaJG1Lgg0WTtJgyAmM8ycQJK2B8We5Lguxx2HREdLs5L2BemTKGYXChdKF0owQRlgjJBgXQlXUlXjtJuwv44dZw6ToU7tTu1OzXYv23/tv3b4JngM8FnghBti7ZF2zj9SIhBSeFjAkvzEzEn1xrXGtcacFfzXc13NcMEY4IxwSD5Apzw3UXkiByRocvoMroMOzaix+gxegyIGBEjYkBUjapRdfj5w/at8ThsHI4wzhLH1TDBKIkgHbuONa+kkUYakKan6Wk6TJGmSFMkeztGHaOOUbGDXZP0jzh/kb5IX6RDV3NXc1czVDdUN1Q3QE9pT2nPmQzStgQQR7uj3dEOUqVUKVXC17O+nvX1LPis8lnlswpkkUUWDLd0jBKmYiqmAn1yn9wnw2HjsHHYgIPyQfmgDB/qH+of6vZ3IYKHheAwUZ2oTlQhz8gz8gwYxzjGAelyupwuM7JgKGC953HGOGOcAT7DZ/gM8Kpe1avCf679z7X/uRa2Nmxt2NoAgwwyePyPm8IZxolnwVhpcrIsy7IMt224bcNtG2xXwkgSc1gKS2EJXtFe0V7RYFPBpoJNBbCjc0fnjk4I7Q3tDe2FyNrI2shaiC6MLowuBB7hER5huAaQkE4YwxKWsAS4gAu4IO68vexlb9x5b/EWb8X50MX/idcR+5vZzGagggoqsDXRWketoxYcax1rHWvtgTZl2ZRlU5bB1fLV8tUy3MIt3ALMlmZLsyVIM9KMNCPuOgmaqltxK24FvqZ/Tf+aDm93vt35die8HXw7+HbQ1oBTPtHzDNb3OF2aLk2XoNKsNCtNmGxMNiYbjDgORczDO8Y7xjsG/Ik/8Sdga8XWiq0VEHQH3UE3DMwemD0wG8xKs9KsBPMB8wHzAfv6ydJ6Y1jOcpYDRRRRFHeesPgkWNgcex17HXuBLWxhC7HxGYO4TiONNMa1J65jjXfneud653pwLHMscyyD3FBuKDcEs9fPXj97Pdyk3KTcpMCn9E/pn9JhrDRWGisdpd9Edo01nm/RbtFu0aD9gfYH2h+AP7r+6PqjCyJEiIzitY0Ww9KmLUXAUeWoclTBFcoVyhUKfKv6W9XfqoYrpCukK6S4WI5kSMw6shS7kBJSQgq0G+1GuwF/1/6u/V2DN/LeyHsjD3a37W7b3QaHKw9XHq6Efk+/p98D0YpoRbQCWMta1kL6wvSF6Qshuya7JrsGJj8w+YHJD8AlDZc0XNIAV+denXt1LhRJRVKRBBcoFygXKHbW2DAkvA+n7JSdMlzO5VwOqF1ql9oFjwUeCzwWgI2+jb6NPuhv6m/qj7OIpFwzHy+csAAyuW1y2+Q2+NKqL6360ioYz3jGQ/IJz9Iw9mv7tf0arJPXyetkeLro6aKni+Dw5sObD2+O48cQ+f9nGgm+xOOGDx8+e1e4SLpd3a5uF7zrftf9rhu2LNyycMtCuCP/jvw78mGZscxYZtjBYsNg9aukS7qkQ7lSrpQr8EjLIy2PtEC/0W/0Gydx3ymclcgIZgQzgrYCIDTCpK4Ua+ERFsc/GX8y/mTAumXrlq1bBv/a8q8t/9oCkc2RzZHNRzk/4fs+bUjkBTlRCJ4SC0e8R7xHvLCrfFf5rnLYWru1dmstLPYu9i72wsoVK1esXAEXciEXgq2hJ1hqspVsJVuB5dJyabkEr6x+ZfUrq6GTTjpPwW3HkGB5FJau+fp8fb4O9224b8N9G+w06hE9eNb7F4KnmO6alWalWYFn857NezbPVnB6Hu95vOdxO806xrtSTjnlx7iOhcMc5jAQrAxWBithR2BHYEcA/lT1p6o/VcG0B6Y9MO0BuM68zrzOhJv0m/SbdLjMuMy4zLBj1pK6bBJc1n7Nr/k1GPvY2MfGPgZPLX9q+VPLIdwSbgmfQFZZCqcXx82E6ix2FjuL4brQdaHrQnC5drl2ucaIgsch9ZB6SIWfFvy04KcFUL+6fnX9ajgsHZYOS0AVVZyL6Z0JmqFIY93l3eXd5YVHn3r0qUefgiflJ+UnZehX+9V+Ne78RNO05cOVDdmQDZjlneWd5cUmeEvhvIAQbKVSqVQqhRv0G/QbdEhT0pQ05SgnWBq8SPv9o/FH44+GPR472zvbO9sh6oq6oucgM+WwLJF66qmHHnePu8cNjeHGcGMYHul8pPORTghqQS2okTzt2NqfIc+QZ8hwfev1rde3xqUvn+R9Drtfax6dE5oTmhOCQFmgLFAGs5RZyixl9ILHEf2IfkSHp3map4F7F9678N6F8JP+n/T/pB9eL3299PVSOFJ5pPJIZRy/yymCmP8GCwcLBwvBCBgBIwC/vu/X9/36Prin4p6KeyrgMeMx4zEDdqu71d0qI8egWP/nKrlKrmJbim/77W2/ve23kNGU0ZSRig352GHUY0X4ZMcGxgbGBuD6suvLri87RtS+9cELX9xvld8qv1Vgc2BzYHMAhgqHCocKOfdjC8TzCY1O7FsurJ7inuKeYlj/1vq31r8FL/IiL4KtcSUGDVoDbaI8UZ4og9wqt8qt4Gx3tjvbz/CzpXDGIUzygpny02WfLvt0mR1kmBSWpviq8arxqgG/XP/L9b9cD93B7mB3PMPoeTIeE12VkWAkGAnClqotVVuqYL20Xlov2YRayZBOOunAIm2Rtkg7BbGoFk9HbMG1gkunhaeFp4Xhnvx78u/JtwWPEYNJrXYM3dANHaoWVi2sWggPNz7c+HAj7DB2GDsMGJAH5AHZtnCI/ontn2KmZrPNbDPb7HajoWgoGrJ5Qdbdt+6+dfdBoDvQHeiGv2p/1f6q2USUSbO4rOcdL4+Xx8uwUl4pr5ThxvYb229sh7T6tPq0+jjXVgofKUYtgIgPZXrT9KbpTTBLnaXOUrEHSuJAsH5/R3tHe0eDhvUN6xvW25Kv8MXFPvRz1DcXe84Rtt1yt9wtw5OOJx1POmyLUTLJ32k4DacBVylXKVcpML5+fP34+jP5ZCl8FBDjJduf7c/2wzXqNeo1apzlI8mCdIQjHAF+0/mbzt902sHeiQvyOT8OEwSPxOcWDKjP5D2T90webJO3ydtkRlzoBb/RTNdM18x4C8YoETs+gbI+pyWnJacFvlb3tbqv1cF8db46Xz2KxSOBeTZKlCg20ez3HN9zfM8BG5WNykbFDpqNUeN/xIi9F8viE5EiUkSCVk+rp9UD3y/6ftH3i+AP0h+kP0jQJ/VJfdIxGrTmzTw9T8/T4a5Vd626a5UdI3OiQbkpnFqMKIAIy4fwPc70z/TP9NvpXsmi0cUAaFab1WYV9up79b06Zz5d7SyBMHW+qb+pv6nDdmm7tF0i+cRn9fs0pjENmOKd4p3iTUn25ypipnmrZseEtgltE9ri5NNk2SIWRGmV1nBruDVMzId/rgocJwrRz12hrlBXCJ7Tn9Of02GIIYaOcV6OkqPkKHBp/qX5l+YTG7fHXfTPmh8FkV2pWqqWqlAqlUqlkp2NMyx92oLIQnlFfkV+RQZ1vbpeXQ/b3Nvc29wQbYm2RD/GsRAxQURYjC2Bal/hvsJ9hbB279q9a/fCetazHug1eo1e4xgNWgNE8LrcteauNXetgcmVkysnV6bmy48aI1tABJ9BpaPSUQnTyqaVTSuDDDVDzVCTnyZKM2wr2FawrQDMYrPYjC+2lcJRIfptu7Jd2a4cxQebMOGM08Zp4zSY6p7qnhpPvZzCOY2J/on+iX4Yp4/Tx+nJjxPfzw59h75Dh8PaYe2wxskHW5+jiFlKLKbNHY07Gnc0whH1iHpETX6eiP3IN/KNfCMuCyVZVlCS64qFVyqXyqVy+GLJF0u+WGLXWhmWtirSh63fd0g7pB0S/Ef3f3T/Rze81/Re03tNYJaapWbp2ZMlF7NMia1lqTniOeI54gFttjZbmw31ar1ar8KAMqAMKMdo0Oqnq6SrpKskuK36turbqiG9Nr02vdbOfkwJJGcWIwsg1oLnLHQWOuOKyDkMh+Ewkp/Wq/fqvToc9B/0H/QzvEpmCkeFcHXtW7Zv2b5ldrR6DAkumQwyyMAuFiZ8xqmBdI7CGj+CT0YUW0sGUT16j7xH3iND1B11R90py0cyJAZ/HgodCh0KwYfyh/KH8lFOEOPRMunnaXlanmYH6x+va0uM51ubb22+tRkKpAKpQGK4KzbB8rxb3i3vlqFmfc36mvXwTtM7Te80xfEmnSuwLEQ9wZ5gTxDqKuoq6irgL+pf1L+ocfw0SSAU52UsYxl2DSSBs0VAO1cwsgBiZVeIBS1bz9azdUYkkBELpyg+lMKxEeMfsQbYoG/QN+g7RhS6NQEJQTCzOrM6s5pzh8kyhaPDEkCyGrIashrAqTk1p3aM4y1XgCiD/pGlt59tsCyJfUaf0WfAYfWwelglOZOsNR5jrpommmgaWbOOuWis/0X12Ru5kRs5Cj9Q4m3qYT2swxPaE9oTGrxW+1rta7Vguk23eS4qIAnB/KKKeG1jbWNtI+yUd8o7ZYYTyyWk8V5gXGBcYMBtHbd13NYBWe4sd5b7+GN3Ujg5jNoFIwQRUcU1KXOgBREDEglFQpEQI6dRne9IqCIc9of9Yb+dNpkMwuc7VDRUNFTEqE2+KZxdiGnQ1jjskrvkLnlk03OsJo2RY+QY4Kh31DtSwcpJkVjb6sPaD2s/rLWLRIpq1omxWX1Kn9KnwCtbXtnyyhYYqhmqGaohNq6TatYi5kNKk9IkWFy3uG5xHUxVp6pTVZIrFNbvfzP+ZvzNgE1Fm4o2FcGQMqQMKaeuPz5uGJa8YClonQ2dDZ0NsM6xzrHOAYeVw8phJXk7gs6gRC6RS2S4TLtMu0wjFaN4hjGyACKisuvNerMeonpUj+qMXMzK+t9sMBvMBoaXiU7h/yA2sERamnC9jJaquIYa4ia8FC/IuYVEDfqQ75DvkA/6tD6tT0t+nmCMFFlrmeHMcGY45aIbEdZ8NWgMGoMGrH9g/QPrH4ANxgZjgwHvG+8b7xvwrvGu8a4Bv1R+qfxSgeeKnyt+rjgu5k1QpifTrC1BRwRFXqdep16nxlm2hKCTMN92q91qtwpPbXtq21PbIFQYKgwVHqX9cxSxdHTLwhRtijZFm+Cl4EvBl4I2ge5IFuRJ6iR1kgo31N1Qd0PdMXh0UjgtGJEJNSa5JzAKjhpCA7DyvlM4zbDSHT4u6XUpnCKI4GJrQTtYerD0YCnsvX3v7Xtvh0nSJGmSFHd8QozAJ9RPqJ9Q4aItF225aAv8M/jP4D9TQahJkWix2F26u3R3KVQFq4JVQZhYObFyYqVdO+eAekA9oMZZICzFwQyaQfNY/VxKKaUwt3Bu4dxCuLju4rqL6xhuYU5Is31Ne017TYPX61+vf70e21ISIEDg1PXDxxXJLEoi9nBD04amDU3w6ZJPl3y6BCYwgQlHacehOTSHBp/iU3wKeLLzyc4nO2Gve697b6q0xWnHyZD2pZBCCmcIiVkS3bXdtd218IbxhvGGAaZkSqZEUsvkhdqF2oUa3FRyU8lNJZARzghnxFN8p3zfx4Sostrn7/P3+WGXZ5dnlweCRtAIGjDkG/IN+Rh1kL3ob0FktqBjQceCjpE9qKLkwl/y/pL3lzzo0Xv0Hn301z3nYQlobzS80fBGA7wmvya/JpNcoLME9enydHm6DJerl6uXq9jFRVOWwtOKlACSQgpnEYSLbqhyqHKoEl50vOh40QE9ao/ao5JUABGm5Vu1W7VbNfiU61OuT7nA2eRscjaRctkdL4RFShC5JRALjhbjasbVjKuBy6XLpcslkhNkWQuloCZvlVqlVsmuVpvS1C1YFsLeht6G3gZ4wfGC4wUHDBgDxoBxlOOtfh1jjDHGGFDUUdRR1AHOBmeDs4FUyMBpRkoASSGFsxDCpdnqbnW3uqFNaVPaFEakqBY+72+Z3zK/ZcKnQ58OfToE6eXp5enlcdlYKRwTyZhVj/d8j+SRPBJcaFxoXGiQPFjfeq+CUG5/+/72/SkX63AIwdAi2vuH/x/+f/jjmH+TQARrz9Zn67P1uGzPFF/OacU5J4AkprXF0uAki9F1hHS4FFI4m9Ad6A50B+B3nb/r/F0ndBvdRrdxlAMTmFIlVVIlFdbkr8lfkw8VuRW5Fblwke8i30U+yPBl+DJ84PA7/A5/3Pixth93xMb/CVKiJwpiie2dqvlCpIPmqDlqjpr8uKgW1aIavCm9Kb0pwUBgIDDwEcR6JBNQE/tjtP2TmKZ80oRgwodlCQ773Pvc+9zQQQcdYAf1JhH0pqnT1GkquIvdxe64IOIUTg9GDEI9a5GQPizy8icpk5RJCuS35Lfkt8Cgf9A/6If33O+533Pb5bpTQZwpnBWwJtKtTVubtjbB0/LT8tMyLNeWa8s1SFfSlXTlKOdZgogYHl+Xvi59XYIlyhJliQJ/l/4u/V2Ct4veLnq7CHrae9p72u2skIHigeKBYjB9ps/0YU/sFs9IrHhZwu8xX7wIThdZCon8JNZxIs089n9ie+J6Isjd2k+rTKtMqwRXjavGVQP9gf5AfwCiNdGaaI1dzM/pd/qdfrs/Iv6IP+K3q1L3NPU09TTBocChwKEAdBV3FXcVw2DtYO3giQTlWxDBjxc8fMHDFzx8jKKeFgblQXlQhs7Ozs7OTsCHDx9nXkO35lWny+lyumBS26S2SW0wwz3DPcMNA6UDpQOl8G7pu6XvlsYRuIn3lMxiJD5EaztGHaOOUWFGeEZ4Rhjy2vLa8trAUegodBwr20eYiCwIV8pQyVDJUAn295ckvTlHypFyJJjgm+Cb4IMPXB+4PkgppqcN564AIiRX60ObHZodmh2Cb5vfNr9twqyOWR2zOiDSFemKdMFL816a99I8qM6rzqvOg/2u/a79rhRjZAofc1gTdtgX9oV9oLk1t+aGi1ZctOKiFVBilBglBjglp+SUkjeToWQoGQpcql+qX6rDJVzCJUC0OlodrYZoV7Qr2gXRDdEN0Q0Q2RDZENmALRCsYAUrgBJKKAFWsYpVDHcJmZiYYHaZXWZX3P/ivITjqKOOuuTtMI95zLPPF4KPo8xR5iiD9Nb01vRWiMyLzIvMA7POrDPrbAEgphpbgpwQiCK5kdxILgzOG5w3OA8OVR+qPlQNO/J35O/Ih03uTe5NbmhVWpVWBQa0AW1AG/1rc/gcPocPcrfmbs3dCg7doTt0kqbd99JLL7C/Zn/N/hpG5GE6bbDub453jneOF75d9u2yb5fBpV2Xdl3aZWcFvfydl7/z8nfgpxU/rfhpBexlL3uP1p6Ypy3XibvF3aKFpHwAACAASURBVOJugbu23rX1rq1w429v/O2Nv4UxyhhljGK/1+NFupwup8uMWIROCD4T1k9YP2E9I1cbTuGkcM4JIEJgECbALDVLzVJB+bPyZ+XPcJV+lX6VHldN0hJQlmhLtCUavCW9Jb0lwf+4/sf1Py57njtXIKroDitqlWiStH4XE6OjwlHhqCBVa2a0qKSSSkivTq9Or2ZkPhcRdV/gKHAUEPNhjwihAVvvZZ+8T94nw4+7f9z94277O79Wv1a/Vod0KV1Kl+LOT3zv4j4sht00OU1OkyGNNNJg+AQ+Wp6aZEgW83Cy7SakrQ67TrL2EzVj67yJ2kRtogaXKpcqlyrwGekz0mck+NXWX2391Vb4Xfh34d+FYcAz4BnwjBwb4qxyVjmrIFfP1XN17H4VC17CfQxqg9qgBn1FfUV9RR+dYuTyu/wuPyhTlanKVJhfN79ufl3cAVZ/L5YXy4tleFt6W3pbgnX+df51/qPMp+I9tdBCC8he2St74dayW8tuLYMsPUvP0uOOP80CgRBUcgpyCnIKgGKKKT591zvfcc4JIDFYknVOMCeYE4QCpUApUI5SxtqaYISp+nL9cv1yHdICaYG0gM3oeq4gvSG9Ib0BMo1MI9M4xoGiX7R0LV2zTdYpjA6OsCPsCMMYbYw2RmNEJmBRvCxWxGyUSFzohOD9fvn75e+Xw4MVD1Y8WAF35t+Zf2c+3MzN3AyMk8ZJ4yRGzLo4a3Gq71+0Zy2Ak4xJxiQDVmortZWaHWvwbPjZ8LOjEAwcDY4GRwNkyVlylmwzcyazbAjLjsh6oZ56PgJG25zinOKcYsgvyS/Jj3dpCFj9JBScy7iMy4C0vLS8tLzk86nT7XQ73TCja0bXjC7I1DP1TP30PstRYb3fzPWZ6zPXk2KWPs0454JQE5FWmFaYVgjparqarh7lACGBWx+eOO5cDUodrBmsGayBPrlP7pOPcaDoF6GJne0L0pmGSM8cgZ9DIEKECDDIIIMncdmYZmwtUMG2YFuwDWreqnmr5i34QfcPun/QbRNZhdWwGlYZ0TQ9IvPx2Y7E8vZJyt3HYI0Ht+SW3BJ8fsXnV3x+BYyvH18/fhSCgXhPPXKP3CPHxcwko1633k/MVfMRodff6+/1x4WeCAEkoZ8EZX2n1ql1anYRxETEsom8ptf0wi5tl7ZLiyN0O8PfXa/Ra/QasKdmT82eGkYeFymcFM5ZAUQwEMYGdjKIhVUwF4rjz1Fin365X+6XoUfpUXoURkz7G6OP0cfokF6fXp+eqiEyaqSVp5WnlcNYZawyVmFEAW6AAQbitieL2MRujYPelt6W3hbYrG/WN+tw7/J7l9+7HNYsW7NszTJ4Rn5Gfka2QyIOa4e1wxoMyUPykAwRKSJFJFtQOt1bUYrgpLdWddRYcczE6xkRI2LE7VvPGSs5MdICZAkMXrx4gYKGgoaChlG8H8sV0Sv3yr0yyWM6rHGYoWfoGTq4fC6fyzdy+6cLvZ5eT68Hnqx4suLJCpuKXgjOYn55WXtZe1mDjZ0bOzd2JhdARLaL6I8XVr+w+oXV8Lz0vPS8BL1ar9ar2UVNY+/rZLcJ7/9D40PjQwMapAapQYLtoe2h7ceq4ZPCKcG564JJ4agYaBloGWiBXrVX7VVJ7iO3IEzN2Vuyt2RvgSPaEe2IdgZv+CxFVm1WbVYtjM8anzU+a+Tju+iiCwjXh+vD9eBQHIpDOXW+/phAYk30B10HXQdd8Iz7GfczbnjO/5z/OT9MypuUNykPPG6P2+OGyfJkebIM47Rx2jgN6KSTToi4I+6IGyikkEJGFLBE0GVsQRfVnMXvoh0RlLia1awmphgMy34Q1xPBr6I9q9/ENlPL1DI1u2hfZFtkW2QbmH7Tb/qHW6hEDZZsI9vINmCBscBYYMB1ynXKdcpRqg9b1x1njDPGGVCwumB1wWrYVrmtclslmLVmrXmUbJlobbQ2WmsXTRPZPiL2JrE/s+VsOVuGid0Tuyd2xy3cZ3iBFFlPr3he8bzigdXSamm1BLNXz149ezX0L+xf2L8Qtju2O7Y74EDxgeIDxSSP3RAuDmu7X9mv7Ffgh4U/LPxhIczV5mpzNZi4euLqiattV81Jw6qdJbJkdjXsatjVANtrt9dur4Ved6+7N5V+e9qREkDOMwy0DbQNtEFwZXBlcCX2xJAYlCdMzKpbdaswYeGEhRMWwj72se8juO+PO2IuO2sBzXPnufPcMLVuat3UOka0NB2UD8oHZQhvDm8Ob+a0+55jwdrWQjagDqgDKuwK7QrtCsEudrGLON6HRD4Eke5oBQ+OCGEiaKCBBuzns2qhDINoX9Q2SXbcNraxDXiLt3gLWM5ylgN55JEHPM7jPM7wNGCR9iksneL5RM0rP3788OayN5e9uQzmt85vnd8K4xnP+KPchsgy8rR6Wj2tcd+D9ZzDBElrvAkX2eCKwRWDK44SdGl9N5lyppwpw3RjujHdAKqoomoU/X6aIGI5Olo6WjpawCg2io1i7H5OLI45Soiip+K0l2peqnmpBnt+EunHJwvx3Yrv0hKAY/QL1vtP4fQiJYCcZ4i4Iq6ICzrUDrVDtYPahMaYiFwtV8vVYGbezLyZefCW9pb2lmZPFClYEAuYNaFdXHVx1cVVkJefl5+Xn/y0WDnxfxsWGAwNhgbPoOl3pOuMWEztbEGy2IoREKoKVYWqoL+sv6z/GOmfDtkhO2SYrExWJivgWO5Y7lgO0XA0HD2aBcuKEdqt79Z369Aj9Ug9EmQZWUaWwTCBVVQ1nqPN0eZokKFlaBkaDFQOVA5UHv9znSwSvwuzxWwxRyOIjtDO8f6fwtmNczYGJIUksNLK3ln7ztp31o4cjCqyZeZ3ze+a33UMYqvzHZYAIhYKUVNijDRGGiMlP02kV77d+Hbj240QLYwWRgtHzwSZeFwiU+eJMoKer0hkQhXF34SFaBgS0tiz1Ww1WwVHsaPYMYr0zb3qXnWvCkEpKAUlhlvKRPuW62q2OludrcLEtoltE9uGNZdCCmcVUgLIeYqOmo6ajhrYq+3V9mrHONCa+OYr85X5CkwpnFI4pTC1oA2DZTPOa89rz2uHa/Rr9Gt0cOL89yATC0tC1tVB5aByUIG3eZu3IZa9MmoLiCX4JKVIT/G2nBgs18lQ4VDhUCEMSAPSgHSU48R7tSwsgjfF4XV4Hd6RL9Nd2l3aXRpH4Jks6NX6bgRV+FxprjRXAke5o9xRnhqPKZydSAkg5xlEls8B9YB6QIUd6g51h8rwiU9MrNb2Yi7mYqC4rbiteDSUyOcbLOKwIq1IK9JsRtFhSBBEhEtrt3+3f7f/BIJOrRiJ7MrsyuxKmCJPkafIMLZlbMvYFmKCTorRd3SI9ZMluEX9UX/UDxE1okbUkc93Kk7FqYCj3lHvOIaLUgiYg65B16ALXtnyypZXttgU8DEkjEORlXaDeYN5gwljwmPCY87B9yqo3gUvzukSsISlShA0xvhYTvV1LBd3Wm1abVotpAfSA+kBSGtKa0pritu2pbWltUFaVVpVWtXo78dR66h11IKz3dnubB+9AHzcz2FZVJ21zlpnLThCjpDjJFzFqRiQ8wxCUx6QB+QBGV5qfKnxpUa4YdUNq25YBS7dpbt0hk18Ijju5tabW29uBb1Fb9Fb4JDnkOeQ5/zx1Q4LyrQsHzntOe057bB0w9INSzfAGHmMPEY+SgOW4DGoD+qDOrxkvGS8ZECfr8/Xd4zaHsOKo1kTzBWVV1ReUQkrylaUrSiDfDPfzDdhz3f2fGfPd+CJxican2iEv3v+7vm7ByLBSDByHrynk4aIgrSCHs1ms9lsHsV5IqjbCuoR7y1xfMTep2Vp2ebd5t3mhd3ybnm3DAVagVagMZyPxwqivUa+Rr5Ghk+Uf6L8E+X2+z3bkBi8LQjyFpuLzcWmvf9M2zNtz7TBXnmvvFdm5NoyiddJHD+WAvUJ+RPyJ2S4NuvarGuz4I36N+rfqIe/qn9V/6rCEEMMHeN+xXuNLfhW8OrYmrE1Y2tghjpDnaHC7D2z98zeA9Nyp+VOywVXq6vV1RoXWyQYiC2el8iayJrIGnh9/evrX18PL3tf9r7shaHgUHAoaD93pivTlemCzxR+pvAzhTBrw6wNszbA/9b8b83/1sCb9W/Wv1lvx/qNpIjEXLoiSDuBUXi6d7p3uhdubL2x9cZW6Krpqumqgc3ezd7NXjjScKThyCjS0AVSAsj5Cqs4nyCkeq/rva73uuBy6XLpconhvmhrfy5zmQtcv/j6xdcvht+3/b7t923nHmV9UoiFydo6Sh2ljlK4turaqmurYMGGBRsWbGBEXocP1A/UD1RoaWxpbGmEaGW0MnqsYEJxXWtimOKa4prigns77u24twMKVxSuKFxha1qCgdJT5inzlME96j3qPaod7JrC0ZG4wMQIwETtmBEo4kUwKlvYwhZGdoFZ/wdDwVAwBH+d99d5f50HkiEZkhHXXgLy1Dw1T4XPS5+XPi/BTn2nvlOHD/mQD0/guT8yiGwoKzbt2uC1wWuD8G392/q3dTuNekrrlNYprVBdXl1eXQ4DxoAxYBzHdRIW1MnaZG2yBt8u+XbJt0ugsLWwtbAVjBKjxCiB9yvfr3y/EjpqO2o74tKoky3g6YXphemFML9mfs38Gri9+fbm25thvjRfmi+Be4V7hXuFXdJAWDZM2ZRNOa4hIWha97moZFHJohK4W71bvVsFQzEUQ7EPl3yST/LBt1Z9a9W3VsFFykXKRQpc33p96/Wt8PADDz/w8APwWtVrVa9VjTxPC8EmNg6sbKuCtoK2gjYI5AZyA7m2ANxn9Bl9BoSKQ8WhYnie53n+OF5LygVzniFG6WxJ7vvb97fvb4dn9Wf1Z3UYUofUITX5+WK+ENVWZ/pm+mb6zp4y7ScNsTBZE9q04mnF04rhjnl3zLtjHozVxmpjNYZnXVgTiiir/oL6gvqCCrtqd9XuGkVV1djEZ1lIpAapQWqAS6VLpUuluBIDCcR6BRRQAMyWZ8uzZVtTO+Fy5+cZMtQMNUO1KdOHIYGpUywwzmJnsbM4uYYeI0q0tkPtQ+1D7dBU1FTUVAQH1YPqQTX5fQnB5Dr1OvU6Fa43rjeuNyDNk+ZJO5ssIWJCEeNKBHNbac2idtEN2g3aDRrMKpxVOKsQW6AYAbGFVNS2ane0O9phUd2iukV1MEeZo8xRbAEzTUvT0rS48xMtJwnB3VmlWaVZpXDrqltX3boKHmp+qPmhZrhRvlG+UYZJ+iR9kg7pRrqRbsQJssLiYb3HRP4a8XvUiBpRI67acwJivwu+HMuCcpl+mX6ZDvevuH/F/SvgyvCV4SvD4Kxx1jhHU1LDqmVV4CnwFHjgvg33bbhvAxSrxWqxCmlKmpKm2IcLor/jdZWlLCDnOaKBaCAagGd5lmeBJflL8pfkwyxmMetoJ1gDZ6Y0U5opwVe1r2pf1eAhx0OOhxzQ7en2dJ/LLhlLYx3bNrZtbBsopmIqJswx5hhzDIZpMIlZEh8oHygfKLCpe1P3pm4Y2jK0ZWjL8d9Ghi/Dl+ED5wrnCueKoxwganJIaVKaBLnzcuflzsPm1UjxHIwKYsKOlXIYwbIlKMSjF0QviF4w+usIQWUnO9kJvLDqhVUvrLItHDGCsgSMVceqY1W4U71TvVOFd93vut91w46aHTU7amzCtY8rYi4Ma0F/1fOq51UP7FR3qjtVKJKKpCIJJuuT9ck6lMllcpkMb+lv6W/p0E8//aO5kDVupzCFKcBSaam0VIJMKVPKlGxeky36Fn2LDrv8u/y7/CStuePUnbpTh5uKbyq+qRjuNu827zZt2gJhORMWKXG/hmqohgp9ep/ep0O/3q/363E1fqzvSzAQv/7A6w+8/gB8wAd8cJTHMjyGx/DA402PNz3eBP4uf5e/CyYpk5RJih2L9l3zu+Z3TXiw9MHSB0tty3dUiSpRJU4hseaHgvKC8oJyuL/5/ub7m21CPhErI+7/Sf1J/UkdXvnkK5985ZMM5wsaASkLyPkOSwPZVbqrdFcp/F75vfJ7BcJ6WA/ryU8TE+Jnpc9Kn5XgjjV3rLljDYzxj/GP+RhPeCeLLFeWK8tlm1hvMW4xbjHshX4YLEFgUBqUBiVoUBqUBsXOQjruWhfWAO8J9YR6QjCkDWlDWvLDhSZ5oXahdqEGziZnk7MpRTGdDIkm9vH6eH28Di7DZbiMo5yQYHE6pB5SD6kQaY+0R9pHf12hOQ4UDxQPFMMfKv5Q8YeKuIVnhJo0kiqpkgp3l91ddncZXBS4KHBRYPTpwB85rHnoQOmB0gOl0NjY2NjYaC/QIptM1mVd1sFreA2vMbLlNUa4Z1kGFnUs6ljUEadgWQqDyAbc6Njo2OiAwdLB0sFShhMCWuNvpmuma6YL7lxx54o7V0CumqvmqraFeae0U9opwZrONZ1rOmHV8lXLVy2HH2754ZYfboEf9/+4/8f98Ojjjz7+6OPwaP+j/Y/2w6MLH1346EJ4bO9jex/ba/OlDTUNNQ01Df8+RSyZYDT+2bKfLfvZMjioH9QP6sQEoUuMS4xLDPhu9Xerv1sNVwavDF4ZjLOIWAqJsJjev+b+NfevgQXqAnWBGid40Ecf8IT8hPyEDHWz62bXzYbeYG+w9wQUzpQAcp5DLESRhkhDpAE2d2/u3twNW5WtylYlzmeYZAIUxElflL8of1GG5WuWr1m+BrLLs8uzy88eU78wtSaaEMW+CMJdVr2selk1VCgVSoUC2WSTDcmZTq2FqU1qk9okaMxrzGvMs2tbjPr+RD9aE+Ah3yHfIR98qH2ofahhBz8mUF4LX/McfY4+R4ccPUfPOY7rnm+I8aZYLq3Zodmh2SHI0XK0HC35ecIEHSu+Vh4tj5aP/roxqnyL0OvtwrcL3y6Ep+Sn5Kdk6Ff6lX6FpONQ3O8CeYG8QIZ7mu9pvqcZLtAv0C/QwVHpqHR8BIRlo4WwhERrojXRGtiiblG3qLDT2GnsNIh915ONycZkA5bmLs1dmguZaqaaqcaNX+FiTODDiVk+lKXKUsW2fAgq/ueN543nDXg38G7g3cDwGmJC0Enzp/nT/HBT9U3VN1VDPvnkQ2z8dyqdSqcCDxY9WPRgETzHczwHdNNNNxApjhRHiiFaHC2OFttEdabbdJtuiHqinqgHzCqzyqyynzv2XSaZTwfCA+GBMGxyb3JvcsPPHD9z/MxhMyyLeegSLuES4H7zfvN+E65sv7L9ynaYWTWzamYVBNYE1gTWwAJlgbJAiRM81D61T4VfS7+Wfi3BurfWvbXuLTjSdqTtSBv2d3mczLcpASSF/4NQMBQMBeEXy36x7BfL4F/Kv5R/Kcc4wRp4OXKOnCPDV5WvKl9V4M6uO7vu7ILxbePbxrd9/AWRmMvI0njEhDa2fGz52HL44p4v7vniHviG9g3tGxq4DbfhNhiRYj2oBbWgBv9d8d8V/10B+wL7AvsCcQvOKNNjYxYLQd0ePBg8GIT3pfel9+Ovn4Txc7Y+W5+tw5yaOTVzauzg2RQSYJnqx7WPax/XDovyF+UvyreZR5Ohl156gXe73+1+t5sTZl4VGKocqhyqhMYljUsal8AWY4uxxQBTNVVTPcoJwuVmxaAs0hfpi3S4r/W+1vtaYWZoZmhm6BTWUjnNOKgd1A5q0OhodDQ6oF/ql/ole0FcqC3UFmowq3xW+axybEuFyCIT/DhWzMWiNYvWLFpjx0bELB/SXmmvBBu3bdy2cRsMtg22DcbHWoggYqtdt8vtcrugWC6Wi2U77VrEzv2BP/AH4B/1/6j/Rz2YATNgBuyYjsxAZiAzAON843zjfDCuflz9uHr7extvjDfGG5BTmVOZUxkXk5JsnknAQNNA00ATbPRu9G70xllErP4UgsilxqXGpQZ8v+T7Jd8vgR/m/zD/h/lQbBQbxYZNqChcLb9Wf63+WoV1Y9aNWTcGerw93p64NN/jnc8EUgJICv8HoijWG643XG+4oHZh7cLahXGCbaIGlpCum6PkKDkKVKgVaoUK96+8f+X9K2FGYEZgRgCcbc42Z9vwYC6B002oNNJ1xcI8vWF6w/QGuHfrvVvv3Qpf176ufV2zTa0jTQg9ao/ao8I6eZ28ToZXtVe1VzW7mNeJ3regeu+p6qnqqYJXlVeVVxXbl5sMwpWw3FxuLjfBXe4ud6cIrIYxxQqT9GeDnw1+NghXaldqV2okJwizfPZC8/1n6J+hf55CKv2QFtJCGjxW8FjBYwXwpvSm9KZE8mwcIYhYhGglUolUIsGD1Q9WP1gN11VeV3ldJWQVZhVmFSbXrD/q70IoBHqNXqPX2DEhApOZzGTAN883zzcPMr2Z3kwvw4Jap7RNaZvSBku1pdpSzWZ2FsGdz8nPyc/J8I70jvSOdJT3lhAcO6V+Sv2UevDoHt2jExNkDhmHjEMGvLzt5W0vb4NoVbQqWkVs4ryw/cL2C9vhe6u+t+p7q6A2vza/Nh8eX/z44scXw+MrHl/x+Ap4fOXjKx9fCT9f/PPFP18Md9bdWXdnHYwpHlM85hhBzYkYrBqsGqyCjcZGY6MBa7vXdq/thgPSAemAhB2kLhVIBRJ8QvuE9glteIzHEzzBE8CvOn/V+atOOBI+Ej5yAoJGMqQEkBSOCmEK3RzcHNwchDqjzqgzoMfoMXoMhle3TGCEFFkDS9Ql6hIVflTyo5IflcCtgVsDtwYgL5AXyAsQo4CM+XJHGd1+vEiMZo9pSE2OJkcTjPeP94/3w+caPtfwuQb4Ud2P6n5UB7dxG7dhE0AN02wTBJGwETbCBvxG+o30GwkaGhsaGxphKDwUHjqJARvTMKyo96gr6oq64MWiF4teLLIpvZM+v/Tv/v208Wnj0wbcMeuOWXfMgmxfti87Lovpo154PiqI7Iir9av1q3X4StdXur7SZVOrJ4Mo675F26Jt0eAQhzh0Km/MSpd/t+ndpneb4CfLfrLsJ8tsV09SQcT6TkUMkKgh84N5P5j3g3nwzanfnPrNqSBVSpVSZVx6qBgno9S4Txssi8PB2oO1B2uhYVnDsoZltitKfK+yKquyCl631+11E7tvQcy1qHVR66JWmCXPkmfJ9v+C+n7j+o3rN64/iuVDIKFYobvB3eBusIN/BY4oR5QjCoSUkBJS4s63BNdrGq5puKYBbtFu0W7R4Ar1CvUKFeYac425BszV5+pzdXtbJBfJRTLcrtyu3K7AdN903/TjUVwsQWywZbBlsAU2SZukTRL8Rv2N+hvVDpaOzWdia93vc8pzynMK1HXXddd1Q4/cI/fIp57QMCWApHBM9Pv7/f1+qL+g/oL6C+AJ7QntCQ166KHnWCeK8ubWBHiZcZlxmQHfqftO3Xfq4JHFjyx+ZDHcot6i3qLC1PKp5VPLIa0+rT6t/jQwrVoTSJo3zZvmhcnhyeHJYVgcWhxaHIKH5j0076F58L2O73V8rwPmSHOkOZKtEYwEYSL+nfw7+Xcy1BXVFdUVQV9xX3Ff8XCf8qnCP41/Gv804AX5BfkF2U7zTQZRVfVL2pe0L2lQmVuZW5kLU4NTg1ODwzXhcxUiViBLzVKzVPhs7WdrP1sL98+7f9798+Bi5WLlYoVhzLUxWPvvy+/L78vw7Ppn1z+7HiKBSCASOA03bC0or9W+VvtaLfx4/Y/X/3g9/It/8S8YkcJdLDAT5AnyBBnukO+Q75Chuqy6rLoMvvznL//5y3+GS32X+i71QVYgK5AVGJnR9bRBVBG2ijXGiLV4kzfjDovFhMxbOm/pPMisyazJrIEp4SnhKWFYKi+Vl8qQoWfoGbpt+XhBekF6QYJ3Au8E3gkQE3iSCuDCNWu5dBIZSoUFMtoSbYm22O2JKNIP9A/0D3TYwx72AGE5LIdlO9g/cStceh1yh9whQ0gKSSFp9N0Xm2+sdNqLpIukiyRYoC/QF+iQpqapaepRTrS+I8EH5dW8mlezBbpTjVQabgpHR0L0d29hb2FvIWhhLayFISyFpbAEFVqFVqFBrpwr58oMs4TEYGlqY4wxxhgDPql/Uv+kDkX5RflF+dChdCgdCvxd/7v+dx1eL369+PVimwjoUMOhhkMN0FfeV95XblNkCx+tGHBiYLmCrqArCHmePE+exyZ2KiopKikqgQV9C/oW9MGMkhklM0ogW8vWsrW4+xeaZZKFR6BH69F6NPit9lvttxqsc6xzrHPAh/4P/R/G3d/pwkD7QPtAO/xh7R/W/mEtXFN9TfU11XbUewwJrjLxvEIQEbVFGtY2rG1YC695XvO85oED7QfaD7THUZGLWjXFZrFZHNcf4jkTt5YGOUxzSnZ8wnnD+i/x98StgOBFsIIzneXOcmc5jPWO9Y712vw1Sx5b8tiSx2yeiTzyyIs7fxis5+2X++V+GX6v/V77vWanQ55yiHFoPV+kJlITqYEXXS+6XnTBkH/IP+SHe7R7tHs0uES5RLlEsbM+Ys+RYNEQPA4iKPEu+S75LhluNW41bjWgTW/T23R4Zdkry15ZZqeR7tP36ft0OFJ/pP5IPQyVD5UPldsW01gQZaVZaVYynNnX2h9Jk050NRwIHAgcCECj1qg1anB5yeUll5dAlpalZWkgS7IkS7Bp4aaFmxbCbHO2OduEy9TL1MtU+/nF7WxcuHHhxoUwGBgMDAZG4dqw3rsQiMwSs8QsGf5/YgyKqBre6m31tnrhXu7lXmCqf6p/qh8c2xzbHNuGX26oYKhgqADe636v+71u2C/vl/fLR+nPBMQEqBpqqLGzWgLzAvMC8+ysFsE3MqANaAOabckTDM4z5ZnyTBnu537uBx5qeajloRZ4zfWa6zXX8QdZJ0NKAEnhqBhWbtsaoD2eHk+PB5745BOffOKTsL9gf8H+AqhcU7mmcg1cZFxkXGTEEWMlQkyElqQt5tfZzGY24JW9sleGL2hf0L6gQWhFaEVoBexr3de6tGu1fQAADpxJREFUrxW6V3av7F4JvbN6Z/XOsk2x5gpzhbnCdpWImIcp0hRpigR5HXkdeR3gUlyKS4ljmEy4n2H3mZhdYvn89+h79D06/Er7lfYrDZ5+6umnnn4K+mr7avtqsSes012DxdKw3uM93gN+se0X236xDe7LvS/3vtyjBMsmLEQZSoaSocBV2lXaVRrM3TB3w9wNsEfbo+3RYNfUXVN3TYX+qf1T+6fCUO5Q7lCuvW/ebt5u3h7XP+K5rVgi0a/mHnOPucf+P7E/xcTOSlayMm4/8XjRnvhf/C5US+v4tJVpK9NWQraerWfrkNma2ZrZChPnTZw3cR5MXzl95fSVkNuV25XblZxxNAZBJKdG1agKz8vPy8/L8HT3091Pd0PEHXFH4mIpTlUMSLJ2Iq6IK+KCv/r+6vurD7qULqVLgbulu6W7JVggLZAWSJBO+tEn+USBxEgz0gyYxjSmAdP0afo0HW7surHrxi74UP1Q/VCF/XX76/bXQdftXbd33Q6H9cP6Yd12QeyT9kn7JNBlXdZl2Nm0s2lnE0Tbo+3R9hMfD2aNWWPW2JaQpR1LO5Z2wHxtvjZfgyn6FH2KDkqJUqKUwDRjmjHNsC1+UT2qR3V4Xntee16Dd4reKXqnCMxas9Y8hmY/rP9LKeUYwdvieNNlusw4BW6wfbB9sN224CRacpJCuKSPV/DwFfgKfHBf3X1199XBNcY1xjVGXHCp1qf1abZLRgTLV8qVcqUME/WJ+kQdLlUvVS9V4bvN323+bjM8pD2kPaTBqy2vtrzaYgueJ4oRBZCYSdaiZD1uHCcxyalCIgPeqM+TrFiERhppPPX3lfS6iaY/qwbFxw1CMBnwDHgGPLCpdlPtplow1hprjbU2MdkntU9qn9RsDWWkbBEBIbi4VJfqUsEjeSSPBB7Vo3pUhi10iSbmROKvYYLEaL+HhHZEOfbXeI3XgF+s/sXqX6yG18tfL3+9HCJKRIkoDKsRc9phSXBign2u4bmG5xpg6oqpK6augK/JX5O/JkMOOeQc7fnE1tKURbqx4JWIvTahSSezDCVaDBLaTRozJAQ/8b84PhnhV+Lxie2JrTg/oYbKsPtLdp2E40T/viy9LL0swaOrH1396Gro9nX7un1x7SXyRpwmxBZG63t7Q31DfUOF/9f2/9r+Xxt8qflLzV9qhjLKKAMmaBO0CRr28yaOk8R96z1kGBlGhmG7bsR2WDtWP4u0/cXGYmOxAd8Lfi/4vSBs9273bo/LmjhuWM95QDmgHFCgsaixqLEI5uTOyZ2Ta3+3siEbshE3jwvLhx7UgzpsbNzYuLERBjcPbh7cTMxFcbYiUfCY4Z/hn+G3FZAYj4dFnBYLLrVc6euK1hWtK4KBJQNLBpZAuDJcGa6EVdIqaZUEk9RJ6iQVLlEvUS9R4bvGd43vGvBg7YO1D9bCq+FXw6+GIVobrY2egItmZAuIkMDEwEr8UBORODGJYL8E3/LpIkKKvZBCComPIRhp4Un8X/j8JOtDHqXp8ISRYGqN3U8XXXSN4r7P1IJnQQgiwnS33b/dv90P3ze+b3zfgNL/397ZhUTx9XH8a5ZuSbQWpUXQsTf+YqUWkTfRdGdBuBGBdFFjdGFXrXWzdNPYC9hNKgQVlM6CUUHkVlrbVaMVrUE2RpBFyfQiLZXtLL7t7rju/+KZs2eZnn18SbfyOZ+bxWXcOTt7Zs7v/F6+P6lUKpWAveJeca8I5Cl5Sp7CJImT7cjjTPT98TLOeUt1Oj5Jn6RPEtBMmkkzAe5U3am6UwV8z/6e/T07oaolxdefYs0tidgj9ogduC5fl6/LQHRbdFt0G6tKotLQPyWfUZKVj1qv2+9OUpwokxyvIRuyIQPtUrvULgHn3Ofc59ysl0/MG/PGvFM60glhfY5+tX21fbUB5/3n/ef9wLPKZ5XPKoF9hfsK9xUCm7RN2iaNeYaShWjG/DvJvKEbCBrypA6DV8Wvil8Vs+fFhL+npUy+3d5ub7cDZUqZUqYwTwit/qHEq11MPY53wjvhnQDAAw88kxjIn8ZFXMRFIK8krySvBHDFXDFXDNhCtpAthHn24uW0SpPSpAANSxuWNiwFBuQBeUBmH9eCFrQASNudtjttN3BkwZEFRxYAi8RF4iIRWCWvklfJwHFynBwnwBn7GfsZ+8/KquNlzCTUWHesO9bNXEpDwpAwJCS4WJNAu60aHsNjeDDtsfD4eC2CPiO+Ed+IjyUJJsXcUQ2TYTJMmLBQyhYWen1Mg48mKY1oI9qIlvzf6A0WqgvVhepSOF4TavBRF2lADagBFbjx4MaDGw+Aqq6qrqou4IJ2QbugAW/EN+IbEQhrYS2sYewd6HRj/u4REiERAryX38vvZeCycFm4LADONmebsw1w57nz3HlsBxaPcf9h0AWJKhZeq7pWda0KOLX91PZT24EuoUvoEhJ6/kzQQzjToTv4b/I3+ZsMNIqNYqMInL59+vbp28BHz0fPRw9+/7y1YPWg0vn82PXY9dgFHO863nW8CzhZf7L+ZD3wRHgiPBGAftJP+klCSGuqxmPmoMwX5gvzhbEVS8eNuaHtI32kjyQoptLnCTWMTIOJVoe1VLdUt1SzqpDf5Zmfamio5FjzseZjzcAWZYuyRWFK1cPisDgsJgiItTW0NbQBg55Bz+B/McAMGDAAtNhabC22BEEzsU/sE/GTsqor4Aq4AsBq22rb6kl4/sadAzLqGfWMeoBOrVPr1FiyUjzGbP7w1AKix/0I/wj/SBTrT5GLktKv9+v9OvCcPCfPCZAv5Uv5UkIzHXOi0mTCp8GnwadBIOqNeqOJO5tpHndcMtgsg9PqtDqtDugp7CnsKQQ2KBuUDQrYg8+cCLQ74uuq11Wvq36OPU471hvZ9BTRaowP+gf9gw5cyb+SfyUfuOu767vrAzYXby7eXAxsrd1au7UWKJAKpAIJWCwsFhYLrDrBmm0+2Z04FXCihvF3fMd3AN1yt9wtA4/cj9yP3ECHo8PR4QC+dHzp+NIBRJ1RZ9SZ4Gkwdxx/KvGFiJbhlRglRgnw0PHQ8dABvL79+vbr28BO7MROADvEHeIOEVihrFBWKICN2IiNgIU4krno/xaSGVjm+1ESJVEC9KEPfQB8kk/ySUBzsDnYHAReVr+sflmd0H2VeoRTtKEaNxYParyrKfnPwk8FBlv9rf5WP/Co+1H3o25gXcW6inUVrKndRnGjuFEElivLleUKkKVlaVkayx2wLvBxLPOEPk992b5sX/bUVQf95AkR28V2EdhTtqdsTxmwQdugbdDYBvkhHuIhgHehd6F3oZ89hhMl4og4Ig5gJDASGAkgfp9QSfTRw6OHRw8jZevcstxluctyWZdya45HE5rQBKDhcMPhhsPAQO5A7sA45i9NTr3ruuu66wJibbG2WBtw5MSRE0dOMI/ISnGluFIE8rvyu/K7gLd4i7cTGH9aQaggVBCKJe3QG68LNwecWZ5ZnlkO7Grc1birEdgt7ZZ2S0yquJN0kk4CXAleCV4JAp/1z/pnnXlSUkXc4jY9Akt8S3xLfMDB2oO1B2uZiyoshaWwBLRqrVqrBtx033TfdANDriHX0HSU0yUbr6UNOBXEKnYVu4pdQEVZRVlFGUAIIYQAvVKv1CsB7np3vbse6PB1+Dp8QFSLalEtdeOeLGmeNE+aB8hUM9VMFcjx5nhzvMAqdZW6SgXWNK5pXNMIrBRWCisFFjpYqC3UFmost4QaktS1G0EEEQABLaAFNNYToUfpUXoU4G3wbfBtEHivvlffq4Bf8At+AQipITWkArHSWGnsfySZ/e3QUOisylmVsyqBJdoSbYkGrBfXi+tFYNOLTS82vQDypDwpTwJySA7JIawtejwJlEr0J1uIxhtCsxxvjd0n/dwxPp8arvSV6h7oki7pEtCr9Cq9CvBSeim9lIDO+s76znqgp7ynvKccCNeEa8I1rIphpkJd9Ok16TXpNawKKN5tuXZN7ZpaYK28Vl4rAzlSjpQjsQVoHplH5hGmCNqv9Wv9GtCKVrQCuPng5oObD4Bh57Bz2Dl1ofe4gJpZJryzaGfRziLg0IFDBw4dYAJuNcGaYE0QeFPypuRNycTPH1//zOdyti3blm0Djt4/ev/ofWbw3FJuKbcU4Oqlq5euXgIMzdAM7de/51hk69l6tg4cu3/s/rH7bKN6T7on3ZOApuqm6qZqZhBONoWACr3t0HfoO3Rg/4L9C/YvYBu5s8Vni88WAz3Xe673TOB+GdsAIRbXmbmgU30HWm1AJWb1Or1Or2NNlZK1EZ4u4gu5dadiTiAqqWxX7apdBaK+qC/qYxN2sjHKXx63pddHPJRiJhfNE+eJ80Qm3TvoH/QP+oGB0oHSgVIg9k/sn9ivJHmlmHjohnp+6O9GFxAzljzbP9s/28+awFHPSIYzw5nhBOaQOWQOAQyn4TScgCEZkiEBITkkh2Qg3B3uDncDI+qIOqKy+UhfrQJlM7aLr0l8ntH7w5JESJVA5xbNLZpbBMxV56pzVSDTk+nJ9LCunfHcl2QGAc3BsnrI6HHU0KbVJ/Q4a24KfZ/OC6oPYz0fPc48L9VrmOWc5ZzlBCKeiCfiAYZzh3OHc4EhcUgcEgHDYTgMB/t/6wIVv15Ul2Kmzw+LB42WXafXpdel1wEZJRklGSWATbWpNhXI8Gf4M/ysGSMtz6eG3mjRaNFoEaatKoyuT+nedG+6F1jsWOxY7GDrT8Af8Af8wKh31Ds6Bbk6aY40R5oDyCJZJIswCfW+yr7KvkrAKDfKjfIUVL/R8Zj3T5Y9y55lB+b75vvm+5iA21SPJ92f7k/3A4v0RfoiHYjoET2iA8GiYFEw4X4f7/nGNkAsscVUXdhf5W8b99823pmG1SCa6fy/fV/OzCTVz80//b75XevIZK/LmAYIh8PhcDgczlTDpdg5HA6Hw+GkHG6AcDgcDofDSTncAOFwOBwOh5NyuAHC4XA4HA4n5XADhMPhcDgcTsr5F7EwRCKpIDzEAAAAAElFTkSuQmCC'/>]]
end

-- ---------------------------------
-- / Utility Functions /
-- ---------------------------------

--======================== ENVIRONMENT SETUP ============================--

------------------------------------------------------------------------
-- getFusion()
--
-- check if global fusion is set, meaning this script is being
-- executed from within fusion
--
-- Arguments: None
-- Returns: handle to the Fusion instance
------------------------------------------------------------------------
function getFusion()
	if fusion == nil then 
		-- remotely get the fusion ui instance
		fusion = bmd.scriptapp("Fusion", "localhost")
	end
	return fusion
end -- end of getFusion()


--========================== DEBUGGING ============================--


---------------------------------------------------------------------
-- dprint(string, suppressNewline)
--
-- Prints debugging information to the console when DEBUG flag is set
--
-- Arguments:
--		string, string, a message for the console
--		suppressNewline, boolean, do not start a new line
---------------------------------------------------------------------
function dprint(string, suppressNewline)
	local newline = "\n"
	if suppressNewline then newline = '' end
	if DEBUG then _comp:Print(string..newline) end
end -- dprint()

---------------------------------------------------------------------
-- ddump(object)
--
-- Performs a dump() if the DEBUG flag is set
--
-- Arguments
--		object, object, an object to be dumped
---------------------------------------------------------------------
function ddump(object)
	if DEBUG then dump(object) end
end -- end ddump()

status = main()
print("Script finished with status code "..status)