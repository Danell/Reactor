--[[--
This tool sets TimeCode into the image's metadata

By Le Grand Fromage

v 3.1 by Bryan Ray adds 23.976, 29.97, and 59.94 options, with a swith for dropframe timecode for those formats that support it.

--]]--

FuRegisterClass("SetMetaDataTC", CT_Tool, {
	REGS_Name = "Set Time Code",
	REGS_Category = "Metadata",
	REGS_OpIconString = "TCMeta",
	REGS_OpDescription = "Sets TimeCodeMetadata",
	REG_NoMotionBlurCtrls = true,
	REG_NoBlendCtrls = true,
	REG_OpNoMask = true,
	REG_NoPreCalcProcess = true,	-- make default PreCalcProcess() behaviour be to call Process() rather than automatic pass through.
	REG_SupportsDoD = true,
	REG_TimeVariant = true,
	REG_Fuse_NoJIT = true,
	REG_Version = 310,
	})

function Create()
	InFps = self:AddInput("FPS", "FPS", {
		LINKID_DataType = "Number",
		INPID_InputControl = "MultiButtonControl",
		MBTNC_StretchToFit = true,
		{ MBTNC_AddButton = "24" },
		{ MBTNC_AddButton = "25" },
		{ MBTNC_AddButton = "30" },
		{ MBTNC_AddButton = "48" },
		{ MBTNC_AddButton = "50" },
		{ MBTNC_AddButton = "60" },
		{ MBTNC_AddButton = "23.976" },
		{ MBTNC_AddButton = "29.97" },
		{ MBTNC_AddButton = "59.94" },
		INP_DoNotifyChanged = true,
		})
	InHours = self:AddInput("Hours", "TCHours", {
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_MaxScale = 23,
		INP_Integer = true,
		INP_Default = 0,
		INP_MinAllowed = 0,
		})
	InMinutes = self:AddInput("Minutes", "TCMinutes", {
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_MaxScale = 59,
		INP_Integer = true,
		INP_Default = 0,
		INP_MinAllowed = 0,
		})
	InSeconds = self:AddInput("Seconds", "TCSeconds", {
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_MaxScale = 59,
		INP_Integer = true,
		INP_Default = 0,
		INP_MinAllowed = 0,
		})
	InFrames = self:AddInput("Frames", "TCFrames", {
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_MaxScale = 60,
		INP_Integer = true,
		INP_Default = 0,
		INP_MinAllowed = 0,
		})
	InDF = self:AddInput("Drop Frame", "DropFrame", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 0,
		IC_Visible = false,
		})
	InConsole = self:AddInput("Print to Console", "Console", {
		LINKID_DataType = "Number",
		INPID_InputControl = "CheckboxControl",
		INP_Default = 0,
		})


	InImage = self:AddInput("Input", "Input", {
		LINKID_DataType = "Image",
		LINK_Main = 1,
		})

	OutImage = self:AddOutput("Output", "Output", {
		LINKID_DataType = "Image",
		LINK_Main = 1,
		})
end

function NotifyChanged(inp, param, time)
	if inp ~= nil and param ~= nil then	
		if inp == InFps then
			if param.Value > 6 then
				InDF:SetAttrs({IC_Visible = true})
			elseif param.Value <= 6 then 
				InDF:SetAttrs({IC_Visible = false})
			end
		end
	end
end

function Process(req)
	local img = InImage:GetValue(req)
	local fps = InFps:GetValue(req).Value
	local hours = InHours:GetValue(req).Value
	local minutes = InMinutes:GetValue(req).Value
	local seconds = InSeconds:GetValue(req).Value
	local frames = InFrames:GetValue(req).Value
	local console = (InConsole:GetValue(req).Value >= 0.5)
	local currentframe = req.Time
	local df = (InDF:GetValue(req).Value >= 0.5)

	local result = Image({IMG_Like = img, IMG_NoData = req:IsPreCalc()})
	-- Crop (with no offset, ie. Copy) handles images having no data, so we don't need to put this within if/then/end
	img:Crop(result, { })

	local rates = { 24, 25, 30, 48, 50, 60, 23.976, 29.97, 59.94 }

	-- returns a number based on the input from the combo box. 
	local fpsx = rates[math.min(math.max(fps, 0), 8)+1]
	
	-- Calculate the frame number based on user input. For non-dropframe timecode, this is all we need.
	local frames_o = currentframe + frames + math.floor(fpsx * (seconds + 60*minutes + 3600*hours))
	local seconds_s, minutes_s, hours_s, frames_s
	
	-- dropframe timecode uses a semi-colon instead of a colon to delineate the frame number.
	local framesep = ":"


	if fpsx == 29.97 and df then	-- Calculate 29.97 dropframe

		--local d = math.floor(frames_o / 17982)
		--local m = frames_o % 17982
		-- How many frames elapse before we need to add a new one? 
		-- We fall behind by 0.03 / second. Over the course of 1 minute, we're behind 1.8 frames. 
		-- Subtract 2 frames whenever we tick over 1 minute.
		
		local minutes_total = math.floor((frames_o - 1) / (60*fpsx))
		
		-- After adding 1 frame every minute, we'll gain 0.2 frames per minute. Thus, after 10 minutes, we're 2 frames ahead.
		-- Therefore, at the 10 minute mark, we don't drop the frame, falling back into sync every 00:10:00;00. For efficiency, we just subtract two 
		-- frames rather than dealing with the logic of an exception.

		local tenMinutes = math.floor(minutes_total / 10)
		frames_o = frames_o + minutes_total * 2 - tenMinutes * 2
		
		minutes_s = math.floor(frames_o / (60*30)) % 60
		hours_s = math.floor(frames_o / (3600*30))
		seconds_s = math.floor(frames_o / 30) % 60
		frames_s = frames_o % 30
		framesep = ";"

	--elseif fpsx == 23.976 and df then	-- Calculate 23.976 dropframe
		-- -- IMPORTANT NOTE: 24p dropframe is not a standard. Most systems do not drop frame numbers, preferring instead to allow the clock
		-- -- to drift.  This code is based on David Heidelberger's article at https://www.davidheidelberger.com/2010/06/10/drop-frame-timecode/
		-- -- As Resolve does not even have an option for 23.976 dropframe, I've left the code disabled. 
		
		-- framesep = ";"
		
		-- local secondsDecimal = frames_o / fpsx
		
		-- local secondsInt = math.floor(secondsDecimal)
		
		-- -- Get the remainder of the operation, representing the percentage of a second
		-- local secondsDecimalPart = secondsDecimal - secondsInt
		-- -- Convert the percentage to a number of frames, rounding to the nearest.
		-- frames_s = math.floor(0.5 + secondsDecimalPart * fpsx)
		
		-- -- 24 frames is greater than 1 second, so reset and increment to the next second.
		-- if frames_s == 24 then 
			-- frames_s = 0
			-- secondsInt = secondsInt + 1
		-- end
		
		-- -- This is now the total number of seconds elapsed.
		-- local remainingSeconds = secondsInt
		
		-- -- We determine how many hours those seconds represent,
		-- hours_s = math.floor(remainingSeconds / (60*60))
		-- -- Then subtract that result from the total seconds.
		-- remainingSeconds = remainingSeconds - (hours * 60 * 60)
		
		-- -- Do the same for minutes
		-- minutes_s = math.floor(remainingSeconds / 60)
		-- remainingSeconds = remainingSeconds - (minutes * 60)
		
		-- -- What's left is how many seconds are on the clock.
		-- seconds_s = remainingSeconds
		
	elseif fpsx == 59.94 and df then
		
		local minutes_total = math.floor((frames_o - 3) / (60*fpsx))
		
		-- This is the same as 29.97df except we drop 4 frames each minute rather than 2.

		local tenMinutes = math.floor(minutes_total / 10)
		frames_o = frames_o + minutes_total * 4 - tenMinutes * 4
		
		minutes_s = math.floor(frames_o / (60*60)) % 60
		hours_s = math.floor(frames_o / (3600*60))
		seconds_s = math.floor(frames_o / 60) % 60
		frames_s = frames_o % 60
		framesep = ";"

	else	-- integer fpx or non-dropframe
		
		-- Calculate frames from Time code settings

		frames_s  = math.floor(frames_o % fpsx)
		seconds_s = math.floor(frames_o / fpsx) % 60
		minutes_s = math.floor(frames_o / (60*fpsx)) % 60
		hours_s   = math.floor(frames_o / (3600*fpsx))

	end
	
	-- format the time code strings
	local timecode  = string.format("%02d:%02d:%02d"..framesep.."%02d", hours_s, minutes_s, seconds_s, frames_s)

	-- Format and save meta data. Pad the time code with "0" for formating
	local newmetadata = result.Metadata or {}

	newmetadata["TimeCode"] = timecode
	newmetadata["FrameRate"] = fpsx
	result.Metadata = newmetadata

	-- This outputs to the console so it can be used as a timecode frames calculator
	if (not req:IsPreCalc()) and console then
		print("\nTimeCode:  "..timecode.."\nFrames:    "..frames_o)
	end

	OutImage:Set(req, result)
end
