Atom {
	Name = "Open Cell Animation Data Nodes",
	Category = "Kartaverse/OCA",
	Author = "Andrew Hazelden",
	Version = 1.02,
	Date = {2023, 10, 15},
	Description = [[<p>The OCA (<a href="http://oca.rxlab.guide/">Open Cell Animation</a>) data nodes for Blackmagic Design Resolve/Fusion allow you to interact with 2D animation data inside a node graph. This approach works with a series of nodal operators that allow you to edit and modify the OCA data on the fly.</p>

<p>Note: The OCA toolset's capabilities are enhanced with the installation of the Reactor package named "Vonk Ultra" that is found in the "Kartaverse/Vonk Ultra" category in the Reactor window.</p>

<p>Open-Source License<br>
GPL v3</p>

<p>OCA for Fusion is maintained by:<br>
<a href="mailto:andrew@andrewhazelden.com">Andrew Hazelden</a></p>

<p>Documentation<br>
The <a href="https://docs.google.com/document/d/1DXnF47CK7dteF7lidwek5-lwy5qB75nBQMt_2Bp0y0g/edit?usp=sharing">OCA documentation</a> is accessible on Google Docs.</p>

<p>If you would like to provide feedback on the evolution of the OCA data nodes, please check out the <a href="https://www.steakunderwater.com/wesuckless/viewtopic.php?p=45034#p45034">development thread on the Steakunderwater forum</a>.</p>

]],
	Deploy = {
		"Comps/Kartaverse/OCA/Demo ocaLoader/Export/out-color.oca/out-color.oca",
		"Comps/Kartaverse/OCA/Demo ocaLoader/Media/test_color.oca/test_color.oca",
		"Comps/Kartaverse/OCA/Demo ocaLoader/OCA-Loader.comp",
		"Comps/Kartaverse/OCA/Demo ocaStage/Export/out-stage.oca/out-stage.oca",
		"Comps/Kartaverse/OCA/Demo ocaStage/OCA-Stage.comp",
		"Config/DragDrop/OCA ocaLoader DragDrop.fu",
		"Docs/Kartaverse/OCA/README.md",
		"Docs/Kartaverse/OCA/images/oca.png",
		"Fuses/Kartaverse/OCA/Create/ocaFrame.fuse",
		"Fuses/Kartaverse/OCA/Create/ocaLayer.fuse",
		"Fuses/Kartaverse/OCA/Create/ocaStage.fuse",
		"Fuses/Kartaverse/OCA/Create/ocaText.fuse",
		"Fuses/Kartaverse/OCA/Effects/ocaLens.fuse",
		"Fuses/Kartaverse/OCA/Flow/ocaSwitch.fuse",
		"Fuses/Kartaverse/OCA/Flow/ocaWireless.fuse",
		"Fuses/Kartaverse/OCA/IO/ocaLoader.fuse",
		"Fuses/Kartaverse/OCA/IO/ocaSaver.fuse",
		"Fuses/Kartaverse/OCA/Image/ocaRender.fuse",
		"Fuses/Kartaverse/OCA/Utility/ocaInfo.fuse",
		"Fuses/Kartaverse/OCA/Utility/ocaMerge.fuse",
		"Fuses/Kartaverse/OCA/Utility/ocaScript.fuse",
		"Scripts/Comp/OCA/Open/Show Comps Folder.lua",
		"Scripts/Comp/OCA/Open/Show Docs Online.lua",
		"Scripts/Comp/OCA/Open/Show Fuses Folder.lua",
		"Scripts/Comp/OCA/Render Selected.lua",
		"Scripts/Tool/OCA/Render Selected.lua",
	},
}
