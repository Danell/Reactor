Atom {
	Name = "stitchEm VideoStitch Studio",
	Category = "Bin",
	Author = "stitchEm",
	Version = 2.4,
	Date = {2021, 3, 3},
	Description = [[<h1>VideoStitch Studio by stitchEm</h1>

<p>VideoStitch Studio is a post-production video stitching program that enables you to create immersive 360&deg; VR videos. It takes video files and stitches them into a standard 360&deg; spherical video file automatically. VideoStitch Studio supports input synchronization, automatic calibration, exposure and color correction, video stabilization, and a range of different output formats.</p>

<h2>For More Information</h2>

<p>StitchEm GitHub site:<br>
<a href="https://github.com/stitchEm/StitchEm">https://github.com/stitchEm/StitchEm</a></p>

<p>StitchEm Facebook Group:<br>
<a href="https://www.facebook.com/groups/stitchEm">https://www.facebook.com/groups/stitchEm</a></p>

<p>Getting Started YouTube Video by Veroma:<br>
<a href="https://www.youtube.com/watch?v=MhC42-t8nEg">https://www.youtube.com/watch?v=MhC42-t8nEg</a></p>

<h2>License</h2>
<p>VideoStitch Studio is an MIT-licensed open-source project. A license key is not required to run the software. The software was originally developed by VideoStitch SAS. After the company folded in 2018, the source code was acquired by the newly founded non-profit organization stitchEm, to publish it under a free software license.</p>
]],
	Deploy = {

		Windows = {
			"Bin/stitchEm/Studio/1-calibrationimport.bat",
			"Bin/stitchEm/Studio/2-process-ptv.bat",
			"Bin/stitchEm/Studio/3-batch-process-ptv.bat",
			"Bin/stitchEm/Studio/Qt5Core.dll",
			"Bin/stitchEm/Studio/Qt5Gui.dll",
			"Bin/stitchEm/Studio/Qt5Multimedia.dll",
			"Bin/stitchEm/Studio/Qt5Network.dll",
			"Bin/stitchEm/Studio/Qt5OpenGL.dll",
			"Bin/stitchEm/Studio/Qt5Widgets.dll",
			"Bin/stitchEm/Studio/audio/qtaudio_windows.dll",
			"Bin/stitchEm/Studio/avcodec-58.dll",
			"Bin/stitchEm/Studio/avformat-58.dll",
			"Bin/stitchEm/Studio/avresample-4.dll",
			"Bin/stitchEm/Studio/avutil-56.dll",
			"Bin/stitchEm/Studio/batchstitcher.exe",
			"Bin/stitchEm/Studio/calibrationimport.exe",
			"Bin/stitchEm/Studio/ceres.dll",
			"Bin/stitchEm/Studio/core_plugins/jpg.dll",
			"Bin/stitchEm/Studio/core_plugins/png.dll",
			"Bin/stitchEm/Studio/core_plugins/tiffPlugin.dll",
			"Bin/stitchEm/Studio/core_plugins_cuda/av_cuda.dll",
			"Bin/stitchEm/Studio/cudart64_100.dll",
			"Bin/stitchEm/Studio/dropletusage.txt",
			"Bin/stitchEm/Studio/gflags.dll",
			"Bin/stitchEm/Studio/glfw3.dll",
			"Bin/stitchEm/Studio/glog.dll",
			"Bin/stitchEm/Studio/imageformats/qgif.dll",
			"Bin/stitchEm/Studio/imageformats/qico.dll",
			"Bin/stitchEm/Studio/imageformats/qjpeg.dll",
			"Bin/stitchEm/Studio/imageformats/qsvg.dll",
			"Bin/stitchEm/Studio/imageformats/qtga.dll",
			"Bin/stitchEm/Studio/imageformats/qtiff.dll",
			"Bin/stitchEm/Studio/imageformats/qwbmp.dll",
			"Bin/stitchEm/Studio/jpeg62.dll",
			"Bin/stitchEm/Studio/libeay32.dll",
			"Bin/stitchEm/Studio/libmp3lame.dll",
			"Bin/stitchEm/Studio/libpng16.dll",
			"Bin/stitchEm/Studio/librtmp.dll",
			"Bin/stitchEm/Studio/libvideostitch-base.dll",
			"Bin/stitchEm/Studio/libvideostitch-gpudiscovery.dll",
			"Bin/stitchEm/Studio/libvideostitch-gui.dll",
			"Bin/stitchEm/Studio/libvideostitch.dll",
			"Bin/stitchEm/Studio/libvideostitch_cuda.dll",
			"Bin/stitchEm/Studio/libx264-157.dll",
			"Bin/stitchEm/Studio/lzma.dll",
			"Bin/stitchEm/Studio/msvcp140.dll",
			"Bin/stitchEm/Studio/nvml.dll",
			"Bin/stitchEm/Studio/opencv_calib3d.dll",
			"Bin/stitchEm/Studio/opencv_core.dll",
			"Bin/stitchEm/Studio/opencv_features2d.dll",
			"Bin/stitchEm/Studio/opencv_flann.dll",
			"Bin/stitchEm/Studio/opencv_imgproc.dll",
			"Bin/stitchEm/Studio/opencv_video.dll",
			"Bin/stitchEm/Studio/openvr_api.dll",
			"Bin/stitchEm/Studio/platforms/qminimal.dll",
			"Bin/stitchEm/Studio/platforms/qoffscreen.dll",
			"Bin/stitchEm/Studio/platforms/qwindows.dll",
			"Bin/stitchEm/Studio/portaudio_x64.dll",
			"Bin/stitchEm/Studio/ptvb2ptv.exe",
			"Bin/stitchEm/Studio/ssleay32.dll",
			"Bin/stitchEm/Studio/swresample-3.dll",
			"Bin/stitchEm/Studio/tiff.dll",
			"Bin/stitchEm/Studio/turbojpeg.dll",
			"Bin/stitchEm/Studio/unins001.dat",
			"Bin/stitchEm/Studio/unins001.exe",
			"Bin/stitchEm/Studio/vcruntime140.dll",
			"Bin/stitchEm/Studio/videostitch-cmd.exe",
			"Bin/stitchEm/Studio/videostitch-studio.exe",
			"Bin/stitchEm/Studio/zlib1.dll",
			"Docs/VideoStitch Studio/BUILD.md",
			"Docs/VideoStitch Studio/CHANGELOG.md",
			"Docs/VideoStitch Studio/Images/Advanced settings.jpg",
			"Docs/VideoStitch Studio/Images/Audio Settings.jpg",
			"Docs/VideoStitch Studio/Images/Automatic Calibration.jpg",
			"Docs/VideoStitch Studio/Images/Batch Stitcher.jpg",
			"Docs/VideoStitch Studio/Images/Before the stitch.jpg",
			"Docs/VideoStitch Studio/Images/Bottom Information.jpg",
			"Docs/VideoStitch Studio/Images/Calibration Extract Stills.jpg",
			"Docs/VideoStitch Studio/Images/Calibration Tab.jpg",
			"Docs/VideoStitch Studio/Images/Calibration Window.jpg",
			"Docs/VideoStitch Studio/Images/Color Correction Examples.jpg",
			"Docs/VideoStitch Studio/Images/Color Correction advanced parameters.jpg",
			"Docs/VideoStitch Studio/Images/Color Correction window.jpg",
			"Docs/VideoStitch Studio/Images/Color Corretion base tab.jpg",
			"Docs/VideoStitch Studio/Images/Color correction.jpg",
			"Docs/VideoStitch Studio/Images/ColorCorrection Camera response.jpg",
			"Docs/VideoStitch Studio/Images/Drag Media.jpg",
			"Docs/VideoStitch Studio/Images/Frame selection.jpg",
			"Docs/VideoStitch Studio/Images/Import Calibration.jpg",
			"Docs/VideoStitch Studio/Images/Inputs crop.jpg",
			"Docs/VideoStitch Studio/Images/Inputs crop2.jpg",
			"Docs/VideoStitch Studio/Images/Interactive.jpg",
			"Docs/VideoStitch Studio/Images/Media in Source.jpg",
			"Docs/VideoStitch Studio/Images/Open Media.jpg",
			"Docs/VideoStitch Studio/Images/Orientation.jpg",
			"Docs/VideoStitch Studio/Images/Orientation2.jpg",
			"Docs/VideoStitch Studio/Images/Output Configuration Window.jpg",
			"Docs/VideoStitch Studio/Images/Output Configuration.jpg",
			"Docs/VideoStitch Studio/Images/Output.jpg",
			"Docs/VideoStitch Studio/Images/Panels.jpg",
			"Docs/VideoStitch Studio/Images/Process Panel.jpg",
			"Docs/VideoStitch Studio/Images/Send to batch.jpg",
			"Docs/VideoStitch Studio/Images/Shortcuts.jpg",
			"Docs/VideoStitch Studio/Images/Stabilization Orientation Window.jpg",
			"Docs/VideoStitch Studio/Images/Stabilization Orientation.jpg",
			"Docs/VideoStitch Studio/Images/Stitched.jpg",
			"Docs/VideoStitch Studio/Images/Synchronizaition Auto.jpg",
			"Docs/VideoStitch Studio/Images/Synchronizaition Manual.jpg",
			"Docs/VideoStitch Studio/Images/Synchronization Bar.jpg",
			"Docs/VideoStitch Studio/Images/Timeline Limiter.jpg",
			"Docs/VideoStitch Studio/Images/Timeline.jpg",
			"Docs/VideoStitch Studio/Images/Zoom timeline.jpg",
			"Docs/VideoStitch Studio/Images/ghosting.jpg",
			"Docs/VideoStitch Studio/Images/keyframes exposure.jpg",
			"Docs/VideoStitch Studio/LICENSE-3RD-PARTY-LIBRARIES.md",
			"Docs/VideoStitch Studio/LICENSE-3RD-PARTY-SOURCES.md",
			"Docs/VideoStitch Studio/LICENSE.md",
			"Docs/VideoStitch Studio/README.md",
			"Docs/VideoStitch Studio/User Guide.md",
		},
	},
	InstallScript = {
		Windows = {
			[[if platform == "Windows" then
	-- ----------------------------------------------------------------------
	-- VideoStitch Studio Shortcut
	
	-- Convert the PathMap to an absolute file path
	videostitchPath = app:MapPath("Reactor:/Deploy/Bin/stitchEm/Studio/videostitch-studio.exe")
	
	-- Add a "VideoStitch Studio.lnk" Shortcut to the Desktop Folder
	CreateShortcut(videostitchPath, "Desktop:", "VideoStitch Studio", "file")
	
	-- ----------------------------------------------------------------------
	-- Batch Stitcher Shortcut
	
	-- Convert the PathMap to an absolute file path
	batchstitchPath = app:MapPath("Reactor:/Deploy/Bin/stitchEm/Studio/batchstitcher.exe")

	-- Add a "Batch Stitcher.lnk" Shortcut to the Desktop Folder
	CreateShortcut(batchstitchPath, "Desktop:", "Batch Stitcher", "file")
end]],
		},
	},
}