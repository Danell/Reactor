{[[You can write the current frame size onscreen in a Text+ node using the StyledText field expression:

Text(Text1.Width .. "x" .. Text1.Height .. "px")

This expression will generate a text string like 1920x1080px. In this example "Text1" is the name of the current Text+ node.

-- Andrew Hazelden]]}
