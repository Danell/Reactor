{[[Fusion will run any script automatically which sits in the same folder as the (template) comp and has the same name. So if you e.g. have TheTemplate.comp and TheTemplate.lua living in the same location, the script will be executed each time you load the comp. 

-- Eric Westphal]]}
