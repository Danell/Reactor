{[[You can use trace() within an expression to print the result within the brackets to the console, to help debug expressions. The trace() function returns its argument, so it is usually transparent when evaluating the expression.

eg: trace(Width*2.4) or trace(Width)*2.4

-- Unknown Genius]]}
