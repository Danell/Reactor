{[[Fusion supports the use of the .ifl file format in Loader and Saver nodes. An .ifl document is simply a textfile that has one image filepath listed per line. Fusion will let you work with the images listed in the ifl file as if it was a traditional image sequence. This is handy if you have non-standard filenames for your images and you don't want to have to rename them.

-- Andrew Hazelden]]}
