--[[--
Media Tree for Fusion - v3.1 2021-25-10
by Andrew Hazelden <andrew@andrewhazelden.com>
www.andrewhazelden.com
updates by Alexey Bogomolov <mail@abogomolov.com>:
* add csv export button, save file to the comp folder
* export only unique file paths
* add refresh button
* click to set active tool
* allow empty file paths in Loaders
* add ParseFilename function from bmd.scriptlib
* use bmd.setclipboard() to copy filepath to clipboard
----------------------------------------------------------------------------

Overview:
This script is a Fusion Lua based UI Manager example that works in Fusion v9-16.1+ and Resolve v15-16.1+. It allows you you quickly view a list of the Fusion saver, loader, and geometry nodes in your composite in a UI Manager based Tree view list.

Installation:
Copy this script to your Fusion user preferences "Scripts/Comp/" folder.

The Linux copy to clipboard command is "xclip"
This requires a custom xclip tool install on Linux:


Debian/Ubuntu:
sudo apt-get install xclip

Redhat/Centos/Fedora:
yum install xclip

Usage:

Step 1. Save your fusion composite to disk.

Step 2. Select the Script > Media Tree menu item. This will open a window with a tree view list of the loader and saver nodes in your composite.

Step 3. Single click on a row in the tree view to copy the filepath to your clipboard. Double click on a row to open the containing folder for the media asset up in a Finder/Explorer/Nautilus folder browsing window.

Step 4. After the "Media Tree" window is open, any time you re-save your Fusion .comp file in Fusion's GUI the contents will be updated automatically in the window.

--]]--

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

-- Find out the current operating system platform. The platform variable should be set to either 'Windows', 'Mac', or 'Linux'.
platform = (FuPLATFORM_WINDOWS and 'Windows') or (FuPLATFORM_MAC and 'Mac') or (FuPLATFORM_LINUX and 'Linux')

------------------------------------------------------------------------
-- Add the platform specific folder slash character
osSeparator = package.config:sub(1,1)

local ui = fu.UIManager
local disp = bmd.UIDispatcher(ui)
local width,height = 1280,600

win = disp:AddWindow({
	ID = 'MediaTreeWin',
	TargetID = 'MediaTreeWin',
	WindowTitle = 'Media Tree',
	Geometry = {100, 100, width, height},
	Spacing = 0,

	ui:VGroup{
		ID = 'root',
		ui:Tree{
			ID = 'Tree',
			SortingEnabled = true,
			Events = {
				ItemDoubleClicked = true,
				ItemClicked = true
			},
		},

		ui:HGroup{
			Weight = 0,
			-- Add your GUI elements here:
			ui:Label{
				ID = 'CommentLabel',
				Text = 'Single click on a row to copy the filepath to your clipboard. Double click on a row to open the containing folder.',
				Alignment = {
					AlignHCenter = true,
					AlignTop = true
				},
			},
		},
        ui:HGroup{
			Weight = 0,
            ui:Button{
                ID = "Refresh",
                Text = "Refresh"
            },            
            ui:Button{
                ID = "SaveCSV",
                Text = "Save CSV"
            },
        },
	},
})

-- The window was closed
function win.On.MediaTreeWin.Close(ev)
	disp:ExitLoop()
end

function win.On.Refresh.Clicked(ev)
    UpdateTree()
end
-- Add your GUI element based event functions here:
itm = win:GetItems()



-- Track the Fusion save events
notify1 = ui:AddNotify('Comp_Save')
notify2 = ui:AddNotify('Comp_SaveVersion')
notify3 = ui:AddNotify('Comp_SaveAs')
notify4 = ui:AddNotify('Comp_SaveCopyAs')

-- The Fusion "Save" command was used
function disp.On.Comp_Save(ev)
	print('[Update] Comp saved. Refreshing the view.')
	UpdateTree()
end

-- The Fusion "Save Version" command was used
function disp.On.Comp_SaveVersion(ev)
	print('[Update] Comp saved as a new version. Refreshing the view.')
	UpdateTree()
end

-- The Fusion "Save As" command was used
function disp.On.Comp_SaveAs(ev)
	print('[Update] Comp saved to a new file. Refreshing the view.')
	UpdateTree()
end

-- The Fusion "Save Copy As" command was used
function disp.On.Comp_SaveCopyAs(ev)
	print('[Update] Comp saved as a copy to a new file. Refreshing the view.')
	UpdateTree()
end

-- Copy the filepath to the clipboard when a Tree view row is clicked on
function win.On.Tree.ItemClicked(ev)
	-- Column 2 = Filepath
	sourceMediaFile = ev.item.Text[2]
    toolName = ev.item.Text[1]
    comp:SetActiveTool(comp:FindTool(toolName))
	-- Copy the filepath to the clipboard
	CopyToClipboard(sourceMediaFile)
	-- print('[Item Selected] ' .. sourceMediaFile)
	-- print('\n')
end

-- Open up the folder where the media is located when a Tree view row is clicked on
function win.On.Tree.ItemDoubleClicked(ev)
	-- Column 2 = Filepath
	sourceMediaFile = ev.item.Text[2]
	
	-- Open up the folder where the media is located
	mediaFolder = Dirname(sourceMediaFile)
	if bmd.fileexists(mediaFolder) then
		OpenDirectory(mediaFolder)
	end
	
	-- print('[Item Selected] ' .. sourceMediaFile)
	-- print('\n')
end


-- Update the contents of the tree view
function UpdateTree()
	-- Clean out the previous entries in the Tree view
	itm.Tree:Clear()

	-- Add the Tree headers:
	-- Loader			Loader1			/Media/Project22/Image.0000.exr			[0-144]			0/0
	hdr = itm.Tree:NewItem()
	hdr.Text[0] = 'Node'
	hdr.Text[1] = 'Name'
	hdr.Text[2] = 'Filepath'
	hdr.Text[3] = 'Frame Range'
	hdr.Text[4] = 'Node X/Y Pos'
	itm.Tree:SetHeaderItem(hdr)

	-- Number of columns in the Tree list
	itm.Tree.ColumnCount = 5

	-- Resize the header column widths
	itm.Tree.ColumnWidth[0] = 120
	itm.Tree.ColumnWidth[1] = 120
	itm.Tree.ColumnWidth[2] = 700
	itm.Tree.ColumnWidth[3] = 100
	itm.Tree.ColumnWidth[4] = 100

	-- -------------------------------------------
	-- Start adding each image element:
	-- -------------------------------------------

	-- Should the selected nodes be listed? (Otherwise all loader/saver nodes will be listed from the comp)
	--listOnlySelectedNodes = true
	listOnlySelectedNodes = false
    comp = fu:GetCurrentComp()

	local toollist1 = comp:GetToolList(listOnlySelectedNodes, 'Loader')
	local toollist2 = comp:GetToolList(listOnlySelectedNodes, 'Saver')
	local toollist3 = comp:GetToolList(listOnlySelectedNodes, 'SurfaceFBXMesh')
	local toollist4 = comp:GetToolList(listOnlySelectedNodes, 'SurfaceAlembicMesh')

	-- Scan the comp to check how many Loader nodes are present
	totalLoaders = table.getn(toollist1)
	totalSavers = table.getn(toollist2)
	totalFBX = table.getn(toollist3)
	totalAlembic = table.getn(toollist4)

	totalNodes = totalLoaders + totalSavers + totalFBX + totalAlembic
	itm.MediaTreeWin.WindowTitle = 'Media Tree: ' .. totalNodes .. ' Nodes'
    
    toolsTable = {}

	-- Iterate through each of the loader nodes
	for i, tool in ipairs(toollist1) do
        sourceMediaFile = tool.Clip[fu.TIME_UNDEFINED]
        table.insert(toolsTable, {tool.ID, comp:MapPath(sourceMediaFile)})
		toolAttrs = tool:GetAttrs()
		toolRegID = tool.ID
		nodeName = tool.Name

		currentMediaStartFrameRange = toolAttrs.TOOLNT_Clip_Start[1]
		currentMediaEndFrameRange = toolAttrs.TOOLNT_Clip_End[1]
		if currentMediaStartFrameRange ~= nil and currentMediaEndFrameRange ~= nil then
			-- Perform a sanity check on the frame range value
			if currentMediaEndFrameRange <= -100000000 then
				currentMediaEndFrameRange = currentMediaStartFrameRange
			end
		
			currentMediaTimeRangeString = '[' .. currentMediaStartFrameRange .. '-' .. currentMediaEndFrameRange .. ']'
		end
		
		-- Get the node position
		flow = comp.CurrentFrame.FlowView
		nodeXpos, nodeYpos = flow:GetPos(tool)
	
		-- print('['	.. toolRegID .. '] ' .. nodeName .. '\t[Image Filename] ' .. sourceMediaFile .. '\t[Node X/Y Pos] ' .. nodeXpos .. ' / ' .. nodeYpos)
	
		-- Add an new entry to the list
		itLoader = itm.Tree:NewItem(); 
		itLoader.Text[0] = 'Loader'; 
		itLoader.Text[1] = nodeName; 
		itLoader.Text[2] = sourceMediaFile; 
		itLoader.Text[3] = currentMediaTimeRangeString;
		itLoader.Text[4] = nodeXpos .. ' / ' .. nodeYpos;
		itm.Tree:AddTopLevelItem(itLoader)
	end

	-- Iterate through each of the saver nodes
	for i, tool in ipairs(toollist2) do 
        table.insert(toolsTable, {tool.ID, comp:MapPath(tool.Clip[1])})
		toolRegID = tool:GetAttrs().TOOLS_RegID
		nodeName = tool:GetAttrs().TOOLS_Name

		sourceMediaFile = comp:MapPath(tool.Clip[fu.TIME_UNDEFINED])

		currentMediaStartFrameRange = comp:GetAttrs().COMPN_RenderStart
		currentMediaEndFrameRange = comp:GetAttrs().COMPN_RenderEnd
		
		-- Perform a sanity check on the frame range value
		if currentMediaEndFrameRange <= -1000000000 then
			currentMediaEndFrameRange = currentMediaStartFrameRange
		end
		
		currentMediaTimeRangeString = '[' .. currentMediaStartFrameRange .. '-' .. currentMediaEndFrameRange .. ']'
		
		-- Get the node position
		flow = comp.CurrentFrame.FlowView
		nodeXpos, nodeYpos = flow:GetPos(tool)
	
		-- print('[' .. toolRegID .. '] ' .. nodeName .. '\t\t[Image Filename] ' .. sourceMediaFile .. '\t[Node X/Y Pos] ' .. nodeXpos .. ' / ' .. nodeYpos)
		
		-- Add an new entry to the list
		itLoader = itm.Tree:NewItem();
		itLoader.Text[0] = 'Saver';
		itLoader.Text[1] = nodeName;
		itLoader.Text[2] = sourceMediaFile;
		itLoader.Text[3] = currentMediaTimeRangeString;
		itLoader.Text[4] = nodeXpos .. ' / ' .. nodeYpos;
		itm.Tree:AddTopLevelItem(itLoader)
	end

	-- Iterate through each of the FBXMesh3D nodes
	for i, tool in ipairs(toollist3) do 
        table.insert(toolsTable, {tool.ID,  comp:MapPath(tool.Clip[1])})
		toolRegID = tool:GetAttrs().TOOLS_RegID
		nodeName = tool:GetAttrs().TOOLS_Name
		
		sourceMediaFile = comp:MapPath(tool:GetInput('ImportFile'))
		
		-- Get the node position
		flow = comp.CurrentFrame.FlowView
		nodeXpos, nodeYpos = flow:GetPos(tool)

		-- print('[' .. toolRegID .. '] ' .. nodeName .. '\t\t[FBXMesh3D Filename] ' .. sourceMediaFile .. '\t[Node X/Y Pos] ' .. nodeXpos .. ' / ' .. nodeYpos)
	
		-- Add an new entry to the list
		itLoader = itm.Tree:NewItem();
		itLoader.Text[0] = 'FBXMesh3D';
		itLoader.Text[1] = nodeName;
		itLoader.Text[2] = sourceMediaFile;
		itLoader.Text[3] = 'N/A';
		itLoader.Text[4] = nodeXpos .. ' / ' .. nodeYpos;
		itm.Tree:AddTopLevelItem(itLoader)
	end
	
	-- Iterate through each of the SurfaceAlembicMesh nodes
	for i, tool in ipairs(toollist4) do 
        table.insert(toolsTable, {tool.ID, comp:MapPath(tool.Filename[1])})
		toolRegID = tool:GetAttrs().TOOLS_RegID
		nodeName = tool:GetAttrs().TOOLS_Name
		
		sourceMediaFile = comp:MapPath(tool:GetInput('Filename'))
		
		-- Get the node position
		flow = comp.CurrentFrame.FlowView
		nodeXpos, nodeYpos = flow:GetPos(tool)

		-- print('[' .. toolRegID .. '] ' .. nodeName .. '\t\t[AlembicMesh3D Filename] ' .. sourceMediaFile .. '\t[Node X/Y Pos] ' .. nodeXpos .. ' / ' .. nodeYpos)
	
		-- Add an new entry to the list
		itLoader = itm.Tree:NewItem(); 
		itLoader.Text[0] = 'AlembicMesh3D'; 
		itLoader.Text[1] = nodeName; 
		itLoader.Text[2] = sourceMediaFile; 
		itLoader.Text[3] = 'N/A';
		itLoader.Text[4] = nodeXpos .. ' / ' .. nodeYpos;
		itm.Tree:AddTopLevelItem(itLoader)
	end

    return toolsTable
end

function csvWrite(path, sep)
    hash = {}
    data = UpdateTree()
    sep = sep or ','
    local file = assert(io.open(path, "w"))
    file:write('Tool Type' .. sep .. 'File Path'..'\n')
    for k, v in pairs(data) do
        if not hash[v[2]] then 
            file:write(v[1] .. sep .. v[2].."\n")
            hash[v[2]] = true
        end
    end
    file:close()
    print("Exported media list to [" .. path .. "]")
end

------------------------------------------------------------------------------
-- parseFilename()
--
-- this is a great function for ripping a filepath into little bits
-- returns a table with the following
--
-- FullPath	: The raw, original path sent to the function
-- Path		: The path, without filename
-- FullName	: The name of the clip w\ extension
-- Name     : The name without extension
-- CleanName: The name of the clip, without extension or sequence
-- SNum		: The original sequence string, or "" if no sequence
-- Number 	: The sequence as a numeric value, or nil if no sequence
-- Extension: The raw extension of the clip
-- Padding	: Amount of padding in the sequence, or nil if no sequence
-- UNC		: A true or false value indicating whether the path is a UNC path or not
------------------------------------------------------------------------------
function ParseFilename(filename)
	local seq = {}
	seq.FullPath = filename
	string.gsub(seq.FullPath, "^(.+[/\\])(.+)", function(path, name) seq.Path = path seq.FullName = name end)
	string.gsub(seq.FullName, "^(.+)(%..+)$", function(name, ext) seq.Name = name seq.Extension = ext end)

	if not seq.Name then -- no extension?
		seq.Name = seq.FullName
	end

	string.gsub(seq.Name,     "^(.-)(%d+)$", function(name, SNum) seq.CleanName = name seq.SNum = SNum end)

	if seq.SNum then
		seq.Number = tonumber( seq.SNum )
		seq.Padding = string.len( seq.SNum )
	else
	   seq.SNum = ""
	   seq.CleanName = seq.Name
	end

	if seq.Extension == nil then seq.Extension = "" end
	seq.UNC = ( string.sub(seq.Path, 1, 2) == [[\\]] )

	return seq
end


function win.On.SaveCSV.Clicked(ev)
    compName = comp:GetAttrs().COMPS_FileName
    if compName == "" then
        print("Save Comp!")
        comp:Save()
    else
        local parseFile = ParseFilename(compName)
        local cleanName = parseFile.CleanName
        local parent = comp:MapPath(parseFile.Path)
        path = parent .. cleanName .. "_MediaList.csv"
        csvWrite(path)
        OpenDirectory(parent)
    end
end

-- Open a folder window up using your desktop file browser
function OpenDirectory(mediaDirName)
	command = nil
	dir = Dirname(mediaDirName)

	-- Open a folder view using the os native command
	if platform == 'Windows' then
		-- Running on Windows
		command = 'explorer "' .. dir .. '"'
		
		-- print("[Launch Command] ", command)
		os.execute(command)
	elseif platform == 'Mac' then
		-- Running on Mac
		command = 'open "' .. dir .. '" &'
					
		-- print("[Launch Command] ", command)
		os.execute(command)
	elseif platform == 'Linux' then
		-- Running on Linux
		command = 'nautilus "' .. dir .. '" &'
					
		-- print("[Launch Command] ", command)
		os.execute(command)
	else
		print('[Platform] ', platform)
		print('There is an invalid platform defined in the local platform variable at the top of the code.')
	end
	
	print('[Opening Directory] ' .. dir)
end


------------------------------------------------------------------------
function Dirname(filename)
    -- Find out the current directory from a file path
    -- Example: print(Dirname("/Volumes/Media/image.exr"))
	return filename:match('(.*' .. tostring(osSeparator) .. ')')
end

function CopyToClipboard(textString)

    -- Copy text to the operating system's clipboard
    -- Example: CopyToClipboard('Hello World!')

    local filePath = comp:MapPath(textString)
	bmd.setclipboard(filePath)
	print('[Copy to Clipboard] ' .. textString)

end


-- Update the contents of the tree view
UpdateTree()

-- The app:AddConfig() command that will capture the "Control + W" or "Control + F4" hotkeys so they will close the window instead of closing the foreground composite.
app:AddConfig('MediaTreeWin', {
	Target {
		ID = 'MediaTreeWin',
	},

	Hotkeys {
		Target = 'MediaTreeWin',
		Defaults = true,

		CONTROL_W = 'Execute{cmd = [[app.UIManager:QueueEvent(obj, "Close", {})]]}',
		CONTROL_F4 = 'Execute{cmd = [[app.UIManager:QueueEvent(obj, "Close", {})]]}',
	},
})

win:Show()
disp:RunLoop()
win:Hide()

app:RemoveConfig('MediaTreeWin')
collectgarbage()

