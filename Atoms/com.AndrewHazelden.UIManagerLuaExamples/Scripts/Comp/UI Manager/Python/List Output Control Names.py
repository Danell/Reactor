# List Output Control Names

ui = fu.UIManager
disp = bmd.UIDispatcher(ui)

dlg = disp.AddWindow({"WindowTitle": "Output Control Names", "ID": "MyWin", "Geometry": [100, 100, 600, 700], "Spacing": 0,},[
	ui.VGroup({"ID": "root",},[
		ui.Label({
			"ID": "TitleLabel",
			"Text": "This script lists the output controls for the currently selected node.",
			"Alignment": {
				"AlignHCenter": True,
				"AlignTop": True
			},
			"Weight": 0.1,
		}),
		ui.Tree({
			"ID": "Tree",
			"SortingEnabled": True,
			"Events": {
				"CurrentItemChanged": True,
				"ItemActivated": True,
				"ItemClicked": True,
				"ItemDoubleClicked": True,
			},
			"Weight": 1.0,
		}),
	]),
])


itm = dlg.GetItems()

# The window was closed
def _func(ev):
	disp.ExitLoop()
dlg.On.MyWin.Close = _func

# Add your GUI element based event functions here:
def _func(ev):
	# Read the current node selection
	selectedTool = comp.ActiveTool
	
	if selectedTool:
		x = selectedTool.GetOutputList()
		nodeName = selectedTool.Name

		# Display the "Node.OutputName" value in the Console
		inputName = str(nodeName) + "." + str(ev["item"].Text[0])
		print(inputName)
dlg.On.Tree.ItemClicked = _func

# Update the contents of the tree view
def UpdateTree():
	# Read the current node selection
	selectedTool = comp.ActiveTool

	# Clean out the previous entries in the Tree view
	itm["Tree"].Clear()
	
	# Add a header row
	hdr = itm["Tree"].NewItem()
	hdr.Text[0] = "Output"
	hdr.Text[1] = "Name"
	hdr.Text[2] = "Data Type"
	itm["Tree"].SetHeaderItem(hdr)

	# Number of columns in the Tree list
	itm["Tree"].ColumnCount = 3

	# Resize the Columns
	itm["Tree"].ColumnWidth[0] = 300
	itm["Tree"].ColumnWidth[1] = 200
	itm["Tree"].ColumnWidth[2] = 100

	# Make sure a node is selected
	if selectedTool:
		inp = selectedTool.GetOutputList()
		nodeName = selectedTool.Name
		
		# Update the window title to track the current node name
		itm["MyWin"].WindowTitle = "Output Controls: " + str(nodeName)
		print("Output Controls: " + str(nodeName))

		# Add an new row entries to the list
		for i in inp:
			itRow = itm["Tree"].NewItem()
			
			itRow.Text[0] = inp[i].ID
			itRow.Text[1] = inp[i].Name
			itRow.Text[2] = inp[i].GetAttrs("OUTS_DataType")

			itm["Tree"].AddTopLevelItem(itRow)
		
		print("[Done]")
	else:
		# Nothing was selected in the flow
		itRow = itm["Tree"].NewItem()
		itRow.Text[0] = "Please select a node in the flow area."
		itm["Tree"].AddTopLevelItem(itRow)
		
		print("Please select a node in the flow area.")


# Update the contents of the tree view
UpdateTree()

dlg.Show()
disp.RunLoop()
dlg.Hide()
