# PSDRexa

## Supported OS
- Windows  
- Mac  

There might be issues if it doesn't work properly on Mac, possibly due to security permission problems. Or it could be a bug.

## Installation Instructions

Please run PSDRexa_WinInstaller.bat.
For Mac, please run PSDRexa_MacInstaller.command

When installed, PSDRexa will be added to the scripts in your workspace.

-------------

### Explanation of settings

* Image output destination: Local output destination for PSD images
* Output images in the size of the base file: If ON, a standing picture of the same size as the loaded PSD is output. If OFF, it is output at the size specified by Height, Width while maintaining the aspect ratio.
* Preview in the size of the base file: Size of the standing picture image for this tool's preview
* Use psdtoolkit feature: If ON, it loads the PSD in response to radio buttons and "!". If OFF, it loads everything with checkboxes.

## License
This project is licensed under the GNU General Public License v3.0 (GPL-3.0).

## Contact and bug reports
Twitter: @nu_ro_ku
