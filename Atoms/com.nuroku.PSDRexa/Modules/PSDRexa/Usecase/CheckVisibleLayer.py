from Domain.DTO.CheckVisibleOperation import CheckVisibleOperation
from Service.PsdMemorySaverService import PsdMemorySaverService


class CheckVisibleLayer:
    def __init__(self):
        pass

    def execute(self, check_visible_operation: CheckVisibleOperation) -> list:
        psd = PsdMemorySaverService.get_psd()

        psd.top.select_layers_by_operation(check_visible_operation)

        # I am careful to include the check status on the Domain side and return it,
        # although it may not be necessary depending on the case.
        mod_req = check_visible_operation.create_modification_request_list(
            psd.top.dump_visible_layer_id_names()
        )

        PsdMemorySaverService.set_psd(psd)

        return mod_req
