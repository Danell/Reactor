{
	Tools = ordered() {
		MarbleDisplace3D = Displace3D {
			CtrlWZoom = false,
			NameSet = true,
			Inputs = {
				SceneInput = Input {
					SourceOp = "ShaderBallMerge3D",
					Source = "Output",
				},
				Scale = Input { Value = 0.02, },
				CameraDisplacement = Input { Value = 1, },
				CameraSelector = Input { Value = FuID { "ShaderBallCamera3D" }, },
				Input = Input {
					SourceOp = "DisplaceOutPipeRouter",
					Source = "Output",
				},
				Comments = Input { Value = "To get the displacement effect out of this material you need to add a Displace3D node and connect its \"SceneInput\" input to a 3D model's \"3DData\" output connection. Then connect the \"kas_MarbleStoneDisplace\" node' s \"DisplacementTextureOutput\" connection to the Displace3D node's \"Input\" connection. The strength of the displacement effect is controlled on the Displace3D node's scale attribute with a good starting value of approximately 0.02.", },
			},
			ViewInfo = OperatorInfo { Pos = { 3190, 511.5 } },
		},
		kas_MarbleStoneDisplace = GroupOperator {
			NameSet = true,
			Inputs = ordered() {
				Comments = Input { Value = "Copyright Credits\n\nFreeware:\nkas_MarbleStone PBR Textures \n\nBy:\nhttps://www.textures.com \n\nIs licensed under:\nCG Textures Freeware ", },
				DisplacementTextureInput = InstanceInput {
					SourceOp = "DisplaceMapPipeRouter",
					Source = "Input",
					Name = "DisplacementTextureInput",
				}
			},
			Outputs = {
				DisplacementTextureOutput = InstanceOutput {
					SourceOp = "DisplaceOutPipeRouter",
					Source = "Output",
					Name = "DisplacementTextureOutput",
				}
			},
			ViewInfo = GroupInfo {
				Pos = { 2970, 511.5 },
				Flags = {
					AllowPan = false,
					AutoSnap = true
				},
				Size = { 268.191, 233.573, 99.6831, 24.2424 },
				Direction = "Horizontal",
				PipeStyle = "Direct",
				Scale = 1,
				Offset = { -3150.46, -474.701 }
			},
			Tools = ordered() {
				MarbleStoneDisplacementLoader = Loader {
					Clips = {
						Clip {
							ID = "Clip1",
							Filename = "Macros:/KickAss ShaderZ/Assets/kas_MarbleStone_Displacement.exr",
							FormatID = "OpenEXRFormat",
							StartFrame = -1,
							LengthSetManually = true,
							TrimIn = 0,
							TrimOut = 0,
							ExtendFirst = 0,
							ExtendLast = 0,
							Loop = 0,
							AspectMode = 0,
							Depth = 0,
							TimeCode = 0,
							GlobalStart = 0,
							GlobalEnd = 0
						}
					},
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						["Clip1.OpenEXRFormat.RedName"] = Input { Value = FuID { "R" }, },
						["Clip1.OpenEXRFormat.GreenName"] = Input { Value = FuID { "G" }, },
						["Clip1.OpenEXRFormat.BlueName"] = Input { Value = FuID { "B" }, },
						["Clip1.OpenEXRFormat.AlphaName"] = Input { Value = FuID { "A" }, },
						Comments = Input { Value = "Copyright Credits\n\nFreeware:\nkas_MarbleStone PBR Textures \n\nBy:\nhttps://www.textures.com \n\nIs licensed under:\nCG Textures Freeware ", },
					},
					ViewInfo = OperatorInfo { Pos = { 3195.63, 542.477 } },
				},
				DisplaceMapPipeRouter = PipeRouter {
					CtrlWShown = false,
					NameSet = true,
					ViewInfo = PipeRouterInfo { Pos = { 3093.81, 488.91 } },
				},
				DisplaceOutPipeRouter = PipeRouter {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Input = Input {
							SourceOp = "DisplacementInputSwitchElseFuse",
							Source = "Output",
						},
					},
					ViewInfo = PipeRouterInfo { Pos = { 3192.8, 653.473 } },
				},
				DisplacementInputSwitchElseFuse = Fuse.SwitchElse {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Switch = Input { Value = 1, },
						Else = Input { Value = 2, },
						Input1 = Input {
							SourceOp = "DisplaceMapPipeRouter",
							Source = "Output",
						},
						Input2 = Input {
							SourceOp = "MarbleStoneDisplacementLoader",
							Source = "Output",
						},
						Comments = Input { Value = "The \"SwitchElse.fuse\" is used to automatically switch the surface material between user supplied imagery, and the fallback option of the internal \"default\" imagery. This input switching happens when nothing is connected to the  surface material's \"GroupOperator\" based \"TextureMap\" and \"EnvironmentMap\" input connections.", },
					},
					ViewInfo = OperatorInfo { Pos = { 3195.63, 580.62 } },
					Version = 100
				}
			},
		},
		kas_ShaderBall = GroupOperator {
			NameSet = true,
			CustomData = {
				HelpPage = "https://www.steakunderwater.com/wesuckless/viewtopic.php?p=25255#p25255",
			},
			Inputs = ordered() {
				Comments = Input { Value = "\"kas_ShaderBall\"  adds a shader ball model to your Fusion 3D system scene. It is used to apply and preview the look of surface materials. Make sure to enable the Fusion 3D workspace \"3D Options > Lighting\" mode so you see an accurate preview of the material in the realtime viewport.\n \nThis node's output is supposed to be connected directly to the \"kas_ShaderPreview\" nodes input connection so you can see a high-quality preview of your surface material. ", },
				LightingControls = Input { Value = 1, },
				MaterialInput = InstanceInput {
					SourceOp = "ShaderBallFBXMesh3D",
					Source = "MaterialInput",
				}
			},
			Outputs = {
				Output1 = InstanceOutput {
					SourceOp = "ShaderBallMerge3D",
					Source = "Output",
				}
			},
			ViewInfo = GroupInfo {
				Pos = { 3190, 577.5 },
				Flags = {
					AllowPan = false,
					AutoSnap = true
				},
				Size = { 545.667, 172.813, 228, 20.7546 },
				Direction = "Horizontal",
				PipeStyle = "Direct",
				Scale = 1,
				Offset = { -483.6, 302.428 }
			},
			Tools = ordered() {
				ShaderBallCamera3D = Camera3D {
					CtrlWShown = false,
					NameSet = true,
					CurrentSettings = 2,
					CustomData = {
						Settings = {
							[1] = {
								Tools = ordered() {
									PreviewCamera3D = Camera3D {
										Inputs = {
											["Transform3DOp.Rotate.X"] = Input { Value = -8.19999504089355 },
											["Transform3DOp.Translate.X"] = Input { Value = -2.39945639043846 },
											["Transform3DOp.Translate.Z"] = Input { Value = 2.30316502834493 },
											["SurfacePlaneInputs.ObjectID.ObjectID"] = Input { Value = 4 },
											["MtlStdInputs.MaterialID"] = Input { Value = 17 },
											AoV = Input { Value = 37.24 },
											["Stereo.Mode"] = Input { Value = FuID { "OffAxis" } },
											["Transform3DOp.Rotate.Y"] = Input { Value = -45.0000267028809 },
											["Transform3DOp.Translate.Y"] = Input { Value = 0.444355502984551 },
											FilmGate = Input { Value = FuID { "BMD_URSA_4K_16x9" } },
											FLength = Input { Value = 22.3900912079698 }
										},
										CtrlWZoom = false,
										NameSet = true,
										ViewInfo = OperatorInfo { Pos = { 550, -246.894 } },
										CustomData = {
										}
									}
								}
							}
						}
					},
					Inputs = {
						["Transform3DOp.Translate.X"] = Input {
							Value = -2.4,
							Expression = "-2.4",
						},
						["Transform3DOp.Translate.Y"] = Input {
							Value = 0.47,
							Expression = "0.47",
						},
						["Transform3DOp.Translate.Z"] = Input {
							Value = 2.3,
							Expression = "2.3",
						},
						["Transform3DOp.Rotate.X"] = Input {
							Value = -8.2,
							Expression = "-8.2",
						},
						["Transform3DOp.Rotate.Y"] = Input {
							Value = -45,
							Expression = "-45.0",
						},
						["Transform3DOp.Rotate.Z"] = Input { Expression = "0.0", },
						AoV = Input { Value = 37.24, },
						FLength = Input { Value = 22.3900912079698, },
						["Stereo.Mode"] = Input { Value = FuID { "OffAxis" }, },
						FilmGate = Input { Value = FuID { "BMD_URSA_4K_16x9" }, },
						IDepth = Input { Value = 300, },
						["SurfacePlaneInputs.BlendMode.Nest"] = Input { Value = 1, },
						["SurfacePlaneInputs.BlendMode.GL.BlendMode"] = Input { Value = FuID { "Replace" }, },
						["SurfacePlaneInputs.BlendMode.SW.BlendMode"] = Input { Value = FuID { "SolidMatte" }, },
						["SurfacePlaneInputs.ObjectID.ObjectID"] = Input { Value = 4, },
						["MtlStdInputs.MaterialID"] = Input { Value = 17, },
					},
					ViewInfo = OperatorInfo { Pos = { 550, -247.5 } },
				},
				ShaderBallReplaceNormals3D = ReplaceNormals3D {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						SceneInput = Input {
							SourceOp = "ShaderBallFBXMesh3D",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 550, -280.5 } },
				},
				ShaderBallDirectionalLight = LightDirectional {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						LightingControls = Input { Value = 1, },
						["Transform3DOp.Rotate.X"] = Input { Expression = "kas_ShaderBall.RotateXInput", },
						["Transform3DOp.Rotate.Y"] = Input { Expression = "kas_ShaderBall.RotateYInput", },
						["Transform3DOp.Rotate.Z"] = Input {
							Value = 35,
							Expression = "kas_ShaderBall.RotateZInput",
						},
						Enabled = Input { Expression = "kas_ShaderBall.LightEnabled", },
						Intensity = Input { Expression = "kas_ShaderBall.LightIntensityInput", },
					},
					ViewInfo = OperatorInfo { Pos = { 715, -181.5 } },
				},
				ShaderBallMerge3D = Merge3D {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						PassThroughLights = Input { Value = 1, },
						SceneInput1 = Input {
							SourceOp = "ShaderBallCamera3D",
							Source = "Output",
						},
						SceneInput2 = Input {
							SourceOp = "ShaderBallReplaceNormals3D",
							Source = "Output",
						},
						SceneInput3 = Input {
							SourceOp = "ShaderBallDirectionalLight",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 715, -247.5 } },
				},
				ShaderBallFBXMesh3D = SurfaceFBXMesh {
					CtrlWShown = false,
					NameSet = true,
					CustomData = {
						NumMtlSlots = 0
					},
					Inputs = {
						ImportFile = Input { Value = "Macros:/KickAss ShaderZ/Assets/kas_ShaderBall.obj", },
						ObjName = Input { Value = "ShaderBall", },
						TransformToWorld = Input { Value = 0, },
						EnableAxisConversion = Input { Value = 1, },
						EnableUnitConversion = Input { Value = 1, },
						Triangulate = Input { Value = 0, },
						ImportVertexColors = Input { Value = 0, },
						["ObjectID.ObjectID"] = Input { Value = 1, },
						MaterialInput = Input {
							SourceOp = "ShaderPipeRouter",
							Source = "Output",
						},
						["MtlStdInputs.MaterialID"] = Input { Value = 1, },
					},
					ViewInfo = OperatorInfo { Pos = { 385, -280.5 } },
				}
			},
			UserControls = ordered() {
				LightEnabled = {
					CBC_TriState = false,
					INP_Integer = false,
					INPID_InputControl = "CheckboxControl",
					INP_Default = 1,
					IC_ControlPage = 1,
					LINKID_DataType = "Number",
					LINKS_Name = "Light Enabled",
				},
				LightingControls = {
					INP_Integer = false,
					LBLC_DropDownButton = true,
					INPID_InputControl = "LabelControl",
					LBLC_NumInputs = 6,
					IC_ControlPage = 1,
					INP_MaxScale = 1,
					INP_MinScale = 0,
					LINKID_DataType = "Number",
					LBLC_NestLevel = 1,
					LINKS_Name = "Lighting Controls",
				},
				LightIntensityInput = {
					INP_MaxAllowed = 100,
					INP_Integer = false,
					INPID_InputControl = "ScrewControl",
					IC_ControlPage = 1,
					INP_MaxScale = 2,
					INP_Default = 1,
					INP_MinScale = 0,
					INP_MinAllowed = 0,
					LINKID_DataType = "Number",
					ICD_Center = 0.5,
					LINKS_Name = "Light Intensity",
				},
				RotateXInput = {
					INP_MaxAllowed = 360,
					INP_Integer = false,
					INPID_InputControl = "ScrewControl",
					IC_Steps = 1,
					IC_ControlPage = 1,
					INP_MaxScale = 180,
					INP_Default = 0,
					INP_MinScale = -180,
					INP_MinAllowed = -360,
					LINKID_DataType = "Number",
					LINKS_Name = "Light Rotate X",
				},
				RotateYInput = {
					INP_MaxAllowed = 360,
					INP_Integer = false,
					INPID_InputControl = "ScrewControl",
					IC_Steps = 1,
					IC_ControlPage = 1,
					INP_MaxScale = 180,
					INP_Default = 0,
					INP_MinScale = -180,
					INP_MinAllowed = -360,
					LINKID_DataType = "Number",
					LINKS_Name = "Light Rotate Y",
				},
				RotateZInput = {
					INP_MaxAllowed = 360,
					INP_Integer = false,
					INPID_InputControl = "ScrewControl",
					IC_Steps = 1,
					IC_ControlPage = 1,
					INP_MaxScale = 180,
					INP_Default = 35,
					INP_MinScale = -180,
					INP_MinAllowed = -360,
					LINKID_DataType = "Number",
					LINKS_Name = "Light Rotate Z",
				},
				LightColorRedInput = {
					INPID_InputControl = "ColorControl",
					IC_ControlPage = 1,
					INP_MaxScale = 5,
					IC_ControlGroup = 2,
					INP_MinScale = -1,
					CLRC_ShowWheel = true,
					LINKID_DataType = "Number",
					IC_ControlID = 0,
					ICD_Center = 1,
					INP_Default = 1,
					LINKS_Name = "Light Color",
				},
				LightColorGreenInput = {
					INPID_InputControl = "ColorControl",
					IC_ControlPage = 1,
					INP_MaxScale = 5,
					IC_ControlGroup = 2,
					INP_MinScale = -1,
					LINKID_DataType = "Number",
					IC_ControlID = 1,
					INP_Default = 1,
					ICD_Center = 1,
				},
				LightColorBlueInput = {
					INPID_InputControl = "ColorControl",
					IC_ControlPage = 1,
					INP_MaxScale = 5,
					IC_ControlGroup = 2,
					INP_MinScale = -1,
					LINKID_DataType = "Number",
					IC_ControlID = 2,
					INP_Default = 1,
					ICD_Center = 1,
				}
			}
		},
		kas_MarbleStone = GroupOperator {
			NameSet = true,
			CustomData = {
				Settings = {
				},
				HelpPage = "https://www.steakunderwater.com/wesuckless/viewtopic.php?p=25255#p25255"
			},
			Inputs = ordered() {
				Comments = Input { Value = "\"kas_MarbleStone\" creates a glossy tiled floor material. You can choose to render this stone with a full surface displacement effect on the mesh.\n\nCopyright Credits\n\nFreeware:\nkas_MarbleStone PBR Textures \n\nBy:\nhttps://www.textures.com \n\nIs licensed under:\nCG Textures Freeware ", },
				AlbedoTextureMap = InstanceInput {
					SourceOp = "AlbedoInputSwitchElseFuse",
					Source = "Input1",
					Name = "AlbedoMap",
				},
				RoughnessTextureMap = InstanceInput {
					SourceOp = "RoughnessTextureInputSwitchElseFuse",
					Source = "Input1",
					Name = "RoughnessMap",
				},
				NormalTextureMap = InstanceInput {
					SourceOp = "NormalInputSwitchElseFuse",
					Source = "Input1",
					Name = "NormalMap",
				},
				GlossTextureMap = InstanceInput {
					SourceOp = "GlossInputSwitchElseFuse",
					Source = "Input1",
					Name = "GlossMap",
				},
				AOTextureMap = InstanceInput {
					SourceOp = "AOInputSwitchElseFuse",
					Source = "Input1",
					Name = "AOMap",
				},
				StrengthVariability = InstanceInput {
					SourceOp = "MarbleStoneReflect",
					Source = "Reflection.StrengthVariability",
					Default = 1,
				},
				GlancingStrength = InstanceInput {
					SourceOp = "MarbleStoneReflect",
					Source = "Reflection.GlancingStrength",
					Default = 0.5,
				},
				FaceOnStrength = InstanceInput {
					SourceOp = "MarbleStoneReflect",
					Source = "Reflection.FaceOnStrength",
					Default = 0.05,
				},
				Falloff = InstanceInput {
					SourceOp = "MarbleStoneReflect",
					Source = "Reflection.Falloff",
					Default = 2.5,
				},
				RefractiveIndex = InstanceInput {
					SourceOp = "MarbleStoneReflect",
					Source = "Refraction.RefractiveIndex.RGB",
					Default = 2,
				}
			},
			Outputs = {
				MaterialOutput = InstanceOutput {
					SourceOp = "ShaderPipeRouter",
					Source = "Output",
					Name = "Material Output",
				}
			},
			ViewInfo = GroupInfo {
				Pos = { 2970, 577.5 },
				Flags = {
					AllowPan = false,
					AutoSnap = true
				},
				Size = { 853.25, 401.206, 231.814, 33.3374 },
				Direction = "Horizontal",
				PipeStyle = "Direct",
				Scale = 1,
				Offset = { -1110.15, -285.097 }
			},
			Tools = ordered() {
				StNicolasChurch_IBL_Loader = Loader {
					Clips = {
						Clip {
							ID = "Clip1",
							Filename = "Macros:/KickAss ShaderZ/Assets/kas_StNicholasChurch_IBL.exr",
							FormatID = "OpenEXRFormat",
							StartFrame = -1,
							LengthSetManually = true,
							TrimIn = 0,
							TrimOut = 0,
							ExtendFirst = 0,
							ExtendLast = 0,
							Loop = 0,
							AspectMode = 0,
							Depth = 0,
							TimeCode = 0,
							GlobalStart = 0,
							GlobalEnd = 0
						}
					},
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						["Clip1.OpenEXRFormat.RedName"] = Input { Value = FuID { "R" }, },
						["Clip1.OpenEXRFormat.GreenName"] = Input { Value = FuID { "G" }, },
						["Clip1.OpenEXRFormat.BlueName"] = Input { Value = FuID { "B" }, },
						["Clip1.OpenEXRFormat.AlphaName"] = Input { Value = FuID { "A" }, },
						Comments = Input { Value = "St. Nicholas Church, Gdansk, Poland IBL \n\nCreative Commons:\nSt. Nicolaus Church Interior HDRI \n\nBy:\nhttps://hdrmaps.com \n\nIs licensed under:\nCC BY 2.0 ", },
					},
					ViewInfo = OperatorInfo { Pos = { 1540, 313.5 } },
				},
				MarbleStoneGlossLoader = Loader {
					Clips = {
						Clip {
							ID = "Clip1",
							Filename = "Macros:/KickAss ShaderZ/Assets/kas_MarbleStone_Gloss.jpg",
							FormatID = "JpegFormat",
							StartFrame = -1,
							LengthSetManually = true,
							TrimIn = 0,
							TrimOut = 0,
							ExtendFirst = 0,
							ExtendLast = 0,
							Loop = 0,
							AspectMode = 0,
							Depth = 0,
							TimeCode = 0,
							GlobalStart = 0,
							GlobalEnd = 0
						}
					},
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						Comments = Input { Value = "Copyright Credits\n\nFreeware:\nkas_MarbleStone PBR Textures \n\nBy:\nhttps://www.textures.com \n\nIs licensed under:\nCG Textures Freeware ", },
					},
					ViewInfo = OperatorInfo { Pos = { 1320, 313.5 } },
				},
				MarbleStoneNormalLoader = Loader {
					Clips = {
						Clip {
							ID = "Clip1",
							Filename = "Macros:/KickAss ShaderZ/Assets/kas_MarbleStone_Normal.jpg",
							FormatID = "JpegFormat",
							StartFrame = -1,
							LengthSetManually = true,
							TrimIn = 0,
							TrimOut = 0,
							ExtendFirst = 0,
							ExtendLast = 0,
							Loop = 0,
							AspectMode = 0,
							Depth = 0,
							TimeCode = 0,
							GlobalStart = 0,
							GlobalEnd = 0
						}
					},
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						["Clip1.OpenEXRFormat.GreenName"] = Input { Value = FuID { "G" }, },
						["Clip1.OpenEXRFormat.BlueName"] = Input { Value = FuID { "B" }, },
						["Clip1.OpenEXRFormat.RedName"] = Input { Value = FuID { "R" }, },
						["Clip1.OpenEXRFormat.AlphaName"] = Input { Value = FuID { "A" }, },
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						Comments = Input { Value = "Copyright Credits\n\nFreeware:\nkas_MarbleStone PBR Textures \n\nBy:\nhttps://www.textures.com \n\nIs licensed under:\nCG Textures Freeware ", },
					},
					ViewInfo = OperatorInfo { Pos = { 1210, 313.5 } },
				},
				MarbleStoneRoughnessLoader = Loader {
					Clips = {
						Clip {
							ID = "Clip1",
							Filename = "Macros:/KickAss ShaderZ/Assets/kas_MarbleStone_Roughness.jpg",
							FormatID = "JpegFormat",
							StartFrame = -1,
							LengthSetManually = true,
							TrimIn = 0,
							TrimOut = 0,
							ExtendFirst = 0,
							ExtendLast = 0,
							Loop = 0,
							AspectMode = 0,
							Depth = 0,
							TimeCode = 0,
							GlobalStart = 0,
							GlobalEnd = 0
						}
					},
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						Comments = Input { Value = "Copyright Credits\n\nFreeware:\nkas_MarbleStone PBR Textures \n\nBy:\nhttps://www.textures.com \n\nIs licensed under:\nCG Textures Freeware ", },
					},
					ViewInfo = OperatorInfo { Pos = { 1100, 313.5 } },
				},
				MarbleStoneAlbedoLoader = Loader {
					Clips = {
						Clip {
							ID = "Clip1",
							Filename = "Macros:/KickAss ShaderZ/Assets/kas_MarbleStone_Albedo.jpg",
							FormatID = "JpegFormat",
							StartFrame = -1,
							LengthSetManually = true,
							TrimIn = 0,
							TrimOut = 0,
							ExtendFirst = 0,
							ExtendLast = 0,
							Loop = 0,
							AspectMode = 0,
							Depth = 0,
							TimeCode = 0,
							GlobalStart = 0,
							GlobalEnd = 0
						}
					},
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						Comments = Input { Value = "Copyright Credits\n\nFreeware:\nkas_MarbleStone PBR Textures \n\nBy:\nhttps://www.textures.com \n\nIs licensed under:\nCG Textures Freeware ", },
					},
					ViewInfo = OperatorInfo { Pos = { 990, 313.5 } },
				},
				MarbleStoneAOLoader = Loader {
					Clips = {
						Clip {
							ID = "Clip1",
							Filename = "Macros:/KickAss ShaderZ/Assets/kas_MarbleStone_AO.jpg",
							FormatID = "JpegFormat",
							StartFrame = -1,
							LengthSetManually = true,
							TrimIn = 0,
							TrimOut = 0,
							ExtendFirst = 0,
							ExtendLast = 0,
							Loop = 0,
							AspectMode = 0,
							Depth = 0,
							TimeCode = 0,
							GlobalStart = 0,
							GlobalEnd = 0
						}
					},
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						["Clip1.OpenEXRFormat.GreenName"] = Input { Value = FuID { "G" }, },
						["Clip1.OpenEXRFormat.BlueName"] = Input { Value = FuID { "B" }, },
						["Clip1.OpenEXRFormat.RedName"] = Input { Value = FuID { "R" }, },
						["Clip1.OpenEXRFormat.AlphaName"] = Input { Value = FuID { "A" }, },
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						Comments = Input { Value = "Copyright Credits\n\nFreeware:\nkas_MarbleStone PBR Textures \n\nBy:\nhttps://www.textures.com \n\nIs licensed under:\nCG Textures Freeware ", },
					},
					ViewInfo = OperatorInfo { Pos = { 1430, 313.5 } },
				},
				PurpleQuartzPipeRouter = PipeRouter {
					CtrlWShown = false,
					NameSet = true,
					ViewInfo = PipeRouterInfo { Pos = { 2530, 775.5 } },
				},
				AlbedoInputSwitchElseFuse = Fuse.SwitchElse {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Switch = Input { Value = 1, },
						Else = Input { Value = 2, },
						Input2 = Input {
							SourceOp = "MarbleStoneAlbedoLoader",
							Source = "Output",
						},
						Comments = Input { Value = "The \"SwitchElse.fuse\" is used to automatically switch the surface material between user supplied imagery, and the fallback option of the internal \"default\" imagery. This input switching happens when nothing is connected to the  surface material's \"GroupOperator\" based \"TextureMap\" and \"EnvironmentMap\" input connections.", },
					},
					ViewInfo = OperatorInfo { Pos = { 990, 346.5 } },
					Version = 100
				},
				RoughnessTextureInputSwitchElseFuse = Fuse.SwitchElse {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Switch = Input { Value = 1, },
						Else = Input { Value = 2, },
						Input2 = Input {
							SourceOp = "MarbleStoneRoughnessLoader",
							Source = "Output",
						},
						Comments = Input { Value = "The \"SwitchElse.fuse\" is used to automatically switch the surface material between user supplied imagery, and the fallback option of the internal \"default\" imagery. This input switching happens when nothing is connected to the  surface material's \"GroupOperator\" based \"TextureMap\" and \"EnvironmentMap\" input connections.", },
					},
					ViewInfo = OperatorInfo { Pos = { 1100, 346.5 } },
					Version = 100
				},
				NormalInputSwitchElseFuse = Fuse.SwitchElse {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Switch = Input { Value = 1, },
						Else = Input { Value = 2, },
						Input2 = Input {
							SourceOp = "MarbleStoneNormalLoader",
							Source = "Output",
						},
						Comments = Input { Value = "The \"SwitchElse.fuse\" is used to automatically switch the surface material between user supplied imagery, and the fallback option of the internal \"default\" imagery. This input switching happens when nothing is connected to the  surface material's \"GroupOperator\" based \"TextureMap\" and \"EnvironmentMap\" input connections.", },
					},
					ViewInfo = OperatorInfo { Pos = { 1210, 346.5 } },
					Version = 100
				},
				GlossInputSwitchElseFuse = Fuse.SwitchElse {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Switch = Input { Value = 1, },
						Else = Input { Value = 2, },
						Input2 = Input {
							SourceOp = "MarbleStoneGlossLoader",
							Source = "Output",
						},
						Comments = Input { Value = "The \"SwitchElse.fuse\" is used to automatically switch the surface material between user supplied imagery, and the fallback option of the internal \"default\" imagery. This input switching happens when nothing is connected to the  surface material's \"GroupOperator\" based \"TextureMap\" and \"EnvironmentMap\" input connections.", },
					},
					ViewInfo = OperatorInfo { Pos = { 1320, 346.5 } },
					Version = 100
				},
				AOInputSwitchElseFuse = Fuse.SwitchElse {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Switch = Input { Value = 1, },
						Else = Input { Value = 2, },
						Input2 = Input {
							SourceOp = "MarbleStoneAOLoader",
							Source = "Output",
						},
						Comments = Input { Value = "The \"SwitchElse.fuse\" is used to automatically switch the surface material between user supplied imagery, and the fallback option of the internal \"default\" imagery. This input switching happens when nothing is connected to the  surface material's \"GroupOperator\" based \"TextureMap\" and \"EnvironmentMap\" input connections.", },
					},
					ViewInfo = OperatorInfo { Pos = { 1430, 346.5 } },
					Version = 100
				},
				ShaderPipeRouter = PipeRouter {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Input = Input {
							SourceOp = "MarbleStoneReflect",
							Source = "MaterialOutput",
						},
					},
					ViewInfo = PipeRouterInfo { Pos = { 1650, 610.5 } },
				},
				GlossBrightnessContrast = BrightnessContrast {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Low = Input { Value = 0.345, },
						High = Input { Value = 0.522, },
						Input = Input {
							SourceOp = "InvertCustomTool",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 1320, 445.5 } },
				},
				MarbleStoneReflect = MtlReflect {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						BackgroundMaterial = Input {
							SourceOp = "MarbleStoneCookTorrance",
							Source = "MaterialOutput",
						},
						["Reflection.GlancingStrength"] = Input { Value = 0.5, },
						["Reflection.FaceOnStrength"] = Input { Value = 0.05, },
						["Reflection.Falloff"] = Input { Value = 2.5, },
						["Reflection.Color.Material"] = Input {
							SourceOp = "PanoDefocus",
							Source = "Output",
						},
						["Refraction.RefractiveIndex.RGB"] = Input { Value = 2, },
						MaterialID = Input { Value = 3, },
					},
					ViewInfo = OperatorInfo { Pos = { 1540, 610.5 } },
				},
				PanoDefocus = Defocus {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Filter = Input { Value = 1, },
						XDefocusSize = Input { Value = 6.5, },
						BloomLevel = Input { Value = 0, },
						BloomThreshold = Input { Value = 1, },
						Input = Input {
							SourceOp = "PanoColorCorrector",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 1540, 445.5 } },
				},
				InvertCustomTool = Custom {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						LUTIn1 = Input {
							SourceOp = "InvertCustomToolLUTIn1",
							Source = "Value",
						},
						LUTIn2 = Input {
							SourceOp = "InvertCustomToolLUTIn2",
							Source = "Value",
						},
						LUTIn3 = Input {
							SourceOp = "InvertCustomToolLUTIn3",
							Source = "Value",
						},
						LUTIn4 = Input {
							SourceOp = "InvertCustomToolLUTIn4",
							Source = "Value",
						},
						RedExpression = Input { Value = "1-r1", },
						GreenExpression = Input { Value = "1-g1", },
						BlueExpression = Input { Value = "1-b1", },
						AlphaExpression = Input { Value = "1", },
						Image1 = Input {
							SourceOp = "GlossTransform",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 1320, 412.5 } },
				},
				PanoColorCorrector = ColorCorrector {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						MasterRGBGain = Input { Value = 0.75, },
						MasterRGBGamma = Input { Value = 1.4, },
						ColorRanges = Input {
							Value = ColorCurves {
								Curves = {
									{
										Points = {
											{ 0, 1 },
											{ 0.4, 0.2 },
											{ 0.6, 0 },
											{ 1, 0 }
										}
									},
									{
										Points = {
											{ 0, 0 },
											{ 0.4, 0 },
											{ 0.6, 0.2 },
											{ 1, 1 }
										}
									}
								}
							},
						},
						HistogramIgnoreTransparent = Input { Value = 1, },
						Input = Input {
							SourceOp = "PanoResize",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 1540, 412.5 } },
				},
				PanoResize = BetterResize {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Width = Input {
							Value = 2048,
							Expression = "Height*2",
						},
						Height = Input { Value = 1024, },
						KeepAspect = Input { Value = 1, },
						HiQOnly = Input { Value = 0, },
						PixelAspect = Input { Value = { 1, 1 }, },
						Input = Input {
							SourceOp = "PanoInputSwitchElseFuse",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 1540, 379.5 } },
				},
				NormalTransform = Transform {
					CtrlWShown = false,
					NameSet = true,
					SourceOp = "AlbedoTransform",
					Inputs = {
						EffectMask = Input { },
						SettingsNest = Input { },
						TransformNest = Input { },
						ReferenceSize = Input { },
						Input = Input {
							SourceOp = "NormalInputSwitchElseFuse",
							Source = "Output",
						},
						CommentsNest = Input { },
						FrameRenderScriptNest = Input { },
						StartRenderScripts = Input { },
						EndRenderScripts = Input { },
					},
					ViewInfo = OperatorInfo { Pos = { 1210, 379.5 } },
				},
				GlossTransform = Transform {
					CtrlWShown = false,
					NameSet = true,
					SourceOp = "AlbedoTransform",
					Inputs = {
						EffectMask = Input { },
						SettingsNest = Input { },
						TransformNest = Input { },
						ReferenceSize = Input { },
						Input = Input {
							SourceOp = "GlossInputSwitchElseFuse",
							Source = "Output",
						},
						CommentsNest = Input { },
						FrameRenderScriptNest = Input { },
						StartRenderScripts = Input { },
						EndRenderScripts = Input { },
					},
					ViewInfo = OperatorInfo { Pos = { 1320, 379.5 } },
				},
				NormaBumpMap = BumpMap {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						SourceImageType = Input { Value = 1, },
						Input = Input {
							SourceOp = "NormalTransform",
							Source = "Output",
						},
						HeightScale = Input { Value = 200, },
						BumpmapTextureDepth = Input { Value = 4, },
						["FilterMode.Nest"] = Input { Value = 1, },
						MaterialID = Input { Value = 21, },
					},
					ViewInfo = OperatorInfo { Pos = { 1210, 412.5 } },
				},
				RoughnessTransform = Transform {
					CtrlWShown = false,
					NameSet = true,
					SourceOp = "AlbedoTransform",
					Inputs = {
						EffectMask = Input { },
						SettingsNest = Input { },
						TransformNest = Input { },
						ReferenceSize = Input { },
						Input = Input {
							SourceOp = "RoughnessTextureInputSwitchElseFuse",
							Source = "Output",
						},
						CommentsNest = Input { },
						FrameRenderScriptNest = Input { },
						StartRenderScripts = Input { },
						EndRenderScripts = Input { },
					},
					ViewInfo = OperatorInfo { Pos = { 1100, 379.5 } },
				},
				RoughnessBrightnessContrast = BrightnessContrast {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Gain = Input { Value = 2.025, },
						Contrast = Input { Value = 1, },
						Input = Input {
							SourceOp = "RoughnessTransform",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 1100, 412.5 } },
				},
				MarbleStoneCookTorrance = MtlCookTorrance {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						["Diffuse.Color.Material"] = Input {
							SourceOp = "AlbedoColorCorrector",
							Source = "Output",
						},
						["Specular.Nest"] = Input { Value = 1, },
						["Specular.Color.Material"] = Input {
							SourceOp = "AOTransform",
							Source = "Output",
						},
						["Specular.Intensity"] = Input { Value = 2, },
						["Specular.Intensity.Material"] = Input {
							SourceOp = "GlossBrightnessContrast",
							Source = "Output",
						},
						["Specular.Roughness"] = Input { Value = 0.1, },
						["Specular.Roughness.Material"] = Input {
							SourceOp = "RoughnessBrightnessContrast",
							Source = "Output",
						},
						["Bumpmap.Material"] = Input {
							SourceOp = "NormaBumpMap",
							Source = "MaterialOutput",
						},
						MaterialID = Input { Value = 2, },
					},
					ViewInfo = OperatorInfo { Pos = { 1320, 610.5 } },
				},
				AlbedoColorCorrector = ColorCorrector {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						CorrectionRange = Input { Value = 1, },
						WheelRangeDummy1 = Input { Value = 1, },
						WheelTintAngle2 = Input { Value = 0.0935835209054994, },
						WheelTintLength2 = Input { Value = 0.1091, },
						SuppressionRangeDummy = Input { Value = 1, },
						ColorRanges = Input {
							Value = ColorCurves {
								Curves = {
									{
										Points = {
											{ 0, 1 },
											{ 0.4, 0.2 },
											{ 0.6, 0 },
											{ 1, 0 }
										}
									},
									{
										Points = {
											{ 0, 0 },
											{ 0.4, 0 },
											{ 0.6, 0.2 },
											{ 1, 1 }
										}
									}
								}
							},
						},
						HistogramIgnoreTransparent = Input { Value = 1, },
						Input = Input {
							SourceOp = "AlbedoTransform",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 990, 412.5 } },
				},
				AlbedoTransform = Transform {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Center = Input { Value = { 0.689984223574487, 0.308429118773946 }, },
						Size = Input { Value = 0.75, },
						Angle = Input { Value = -33.054486033619, },
						Edges = Input { Value = 1, },
						Input = Input {
							SourceOp = "AlbedoInputSwitchElseFuse",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 990, 379.5 } },
				},
				AOTransform = Transform {
					CtrlWShown = false,
					NameSet = true,
					SourceOp = "AlbedoTransform",
					Inputs = {
						EffectMask = Input { },
						SettingsNest = Input { },
						TransformNest = Input { },
						ReferenceSize = Input { },
						Input = Input {
							SourceOp = "AOInputSwitchElseFuse",
							Source = "Output",
						},
						CommentsNest = Input { },
						FrameRenderScriptNest = Input { },
						StartRenderScripts = Input { },
						EndRenderScripts = Input { },
					},
					ViewInfo = OperatorInfo { Pos = { 1430, 379.5 } },
				},
				PanoInputSwitchElseFuse = Fuse.SwitchElse {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Switch = Input { Value = 1, },
						Else = Input { Value = 2, },
						Input2 = Input {
							SourceOp = "StNicolasChurch_IBL_Loader",
							Source = "Output",
						},
						Comments = Input { Value = "The \"SwitchElse.fuse\" is used to automatically switch the surface material between user supplied imagery, and the fallback option of the internal \"default\" imagery. This input switching happens when nothing is connected to the  surface material's \"GroupOperator\" based \"TextureMap\" and \"EnvironmentMap\" input connections.", },
					},
					ViewInfo = OperatorInfo { Pos = { 1540, 346.5 } },
					Version = 100
				}
			},
		},
		InvertCustomToolLUTIn1 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 204, Green = 0, Blue = 0 },
		},
		InvertCustomToolLUTIn2 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 0, Green = 204, Blue = 0 },
		},
		InvertCustomToolLUTIn3 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 0, Green = 0, Blue = 204 },
		},
		InvertCustomToolLUTIn4 = LUTBezier {
			KeyColorSplines = {
				[0] = {
					[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
					[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
				}
			},
			SplineColor = { Red = 204, Green = 204, Blue = 204 },
		}
	}
}
