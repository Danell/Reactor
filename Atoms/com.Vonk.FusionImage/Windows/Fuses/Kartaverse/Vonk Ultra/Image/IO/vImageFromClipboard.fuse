--[[--
    Based on Daniel Koch's GetClipboard fuse
    https://eyeondev.blogspot.com/2010/03/loading-clipboard-bitmap-into-fusion.html
    Note: The fuse works on Windows and macOS
--]]--

-- ============================================================================
-- modules
-- ============================================================================

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vImageFromClipboard"
DATATYPE = "Image"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Image\\IO",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Grabs, saves then loads the current clipboard image.",
    REGS_OpIconString = FUSE_NAME
})

function GetPlatform()
    -- Find out the current Fusion host platform (Windows/Mac/Linux)
    local platform = ""
    if jit.os == "Windows" then
        platform = "Windows"
    elseif jit.os == "Linux" then
        platform = "Linux"
    elseif jit.os == "OSX" then
        platform = "Mac"
    else
        platform = "Linux"
    end

    return platform
end

function GetImage()
    print("[vImageFromClipboard] Grabbing Image")
    local filepath
    if GetPlatform() == "Windows" then
        filepath = self.Comp:MapPath("Temp:/clipboard.bmp")
    elseif GetPlatform() == "Mac" then
        filepath = self.Comp:MapPath("Temp:/clipboard.png")
    end
    --dump(filepath)

    if GetPlatform() == "Windows" then
        -- get current path
        local localdir = string.match(debug.getinfo(1).source, '@(.*[/\\])')
        --dump(localdir)

        local commandString = '"' .. tostring(localdir) .. 'clipboard2bmp.exe" ' .. tostring(filepath)
        --print("[Command] ", commandString)
        --print(bmd.executebg(commandString, true))
        os.execute(commandString)
    elseif GetPlatform() == "Mac" then
        scriptpath = self.Comp:MapPath("Temp:/clipboard.txt")
        -- Open up the file pointer for the output textfile
        outFile, err = io.open(scriptpath, "w")
        if err then
            return
        end

        local script = [[
try
	set image to the clipboard as «class PNGf»
	set filename to open for access POSIX path of (POSIX file "]] .. tostring(filepath) .. [[") with write permission
	write image to filename
	close access filename
on error errMsg number errorNumber
	copy errMsg & " Error Number: " & errorNumber to stdout
end try
]]

        --print('[AppleScript Filename] ', scriptpath)
        --print('[Script Contents]')
        --print(script)

        outFile:write(script)
        outFile:write('\n')
        outFile:close()

        local command = [[osascript "]] .. scriptpath .. [["]]
        --print("[Apple Script][Command] ", command)
        os.execute(command)

        -- Clear the temporary files
        --print("[Removing Temp file] ", scriptpath)
        os.remove(scriptpath)
    end

    return filepath
end

function Create()
    InGrab = self:AddInput("Grab", "Grab", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ButtonControl",
        INP_DoNotifyChanged = true,
    })

    OutImage = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Image",
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)
    if inp == InGrab and param and param.Value == 1 then
        GetImage()
    end
end

function Process(req)
    local filepath

    if GetPlatform() == "Windows" then
        filepath = self.Comp:MapPath("Temp:/clipboard.bmp")
    elseif GetPlatform() == "Mac" then
        filepath = self.Comp:MapPath("Temp:/clipboard.png")
    end
    --dump(filepath)
    
    if GetPlatform() == "Windows" or GetPlatform() == "Mac" then
        if bmd.fileexists(filepath) == true then
            local FuVersion = tonumber(bmd._VERSION:sub(1,2))
            if FuVersion <= 16 then
                -- Fusion 9 - 16.0
                --print("[vImageFromClipboard] Fusion v9-16.0")
                local clip = Clip(filepath)
                out = clip:GetFrame(0)
                if out then
                    OutImage:Set(req, out)
                    return
                end
            else
                -- Fusion 16.1+
                --print("[vImageFromClipboard] Fusion v16.1-18.5+")
                local clip = Clip(filepath, false)
                clip:Open()
                out = clip:GetFrame(0)
                clip:Close()

                if out then
                    OutImage:Set(req, out)
                    return
                end
            end
        else
            print("[vImageFromClipboard] Warning: Clipboard image missing from disk.")
      end
    else
        print("[vImageFromClipboard] Warning: This fuse works on Windows and macOS only.")
    end

    local rgba = Pixel({R = r, G = g, B = b, A = a})
    local out = Image({
        IMG_Width = 1920,
        IMG_Height = 1080,
        IMG_NoData = req:IsPreCalc(),
        IMG_Depth = IMDP_128bitFloat,
    })
    out:Fill(rgba)

    OutImage:Set(req, out)
end
