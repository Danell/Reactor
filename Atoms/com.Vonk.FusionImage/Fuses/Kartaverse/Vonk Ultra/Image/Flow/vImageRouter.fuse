-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vImageRouter"
DATATYPE = "Image"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Image\\Flow",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Control the output routing of a Fusion Image object.",
    REGS_OpIconString = FUSE_NAME,
    REG_TimeVariant = true, -- required to disable caching of the current time parameter
    REGB_Temporal = true, -- ensures reliability in Resolve 15
})

function Create()

    InWhich = self:AddInput("Which", "Which", {
        LINKID_DataType    = "Number",
        INPID_InputControl = "SliderControl",
        INP_MinAllowed     = 1,
        INP_MaxAllowed     = 64,
        INP_MaxScale       = 2,
        INP_Integer        = true,
        IC_Steps           = 1.0,
        INP_DoNotifyChanged = true,
        LINK_Main = 1,
    })

    InImage = self:AddInput("Input", "Input", {
        LINKID_DataType = "Image",
        LINK_Main = 2,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutImage1 = self:AddOutput("Output1", "Output1", {
        LINKID_DataType = "Image",
        LINK_Main = 1,
    })

    OutImage2 = self:AddOutput("Output2", "Output2", {
        LINKID_DataType = "Image",
        LINK_Main = 2,
    })
end


function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InWhich:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local which = InWhich:GetValue(req).Value

    local img = InImage:GetValue(req)
    local empty = Image({IMG_Like = img, IMG_NoData = true})

    -- Crop (with no offset, ie. Copy) handles images having no data, so we don't need to put this within if/then/end
    local result = Image({IMG_Like = img, IMG_NoData = req:IsPreCalc()})
    img:Crop(result, {})

    if which == 1 then
        -- Output 1
        OutImage1:Set(req, result)
        OutImage2:Set(req, empty)
    else
        -- Output 2
        OutImage1:Set(req, empty)
        OutImage2:Set(req, result)
    end
end
