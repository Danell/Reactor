-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vNumberTwoArgumentArcTangent"
DATATYPE = "Number"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Number\\Trigonometry",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Returns the arc tangent of y/x (in radians), but uses the signs of both parameters to find the quadrant of the result.",
    REGS_OpIconString = FUSE_NAME
})

function Create()
    -- [[ Creates the user interface. ]]
    InX = self:AddInput("X" , "X" , {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        LINK_Main = 1,
        INP_MinScale = -100,
        INP_MaxScale = 100,
        INP_MinAllowed = -1e+38,
        INP_MaxAllowed = 1e+38,
        -- INP_MinAllowed = -1000000,
        -- INP_MaxAllowed = 1000000,
        INP_Default = 0
    })

    InY = self:AddInput("Y" , "Y" , {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        LINK_Main = 2,
        INP_MinScale = -100,
        INP_MaxScale = 100,
        INP_Default = 0
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutNumber = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Number",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end
        InX:SetAttrs({LINK_Visible = visible})
        InY:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local x = InX:GetValue(req).Value
    local y = InY:GetValue(req).Value

    local result = math.atan2( y , x )
    local out = Number(result)
    OutNumber:Set(req, out)
end