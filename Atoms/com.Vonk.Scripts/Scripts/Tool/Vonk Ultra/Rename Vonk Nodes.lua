--[[--
Rename Vonk Nodes

This script will process the following Vonk node types and automatically rename them based upon the Key or CSV Heading field contents:

- vJSONGet
- vJSONSet
- vArrayGet
- vScriptValGetToText
- vScriptValGetToTable
- vScriptValGetToNumber
- vNumberFromCSV
- vTextFromCSV
- vArrayGet

--]]--

local vtextutils = require("vtextutils")

-- Read the selection
-- local tools = comp:GetToolList(true)

comp:StartUndo("Rename Node")

-- Process the fuses
--for _i, tool in pairs(tools) do
    if tool.ID:match("^Fuse") then
        local PrevName = tool.Name
        local NewName = ""

        if tool.ID:match("^Fuse.vJSONGet$") then
            NewName = "json_" .. tostring(tool:GetInput("Key", comp.CurrentTime) or "")
        elseif tool.ID:match("^Fuse.vJSONSet$") then
            NewName = "json_" .. tostring(tool:GetInput("Key", comp.CurrentTime) or "")
        elseif tool.ID:match("^Fuse.vArrayGet$") then
            NewName = "array_" .. tostring(tool:GetInput("Key", comp.CurrentTime) or "")
        elseif tool.ID:match("^Fuse.vScriptValGetToText") then
            NewName = "scriptval_" .. tostring(tool:GetInput("Key", comp.CurrentTime) or "")
        elseif tool.ID:match("^Fuse.vScriptValGetToTable") then
            NewName = "scriptval_" .. tostring(tool:GetInput("Key", comp.CurrentTime) or "")
        elseif tool.ID:match("^Fuse.vScriptValGetToNumber") then
            NewName = "scriptval_" .. tostring(tool:GetInput("Key", comp.CurrentTime) or "")
        elseif tool.ID:match("^Fuse.vArrayGet") then
            NewName = "array_" .. tostring(tool:GetInput("Key", comp.CurrentTime) or "")
        elseif tool.ID:match("^Fuse.vNumberFromCSV") then
            local data = tool:GetInput("Input", comp.CurrentTime)
            local row = 1
            local col = tool:GetInput("Column")

            local pattern = "(.-),"

            -- A lazy workaround for greedy Lua pattern matching of the last comma in a CSV line
            local line = tostring(textutils.ReadLineIgnoreComments(data, row, "#")) .. ","

            local array = textutils.split(line, pattern)
            local array_size = table.getn(array)

            local result = array[col]
            if result ~= nil then
                NewName = "csv_" .. tostring(result or "")
            end
        elseif tool.ID:match("^Fuse.vTextFromCSV") then
            local data = tool:GetInput("Input", comp.CurrentTime)
            local row = 1
            local col = tool:GetInput("Column")

            local pattern = "(.-),"

            -- A lazy workaround for greedy Lua pattern matching of the last comma in a CSV line
            local line = tostring(textutils.ReadLineIgnoreComments(data, row, "#")) .. ","

            local array = textutils.split(line, pattern)
            local array_size = table.getn(array)

            local result = array[col]
            if result ~= nil then
                NewName = "csv_" .. tostring(result or "")
            end
        end

        -- Rename the node
        tool:SetAttrs({TOOLS_Name = NewName})

        -- List the results
        -- print(string.format("[%03d] [Node Name] %30s [Old Name] %30s [Reg ID] %30s", _i, tool.Name, PrevName, tool.ID))
        print(string.format("[Node Name] %30s [Old Name] %30s [Reg ID] %30s\n", tool.Name, PrevName, tool.ID))
    end
--end

comp:EndUndo()

-- End of the script
-- print('[Done]')
