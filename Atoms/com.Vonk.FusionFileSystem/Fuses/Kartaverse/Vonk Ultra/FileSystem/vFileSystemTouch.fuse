-- ============================================================================
-- modules
-- ============================================================================
local filesystemutils = self and require("vfilesystemutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vFileSystemTouch"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_SinkTool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\FileSystem",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Touch a file/folders's creation and modification dates on macOS and Linux.",
    REGS_OpIconString = FUSE_NAME,
    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    -- https://www.steakunderwater.com/VFXPedia/96.0.243.189/indexf069.html?title=Eyeon:Script/Reference/Applications/Fuse/Classes/Input/FileControl
    InText = self:AddInput("Text", "Text", {
        LINKID_DataType = "Text",
        INPID_InputControl = "FileControl",
        FC_PathBrowse = false,
        INP_Passive = true,
        LINK_Main = 1
    })

--    InSkipCreatingFiles = self:AddInput("Skip Creating Files", "SkipCreatingFiles", {
--        LINKID_DataType = "Number",
--        INPID_InputControl = "CheckboxControl",
--        INP_Integer = true,
--        INP_Default = 1.0,
--        INP_External = false,
--        INP_DoNotifyChanged = true
--    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InText:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local path = InText:GetValue(req).Value
    -- local skip = InSkipCreatingFiles:GetValue(req).Value
    local skip = false

    filesystemutils.touch(path, skip)

    local out = Text(path)

    OutText:Set(req, out)
end
