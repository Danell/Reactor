-- ============================================================================
-- modules
-- ============================================================================
local jsonutils = self and require("vjsonutils") or nil
local arrayutils = self and require("varrayutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vJSONDoString"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\JSON\\Script",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Return JSON text from running a string of Lua code.",
    REGS_OpIconString = FUSE_NAME,
    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InJSON = self:AddInput("JSON", "JSON", {
        LINKID_DataType = DATATYPE,
        INP_Required = false,
        LINK_Main = 1
    })

    InHeader = self:AddInput("Script Header Wire", "Header", {
        LINKID_DataType = "Text",
        INPID_InputControl = "ImageControl",
        LINK_Visible = false,
        ICD_Width = 0.8,
        LINK_Main = 2, -- if set, you lose the ability to instance the imagecontrol
    })

    InSource = self:AddInput(">", "InSource", {
        INPID_InputControl = "ButtonControl",
        INP_External = false,
        IC_Visible = true,
        BTNCS_Execute = GetSource,
        ICD_Width = 0.2,
        INP_Passive = true, -- don't want this to trigger a re-render, so on it goes.
    })

    InSeparator = self:AddInput("UISeparator1", "UISeparator1", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    self:AddInput("Lua Script", "LuaScript", {
        LINKID_DataType = "Text",
        INPID_InputControl = "LabelControl",
        INP_External = false,
        INP_Passive = true
    })

    InText = self:AddInput("Script", "Script", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TECS_Language = "lua",
        IC_NoLabel = true,
        INPS_DefaultText = "return tbl",
        TEC_Lines = 25,
        -- TEC_FontSize = 18,
        -- TEC_Wrap = true,
        INP_Passive = true,
        LINK_Main = 3,
    })

--    InFontSize = self:AddInput("Font Size", "FontSize", {
--        LINKID_DataType      = "Number",
--        INPID_InputControl   = "MultiButtonControl",
--        INP_Integer          = true,
--        INP_Default          = 3.0,
--        MBTNC_StretchToFit = true,
--        {MBTNC_AddButton     = "12"},
--        {MBTNC_AddButton     = "14"},
--        {MBTNC_AddButton     = "16"},
--        {MBTNC_AddButton     = "18"},
--        {MBTNC_AddButton     = "24"},
--        {MBTNC_AddButton     = "36"},
--        {MBTNC_AddButton     = "48"},
--        {MBTNC_AddButton     = "72"},
--        INP_DoNotifyChanged  = true,
--        INP_Passive = true,
--    })

--    InSyntaxHighlight = self:AddInput("Syntax Highlight", "SyntaxHighlight", {
--        LINKID_DataType      = "Number",
--        INPID_InputControl   = "MultiButtonControl",
--        INP_Integer          = true,
--        INP_Default          = 1.0,
--        MBTNC_StretchToFit = true,
--        {MBTNC_AddButton     = "Text"},
--        {MBTNC_AddButton     = "Lua"},
--        {MBTNC_AddButton     = "Python"},
--        {MBTNC_AddButton     = "HTML"},
--        {MBTNC_AddButton     = "XML"},
--        {MBTNC_AddButton     = "JSON"},
--        {MBTNC_AddButton     = "CSS"},
--        {MBTNC_AddButton     = "CPP"},
--        {MBTNC_AddButton     = "Markdown"},
--        {MBTNC_AddButton     = "GLSL"},
--        {MBTNC_AddButton     = "Fusion"},
--        INP_DoNotifyChanged  = true,
--        INP_Passive = true,
--    })

    InDisplayLines = self:AddInput("Display Lines", "DisplayLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = true,
        INP_MaxScale = 100,
        INP_MinAllowed = 1,
        INP_Default = 25,
        INP_Passive = true,
        LINK_Visible = true,
        INP_DoNotifyChanged  = true,
    })

    InWrapLines = self:AddInput("Wrap Lines", "WrapLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_Passive = true,
        INP_DoNotifyChanged = true
    })

    InUISeparator2 = self:AddInput("UISeparator2", "UISeparator2", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InShowCode = self:AddInput("Show Code", "ShowCode", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InShowDump = self:AddInput("Show Dump", "ShowDump", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InUISeparator3 = self:AddInput("UISeparator3", "UISeparator3", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InSort = self:AddInput("Sort List", "Sort", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutJSON = self:AddOutput("Output", "Output", {
        LINKID_DataType = DATATYPE,
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InText:SetAttrs({LINK_Visible = visible})
    elseif inp == InWrapLines then
        local wrap
        if param.Value == 1.0 then wrap = true else wrap = false end
        InText:SetAttrs({TEC_Wrap = wrap})

        -- Toggle the visibility to refresh the inspector view
        InText:SetAttrs({IC_Visible = false})
        InText:SetAttrs({IC_Visible = true})
--    elseif inp == InSyntaxHighlight then
--        local lang
--
--        if param.Value == 1.0 then 
--            lang = "text"
--        elseif param.Value == 2.0 then 
--            lang = "lua"
--        elseif param.Value == 3.0 then 
--            lang = "python"
--        elseif param.Value == 4.0 then 
--            lang = "html"
--        elseif param.Value == 5.0 then 
--            lang = "xml"
--        elseif param.Value == 6.0 then 
--            lang = "json"
--        elseif param.Value == 8.0 then 
--            lang = "css"
--        elseif param.Value == 9.0 then 
--            lang = "cpp"
--        elseif param.Value == 10.0 then 
--            lang = "markdown"
--        elseif param.Value == 11.0 then 
--            lang = "glsl"
--        elseif param.Value == 12.0 then 
--            lang = "fusion"
--        else
--            lang = "text"
--        end
--        InText:SetAttrs({TECS_Language = lang})
--
--        -- Toggle the visibility to refresh the inspector view
--        InText:SetAttrs({IC_Visible = false})
--        InText:SetAttrs({IC_Visible = true})
    elseif inp == InDisplayLines then
        -- Update the TEC Lines value dynamically
        -- Inspired by vNumberXSheet.fuse
        local lines = InDisplayLines:GetSource(time, REQF_SecondaryTime).Value
        InText:SetAttrs({TEC_Lines = lines})

        -- Toggle the visibility to refresh the inspector view
        InText:SetAttrs({IC_Visible = false})
        InText:SetAttrs({IC_Visible = true})
--    elseif inp == InFontSize then
--        -- Change the TextEditControl font size in points
--        local fontSizeTbl = {12, 14, 16, 18, 24, 36, 48, 72}
--        local fontSizeSelect = fontSizeTbl[param.Value + 1]
--        InText:SetAttrs({TEC_FontSize = fontSizeSelect})
--        -- print("[Font Size Pt]", fontSizeSelect)
--
--        -- Toggle the visibility to refresh the inspector view
--        InText:SetAttrs({IC_Visible = false})
--        InText:SetAttrs({IC_Visible = true})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    -- Convert JSON text into a Lua table
    local json_str_in = InJSON:GetValue(req).Value
    local sort = InSort:GetValue(req).Value

    local tbl_in = {}
    if json_str_in ~= nil and json_str_in ~= "" then
        tbl_in = jsonutils.decode(json_str_in)
    end

    -- Sort the array alphabetically
    if sort == 1.0 then
        table.sort(tbl_in)
    end

    -- Run the DoString code block
    local show_code = InShowCode:GetValue(req).Value
    local show_dump = InShowDump:GetValue(req).Value

    local txt_str = InText:GetValue(req).Value or ""
    local header_str = InHeader:GetValue(req).Value or ""

    -- Generate the block of Lua code to run
    local tbl_str = "local tbl = " .. bmd.writestring(tbl_in) .. "\n"
    local script_str = header_str .. "\n" .. tbl_str .. "\n" .. txt_str
    local tbl_out = dostring(script_str) or {}

    -- Sort the array alphabetically
    if sort == 1.0 then
        table.sort(tbl_out)
    end

    if show_dump == 1 or show_code == 1 then
        print("\n----------------------")
        print("[" .. tostring(self.Name) .. "]")
    end
    if show_code == 1 then
        print("\n[Lua Script]")
        print(script_str)
    end
    if show_dump == 1 then
        print("\n[Lua Table]")
        dump(tbl_out)
    end

    -- Convert a Lua table back into JSON text
    -- local json_str_out = jsonutils.encode(tbl_out)
    -- Should the array data have indentations added
    local json_str_out = jsonutils.encode_indent(tbl_out, true)

    local out = Text(json_str_out)

    OutJSON:Set(req, out)
end
